import {createStore, combineReducers, applyMiddleware, compose} from "redux";
import thunk from 'redux-thunk';

import globalReducer from '../reducers/global';
import postsReducer from '../reducers/posts';
import communitiesReducer from '../reducers/communities';
import feedsReducer from '../reducers/feeds';
import authReducer from '../reducers/auth';
import profileReducer from '../reducers/profile';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
    globalReducer: globalReducer,
    authReducer: authReducer,
    communitiesReducer: communitiesReducer,
    feedsReducer: feedsReducer,
    postsReducer: postsReducer,
    profileReducer: profileReducer,
});

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

export {store};

