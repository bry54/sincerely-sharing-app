import {
    BOOKMARK_COMMENT,
    COMMENT_ON_COMMENT, COMMENT_ON_FEED, EDIT_COMMENT,
    SET_ACTIVE_COMMENT, SET_COMMENT_BODY, VOTE_COMMENT,
} from '../utilities/actionTypes';
import {setBusyStatus} from '../utilities/actionsCollection';
import {axiosConfig} from '../../utilities/RequestConfig';
import UrlDefinitions from '../../constants/UrlDefinitions';
import axios from 'axios';
import HTTPCodes from "../../constants/HTTPCodes";

export const setActiveComment = ( comment ) => async dispatch => {
    dispatch({
        type: SET_ACTIVE_COMMENT,
        payload: {
            comment: comment,
        }
    });
};

export const setCommentBodyInput = ( value ) => async dispatch => {
    dispatch({
        type: SET_COMMENT_BODY,
        payload: value
    });
};

export const fetchCommentDetail = ( comment, criterion='DATE' ) => async dispatch => {
    const config = await axiosConfig();
    dispatch(setBusyStatus(true, 'Fetching Comment Detail', HTTPCodes.CREATED));

    await axios.post(UrlDefinitions.FETCH_COMMENT_DETAIL, {
        order_criterion: criterion,
        comment_id: comment.id
    }, config)
        .then((response) => {
            const result = response.data;
            dispatch({
                type: SET_ACTIVE_COMMENT,
                payload: {
                    comment: result.comment,
                }
            });
            dispatch(setBusyStatus(false, ''));
        })
        .catch( (error) => {
            console.log(error);
            if (!error.response)
                dispatch(setBusyStatus(false, 'Network error, make sure you have a working internet connection and try refreshing again.', HTTPCodes.GATEWAY_TIMEOUT));
            else if (error.response.status === HTTPCodes.INTERNAL_SERVER_ERROR)
                dispatch(setBusyStatus(false, 'Server error, please try again later', HTTPCodes.INTERNAL_SERVER_ERROR));
        });
};

export const bookmarkComment = ( comment ) => async dispatch => {
    dispatch(setBusyStatus(true, 'Bookmarking comment', HTTPCodes.CREATED));
    const config = await axiosConfig();

    await axios.post(UrlDefinitions.BOOKMARK_COMMENT, {
        comment_id: comment.id
    }, config)
        .then((response) => {
            const result = response.data;
            dispatch({
                type: BOOKMARK_COMMENT,
                payload: {
                    commentId: comment.id,
                    followers: result.followers,
                }
            });
            dispatch(setBusyStatus(false, 'Comment status updated 😎'));
        })
        .catch( (error) => {
            console.log(error);
            if (!error.response)
                dispatch(setBusyStatus(false, 'Network error, make sure you have a working internet connection and try again', HTTPCodes.GATEWAY_TIMEOUT));
            else if (error.response.status === HTTPCodes.INTERNAL_SERVER_ERROR)
                dispatch(setBusyStatus(false, 'Server error, please try again later', HTTPCodes.INTERNAL_SERVER_ERROR));
        });
};

export const voteComment = ( vote ) => async dispatch => {
    dispatch(setBusyStatus(true, 'Voting comment', HTTPCodes.CREATED));
    const config = await axiosConfig();

    await axios.post(UrlDefinitions.VOTE_COMMENT, {
        comment_id: vote.commentId,
        up_vote: vote.up,
        down_vote: vote.down,
    }, config)
        .then((response) => {
            const result = response.data;
            dispatch({
                type: VOTE_COMMENT,
                payload: {
                    commentId: vote.commentId,
                    upVotes: result.up_votes,
                    downVotes: result.down_votes,
                }
            });
            dispatch(setBusyStatus(false, 'Comment votes updated 😎'));
        })
        .catch( (error) => {
            console.log(error);
            if (!error.response)
                dispatch(setBusyStatus(false, 'Network error, make sure you have a working internet connection and try again', HTTPCodes.GATEWAY_TIMEOUT));
            else if (error.response.status === HTTPCodes.INTERNAL_SERVER_ERROR)
                dispatch(setBusyStatus(false, 'Server error, please try again later', HTTPCodes.INTERNAL_SERVER_ERROR));
        });
};

export const submitComment = ( comment ) => async dispatch => {
    dispatch(setBusyStatus(true, 'Submitting comment', HTTPCodes.CREATED));
    const config = await axiosConfig();
    let COMMENTING_URL = null;
    let COMMENTING_PARAMS = null;

    if (comment.route === 'commentOnFeed'){
        COMMENTING_URL = UrlDefinitions.COMMENT_ON_FEED;
        COMMENTING_PARAMS = {
            comment_id: comment.comment_id,
            post_id: comment.item.id,
            comment_body: comment.commentBody
        }
    }

    if (comment.route === 'commentOnComment'){
        COMMENTING_URL = UrlDefinitions.COMMENT_ON_COMMENT;
        COMMENTING_PARAMS = {
            comment_id: comment.comment_id,
            parent_id: comment.item.id,
            comment_body: comment.commentBody
        }
    }

    await axios.post(COMMENTING_URL, COMMENTING_PARAMS, config)
        .then((response) => {
            const result = response.data;
            if (comment.route === 'commentOnFeed')
                dispatch({
                    type: COMMENT_ON_FEED,
                    payload: {
                        feed: result.feed,
                    }
                });

            if (comment.route === 'commentOnComment')
                dispatch({
                    type: COMMENT_ON_COMMENT,
                    payload: {
                        comment: result.comment,
                    }
                });

            dispatch({
                type: EDIT_COMMENT,
                payload: {
                    comment: result.comment,
                }
            });
            dispatch(setBusyStatus(false, 'Comment submitted 😎'));
        })
        .catch( (error) => {
            console.log(error.response);
            if (!error.response)
                dispatch(setBusyStatus(false, 'Network error, make sure you have a working internet connection and try sending again', HTTPCodes.GATEWAY_TIMEOUT));
            else if (error.response.status === HTTPCodes.INTERNAL_SERVER_ERROR)
                dispatch(setBusyStatus(false, 'Server error, please try again later', HTTPCodes.INTERNAL_SERVER_ERROR));
        });
};


