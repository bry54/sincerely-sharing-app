import {SET_BUSY_STATUS} from '../utilities/actionTypes';

export const setBusyStatus = (busy=false, message='', code=200) => async dispatch => {
    dispatch({
        type: SET_BUSY_STATUS,
        payload:{
            code: code,
            busy: busy,
            message: message
        }
    })
};
