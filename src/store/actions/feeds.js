import {
    BOOKMARK_FEED,
    FETCH_FEEDS, SET_ACTIVE_FEED, SET_ITEM_TO_COMMENT, VOTE_FEED,
} from '../utilities/actionTypes';
import {setBusyStatus} from '../utilities/actionsCollection';
import {axiosConfig} from '../../utilities/RequestConfig';
import UrlDefinitions from '../../constants/UrlDefinitions';
import axios from 'axios';
import HTTPCodes from "../../constants/HTTPCodes";

export const fetchFeeds = ( route, community=null, criterion='DATE' ) => async dispatch => {
    const config = await axiosConfig();
    dispatch(setBusyStatus(true, community ? 'Fetching community feeds': 'Fetching feeds from your communities', HTTPCodes.CREATED));

    await axios.post(UrlDefinitions.FETCH_FEEDS, {
        community_id: community ? community.id : null,
        order_criterion: criterion
    }, config)
        .then((response) => {
            const result = response.data;
            dispatch({
                type: FETCH_FEEDS,
                payload: {
                    route: route,
                    feeds: result.feeds,
                }
            });
            dispatch(setBusyStatus(false, ''));
        })
        .catch( (error) => {
            console.log(error);
            if (!error.response)
                dispatch(setBusyStatus(false, 'Network error, make sure you have a working internet connection and try refreshing again.', HTTPCodes.GATEWAY_TIMEOUT));
            else if (error.response.status === HTTPCodes.INTERNAL_SERVER_ERROR)
                dispatch(setBusyStatus(false, 'Server error, please try again later', HTTPCodes.INTERNAL_SERVER_ERROR));
        });
};

export const fetchFeedDetail = ( route, feed, criterion='DATE' ) => async dispatch => {
    const config = await axiosConfig();
    dispatch(setBusyStatus(true, 'Fetching Feed Detail', HTTPCodes.CREATED));

    await axios.post(UrlDefinitions.FETCH_FEED_DETAIL, {
        order_criterion: criterion,
        post_id: feed.id
    }, config)
        .then((response) => {
            const result = response.data;
            dispatch({
                type: SET_ACTIVE_FEED,
                payload: {
                    route: route,
                    feed: result.feed,
                }
            });
            dispatch(setBusyStatus(false, ''));
        })
        .catch( (error) => {
            console.log(error);
            if (!error.response)
                dispatch(setBusyStatus(false, 'Network error, make sure you have a working internet connection and try refreshing again.', HTTPCodes.GATEWAY_TIMEOUT));
            else if (error.response.status === HTTPCodes.INTERNAL_SERVER_ERROR)
                dispatch(setBusyStatus(false, 'Server error, please try again later', HTTPCodes.INTERNAL_SERVER_ERROR));
        });
};

export const bookmarkFeed = ( feed ) => async dispatch => {
    dispatch(setBusyStatus(true, 'Bookmarking feed', HTTPCodes.CREATED));
    const config = await axiosConfig();

    await axios.post(UrlDefinitions.BOOKMARK_FEED, {
        post_id: feed.id
    }, config)
        .then((response) => {
            const result = response.data;

            dispatch({
                type: BOOKMARK_FEED,
                payload: {
                    feedId: feed.id,
                    followers: result.followers,
                }
            });
            dispatch(setBusyStatus(false, ''));
        })
        .catch( (error) => {
            console.log(error);
            if (!error.response)
                dispatch(setBusyStatus(false, 'Network error, make sure you have a working internet connection and try again', HTTPCodes.GATEWAY_TIMEOUT));
            else if (error.response.status === HTTPCodes.INTERNAL_SERVER_ERROR)
                dispatch(setBusyStatus(false, 'Server error, please try again later', HTTPCodes.INTERNAL_SERVER_ERROR));
        });
};

export const voteFeed = ( vote ) => async dispatch => {
    const config = await axiosConfig();

    await axios.post(UrlDefinitions.VOTE_FEED, {
        post_id: vote.feedId,
        up_vote: vote.up,
        down_vote: vote.down,
    }, config)
        .then((response) => {
            const result = response.data;
            dispatch({
                type: VOTE_FEED,
                payload: {
                    feedId: vote.feedId,
                    upVotes: result.up_votes,
                    downVotes: result.down_votes,
                }
            });
        })
        .catch( (error) => {
            console.log(error);
            if (!error.response)
                dispatch(setBusyStatus(false, 'Network error, make sure you have a working internet connection and try again', HTTPCodes.GATEWAY_TIMEOUT));
            else if (error.response.status === HTTPCodes.INTERNAL_SERVER_ERROR)
                dispatch(setBusyStatus(false, 'Server error, please try again later', HTTPCodes.INTERNAL_SERVER_ERROR));
        });
};

export const setActiveFeed = ( route, feed ) => async dispatch => {
    dispatch({
        type: SET_ACTIVE_FEED,
        payload: {
            route: route,
            feed: feed,
        }
    });
};

export const setItemToComment = ( route, item ) => async dispatch => {
    dispatch({
        type: SET_ITEM_TO_COMMENT,
        payload: {
            route: route,
            item: item,
        }
    });
};

