import {EDIT_POST, SET_POST_CONTROLS, SUBMIT_POST} from '../utilities/actionTypes';
import {axiosConfig} from '../../utilities/RequestConfig';
import UrlDefinitions from '../../constants/UrlDefinitions';
import axios from 'axios';
import {setBusyStatus} from "./global";
import HTTPCodes from "../../constants/HTTPCodes";

export const setPostControls = ( controlObject) => async dispatch => {
    dispatch({
        type: SET_POST_CONTROLS,
        payload: controlObject
    })
};

export const submitPost = ( post ) => async dispatch => {
    dispatch(setBusyStatus(true, 'Submitting your post', HTTPCodes.CREATED ));
    const config = await axiosConfig();
    await axios.post(UrlDefinitions.SUBMIT_POST, {
        post_id: post.post_id,
        title: post.title,
        body: post.body,
        category: post.category,
        community: post.community,
    }, config)
        .then((response) => {
            const result = response.data;
            dispatch({
                type: SUBMIT_POST,
                payload: {
                    feed: result.feed,
                }
            });

            dispatch({
                type: EDIT_POST,
                payload: {
                    feed: result.feed,
                }
            });
            dispatch(setBusyStatus(false, '' ));
        })
        .catch( (error) => {
            console.log(error.response);
            if (!error.response)
                dispatch(setBusyStatus(false, 'Network error, make sure you have a working internet connection and try sending again', HTTPCodes.GATEWAY_TIMEOUT));
            else if (error.response.status === HTTPCodes.INTERNAL_SERVER_ERROR)
                dispatch(setBusyStatus(false, 'Server error, please try again later', HTTPCodes.INTERNAL_SERVER_ERROR));
        });
};
