import {
    TOGGLE_COMMENT_ARCHIVE_STATUS,
    TOGGLE_POST_ARCHIVE_STATUS,
    FETCH_PROFILE_DATA,
    SET_SEARCH_INPUT,
    SUBMIT_POST, DELETE_COMMENT, DELETE_POST,
} from '../utilities/actionTypes';
import {axiosConfig} from "../../utilities/RequestConfig";
import axios from "axios";
import UrlDefinitions from "../../constants/UrlDefinitions";
import {setBusyStatus} from '../utilities/actionsCollection';
import HTTPCodes from "../../constants/HTTPCodes";

export const setSearchInput = (ctrlObj) => async dispatch => {
    dispatch({
        type: SET_SEARCH_INPUT,
        payload:{
            key: ctrlObj.key,
            value: ctrlObj.value
        }
    })
};

export const fetchProfileData = () => async dispatch => {
    const config = await axiosConfig();
    dispatch(setBusyStatus(true, 'Fetching user data', HTTPCodes.CREATED));
    await axios.post(UrlDefinitions.FETCH_PROFILE_DATA, {
    }, config)
        .then((response) => {
            const result = response.data;
            dispatch({
                type: FETCH_PROFILE_DATA,
                payload:{
                    profile: result.profile
                }
            });

            dispatch(setBusyStatus(false, ''));
        })
        .catch( (error) => {
            console.log(error.response);
            if (!error.response)
                dispatch(setBusyStatus(false, 'Network error, make sure you have a working internet connection and pull down to refresh.', HTTPCodes.GATEWAY_TIMEOUT));
            else if (error.response.status === HTTPCodes.INTERNAL_SERVER_ERROR)
                dispatch(setBusyStatus(false, 'Server error, please try again later', HTTPCodes.INTERNAL_SERVER_ERROR));
        });
};

export const togglePostArchiveStatus = (post) => async dispatch => {
    const config = await axiosConfig();
    dispatch(setBusyStatus(true, 'Archiving post', HTTPCodes.CREATED));
    await axios.post(UrlDefinitions.ARCHIVE_POST, {
        post_id: post.id
    }, config)
        .then((response) => {
            const result = response.data;
            dispatch({
                type: TOGGLE_POST_ARCHIVE_STATUS,
                payload:{
                    feed: result.feed,
                }
            });

            dispatch(setBusyStatus(false, ''));
        })
        .catch( (error) => {
            console.log(error.response);
            if (!error.response)
                dispatch(setBusyStatus(false, 'Network error, make sure you have a working internet connection and try refreshing again.', HTTPCodes.GATEWAY_TIMEOUT));
            else if (error.response.status === HTTPCodes.INTERNAL_SERVER_ERROR)
                dispatch(setBusyStatus(false, 'Server error, please try again later', HTTPCodes.INTERNAL_SERVER_ERROR));
        });
};

export const toggleCommentArchiveStatus = (comment) => async dispatch => {
    const config = await axiosConfig();
    dispatch(setBusyStatus(true, 'Archiving comment', HTTPCodes.CREATED));
    await axios.post(UrlDefinitions.ARCHIVE_COMMENT, {
        comment_id: comment.id
    }, config)
        .then((response) => {
            const result = response.data;
            dispatch({
                type: TOGGLE_COMMENT_ARCHIVE_STATUS,
                payload:{
                    comment: result.comment,
                }
            });

            dispatch(setBusyStatus(false, ''));
        })
        .catch( (error) => {
            console.log(error.response);
            if (!error.response)
                dispatch(setBusyStatus(false, 'Network error, make sure you have a working internet connection and try again', HTTPCodes.GATEWAY_TIMEOUT));
            else if (error.response.status === HTTPCodes.INTERNAL_SERVER_ERROR)
                dispatch(setBusyStatus(false, 'Server error, please try again later', HTTPCodes.INTERNAL_SERVER_ERROR));
        });
};

export const deleteComment = (comment) => async dispatch => {
    const config = await axiosConfig();
    dispatch(setBusyStatus(true, 'Deleting comment', HTTPCodes.CREATED));
    await axios.post(UrlDefinitions.DELETE_COMMENT, {
        comment_id: comment.id
    }, config)
        .then((response) => {
            dispatch({
                type: DELETE_COMMENT,
                payload:{
                    deleted_comment_id: comment.id,
                }
            });

            dispatch(setBusyStatus(false, ''));
        })
        .catch( (error) => {
            console.log(error.response);
            if (!error.response)
                dispatch(setBusyStatus(false, 'Network error, make sure you have a working internet connection and try again', HTTPCodes.GATEWAY_TIMEOUT));
            else if (error.response.status === HTTPCodes.INTERNAL_SERVER_ERROR)
                dispatch(setBusyStatus(false, 'Server error, please try again later', HTTPCodes.INTERNAL_SERVER_ERROR));
        });
};

export const deletePost = (post) => async dispatch => {
    const config = await axiosConfig();
    dispatch(setBusyStatus(true, 'Deleting post', HTTPCodes.CREATED));
    await axios.post(UrlDefinitions.DELETE_POST, {
        post_id: post.id
    }, config)
        .then((response) => {
            dispatch({
                type: DELETE_POST,
                payload:{
                    deleted_post_id: post.id,
                }
            });

            dispatch(setBusyStatus(false, ''));
        })
        .catch( (error) => {
            console.log(error.response);
            if (!error.response)
                dispatch(setBusyStatus(false, 'Network error, make sure you have a working internet connection and try again', HTTPCodes.GATEWAY_TIMEOUT));
            else if (error.response.status === HTTPCodes.INTERNAL_SERVER_ERROR)
                dispatch(setBusyStatus(false, 'Server error, please try again later', HTTPCodes.INTERNAL_SERVER_ERROR));
        });
};
