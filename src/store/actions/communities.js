import {
    SET_COMMUNITY_BUTTON_INDEX,
    FETCH_ALL_COMMUNITIES,
    TOGGLE_COMMUNITY_SUBSCRIPTION,
    FETCH_COMMUNITY_FEEDS,
    SET_ACTIVE_COMMUNITY,
    TOGGLE_PASSWORD_DIALOG_VISIBILITY,
    SET_COMMUNITY_PASSWORD,
    SET_POST_CONTROLS,
    SET_COMMUNITY_REQUEST_CONTROLS,
    SUBMIT_COMMUNITY_REQUEST,
    VOTE_FEED,
    TOGGLE_COMMUNITY_REQUEST_VOTE,
} from '../utilities/actionTypes';
import {setBusyStatus} from '../utilities/actionsCollection';
import {axiosConfig} from '../../utilities/RequestConfig';
import UrlDefinitions from '../../constants/UrlDefinitions';
import axios from 'axios';
import HTTPCodes from "../../constants/HTTPCodes";

export const updateCommunityButtonIndex = (buttonIndex) => async dispatch => {
    dispatch({
        type: SET_COMMUNITY_BUTTON_INDEX,
        payload: buttonIndex
    })
};

export const setActiveCommunity = (community) => async dispatch => {
    dispatch({
        type: SET_ACTIVE_COMMUNITY,
        payload: community
    })
};

export const togglePasswordDialogVisibility = () => async dispatch => {
    dispatch({
        type: TOGGLE_PASSWORD_DIALOG_VISIBILITY,
    })
};

export const setCommunityPassword = ( value ) => async dispatch => {
    dispatch({
        type: SET_COMMUNITY_PASSWORD,
        payload: value
    });
};

export const setRequestControls = ( controlObject) => async dispatch => {
    dispatch({
        type: SET_COMMUNITY_REQUEST_CONTROLS,
        payload: controlObject
    })
};

export const fetchCommunities = () => async dispatch => {
    const config = await axiosConfig();
    dispatch(setBusyStatus(true, 'Fetching Communities', HTTPCodes.CREATED));

    await axios.post(UrlDefinitions.FETCH_ALL_COMMUNITIES, {
    }, config)
        .then((response) => {
            const result = response.data;
            dispatch({
                type: FETCH_ALL_COMMUNITIES,
                payload: {
                    available: result.available,
                    requested: result.requested,
                }
            });
            dispatch(setBusyStatus(false, ''));
        })
        .catch( (error) => {
            console.log(error.response);
            if (!error.response)
                dispatch(setBusyStatus(false, 'Network error, make sure you have a working internet connection and try refreshing again.', HTTPCodes.GATEWAY_TIMEOUT));
            else if (error.response.status === HTTPCodes.INTERNAL_SERVER_ERROR)
                dispatch(setBusyStatus(false, 'Server error, please try again later', HTTPCodes.INTERNAL_SERVER_ERROR));
        });
};

export const toggleCommunitySubscription = (community, password='secret') => async dispatch => {
    const config = await axiosConfig();
    dispatch(setBusyStatus(true, 'Toggling subscription', HTTPCodes.CREATED));

    await axios.post(UrlDefinitions.TOGGLE_COMMUNITY_SUBSCRIPTION, {
        community_id: community.id,
        password: password
    }, config)
        .then((response) => {
            const result = response.data;
            dispatch({
                type: TOGGLE_COMMUNITY_SUBSCRIPTION,
                payload: {
                    success: result.success,
                    message: result.message,
                    community: result.community
                }
            });

            dispatch(setActiveCommunity(result.community));

            dispatch(setBusyStatus(false, ''));
        })
        .catch( (error) => {
            console.log(error);
            if (!error.response)
                dispatch(setBusyStatus(false, 'Network error, make sure you have a working internet connection and try refreshing again.', HTTPCodes.GATEWAY_TIMEOUT));
            else if (error.response.status === HTTPCodes.INTERNAL_SERVER_ERROR)
                dispatch(setBusyStatus(false, 'Server error, please try again later', HTTPCodes.INTERNAL_SERVER_ERROR));
        });
};

export const submitRequest = ( request ) => async dispatch => {
    const config = await axiosConfig();
    dispatch(setBusyStatus(true, 'Submitting Request', HTTPCodes.CREATED));

    await axios.post(UrlDefinitions.REQUEST_COMMUNITY, {
        title: request.title,
        body: request.body,
        fif_exclusive: request.fif_exclusive
    }, config)
        .then((response) => {
            const result = response.data;
            dispatch({
                type: SUBMIT_COMMUNITY_REQUEST,
                payload: {
                    requested: result.requested
                }
            });
            dispatch(setBusyStatus(false, ''));
        })
        .catch( (error) => {
            console.log(error);
            if (!error.response)
                dispatch(setBusyStatus(false, 'Network error, make sure you have a working internet connection and try refreshing again.', HTTPCodes.GATEWAY_TIMEOUT));
            else if (error.response.status === HTTPCodes.INTERNAL_SERVER_ERROR)
                dispatch(setBusyStatus(false, 'Server error, please try again later', HTTPCodes.INTERNAL_SERVER_ERROR));
        });
};

export const toggleCommunityRequestVote = (vote) => async dispatch => {
    dispatch(setBusyStatus(true, 'Voting community request', HTTPCodes.CREATED));
    const config = await axiosConfig();

    await axios.post(UrlDefinitions.VOTE_REQUESTED_COMMUNITY, {
        community_id: vote.communityId,
        up_vote: vote.up,
        down_vote: vote.down,
    }, config)
        .then((response) => {
            const result = response.data;
            dispatch({
                type: TOGGLE_COMMUNITY_REQUEST_VOTE,
                payload: {
                    requested: result.requested
                }
            });
            dispatch(setBusyStatus(false, '', ));
        })
        .catch( (error) => {
            console.log(error) ;
            if (!error.response)
                dispatch(setBusyStatus(false, 'Network error, make sure you have a working internet connection and try refreshing again.', HTTPCodes.GATEWAY_TIMEOUT));
            else if (error.response.status === HTTPCodes.INTERNAL_SERVER_ERROR)
                dispatch(setBusyStatus(false, 'Server error, please try again later', HTTPCodes.INTERNAL_SERVER_ERROR));
        });
};
