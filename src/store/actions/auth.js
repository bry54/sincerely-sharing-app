import axios from 'axios'
import AsyncStorage from '@react-native-community/async-storage'
import {SET_AUTH_CONTROLS, SET_LOGIN_PAYLOAD, TOGGLE_PASSWORD_VISIBILITY} from '../utilities/actionTypes';
import {setBusyStatus} from './global';
import UrlDefinitions from '../../constants/UrlDefinitions';
import HTTPCodes from "../../constants/HTTPCodes";

export const attemptUserLogin = ( loginData ) => async dispatch => {
    dispatch(setBusyStatus(true, 'Attempting login', HTTPCodes.CREATED));
    await axios.post(UrlDefinitions.LOGIN, {
        email: loginData.email,
        password: loginData.password
    })
        .then(async (response) => {
            dispatch(setBusyStatus(false, 'Done'));
            const payload = response.data.payload;
            await AsyncStorage.setItem('loginPayload', JSON.stringify(payload));

            dispatch(setLoginPayload(payload));
        })
        .catch( (error) => {
            console.log(error.response);
            if (!error.response)
                dispatch(setBusyStatus(false, 'Network error, make sure you have a working internet connection and try sending again', HTTPCodes.GATEWAY_TIMEOUT));
            else if (error.response.status === HTTPCodes.INTERNAL_SERVER_ERROR)
                dispatch(setBusyStatus(false, 'Server error, please try again later', HTTPCodes.INTERNAL_SERVER_ERROR));
        });
};

export const attemptSignUp = ( loginData ) => async dispatch => {
    dispatch(setBusyStatus(true, 'Attempting login', HTTPCodes.CREATED));
    await axios.post(UrlDefinitions.LOGIN, {
        email: loginData.email,
        password: loginData.password
    })
        .then(async (response) => {
            dispatch(setBusyStatus(false, 'Done'));
            const payload = response.data.payload;
            await AsyncStorage.setItem('loginPayload', JSON.stringify(payload));

            dispatch(setLoginPayload(payload));
        })
        .catch( (error) => {
            console.log(error.response);
            if (!error.response)
                dispatch(setBusyStatus(false, 'Network error, make sure you have a working internet connection and try sending again', HTTPCodes.GATEWAY_TIMEOUT));
            else if (error.response.status === HTTPCodes.INTERNAL_SERVER_ERROR)
                dispatch(setBusyStatus(false, 'Server error, please try again later', HTTPCodes.INTERNAL_SERVER_ERROR));
        });
};

export const attemptForgetPasswordRequest = ( loginData ) => async dispatch => {
    dispatch(setBusyStatus(true, 'Attempting login', HTTPCodes.CREATED));
    await axios.post(UrlDefinitions.LOGIN, {
        email: loginData.email,
        password: loginData.password
    })
        .then(async (response) => {
            dispatch(setBusyStatus(false, 'Done'));
            const payload = response.data.payload;
            await AsyncStorage.setItem('loginPayload', JSON.stringify(payload));

            dispatch(setLoginPayload(payload));
        })
        .catch( (error) => {
            if (!error.response)
                dispatch(setBusyStatus(false, 'Network error, make sure you have a working internet connection and try sending again', HTTPCodes.GATEWAY_TIMEOUT));
            else if (error.response.status === HTTPCodes.INTERNAL_SERVER_ERROR)
                dispatch(setBusyStatus(false, 'Server error, please try again later', HTTPCodes.INTERNAL_SERVER_ERROR));
        });
};

export const attemptLogout = ( ) => async dispatch => {
    dispatch(setBusyStatus(true, 'Attempting logout', HTTPCodes.CREATED));
    await axios.post(UrlDefinitions.LOGOUT, {

    })
        .then(async (response) => {
            dispatch(setBusyStatus(false, ''));
            console.log(response)
        })
        .catch( (error) => {
            console.log(error.response);
        });
};

export const setLoginPayload = ( loginPayload ) => async dispatch => {
    dispatch({
        type: SET_LOGIN_PAYLOAD,
        payload: loginPayload
    })
};

export const setAuthControls = (controlObject) => async dispatch => {
    dispatch({
        type: SET_AUTH_CONTROLS,
        payload: controlObject
    });
};

export const togglePasswordVisibility = (key) => async dispatch => {
    dispatch({
        type: TOGGLE_PASSWORD_VISIBILITY,
        payload: key
    });
};
