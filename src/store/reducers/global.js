import {
    SET_BUSY_STATUS,
} from '../utilities/actionTypes';

const initialState = {
    busyStatus:{
        code: 200,
        isBusy: false,
        message: '',
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_BUSY_STATUS: {
            return {
                ...state,
                busyStatus: {
                    ...state.busyStatus,
                    ...action.payload
                }
            }
        }

        default:
            return state;
    }
};
