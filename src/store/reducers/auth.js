import {
    SET_AUTH_CONTROLS,
    SET_BUSY_STATUS, SET_LOGIN_PAYLOAD, TOGGLE_PASSWORD_VISIBILITY,
} from '../utilities/actionTypes';

const initialState = {
    controls: {
        email: null,
        username: null,
        password: null,
        passwordConfirm: null,
        sections: {
            auth: true,
            community: true
        }
    },
    requestErrorMessage: null,
    controlErrors: [],
    loginPayload: null,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_LOGIN_PAYLOAD: {
            return {
                ...state,
                loginPayload: action.payload
            }
        }

        case SET_AUTH_CONTROLS: {
            const ctrlObj = action.payload;
            const key = ctrlObj.key;
            const value = ctrlObj.value;
            return {
                ...state,
                controls: {
                    ...state.controls,
                    [key]: value
                }
            };
        }

        case TOGGLE_PASSWORD_VISIBILITY:
            const key = action.payload;
            let hidePassword = state.controls.sections[key];
            return {...state,
                controls: {
                    ...state.controls,
                    sections: {
                        ...state.controls.sections,
                        [key]: !hidePassword
                    }
                }
            };

        default:
            return state;
    }
};
