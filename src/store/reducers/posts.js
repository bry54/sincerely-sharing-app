import {
    SET_POST_CONTROLS, SUBMIT_POST,
} from '../utilities/actionTypes';
import {store} from '../utilities/storeConfiguration';

const initialState = {
    categories: [],
    post_id: null,
    np_community: null,
    np_category: null,
    np_title: null,
    np_body: null,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_POST_CONTROLS:{
            const ctrlObj = action.payload;
            const key = ctrlObj.key;
            const value = ctrlObj.value;

            let categoriesClone = JSON.parse(JSON.stringify(state.categories));
            let selectedCategory = JSON.parse(JSON.stringify(state.np_category));

            if (key === 'np_community'){
                categoriesClone = value ? value.categories : [];
                selectedCategory = null;

                return {
                    ...state,
                    categories: categoriesClone,
                    np_category: selectedCategory,
                    np_community: value,

                }
            }

            return {
                ...state,
                [key]: value,
            }
        }

        default:
            return state;
    }
};
