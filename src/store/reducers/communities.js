import {
    FETCH_ALL_COMMUNITIES,
    SET_COMMUNITY_BUTTON_INDEX,
    TOGGLE_COMMUNITY_SUBSCRIPTION,
    SET_ACTIVE_COMMUNITY,
    TOGGLE_PASSWORD_DIALOG_VISIBILITY,
    SET_COMMENT_BODY,
    SET_COMMUNITY_PASSWORD,
    SET_POST_CONTROLS,
    SET_COMMUNITY_REQUEST_CONTROLS, SUBMIT_COMMUNITY_REQUEST, TOGGLE_COMMUNITY_REQUEST_VOTE,
} from '../utilities/actionTypes';

const initialState = {
    passwordDialogVisible: false,
    communityButtonIndex: 0,
    activeCommunity: null,
    subscriptionMessage: null,
    communities: {
        isFetched: false,
        available: [],
        requested: []
    },
    request:{
        title: null,
        body: null,
        fif_exclusive: false
    },
    communityPassword: null
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_COMMUNITY_BUTTON_INDEX: {
            return {
                ...state,
                communityButtonIndex: action.payload
            }
        }

        case SET_COMMUNITY_REQUEST_CONTROLS:{
            const ctrlObj = action.payload;
            const key = ctrlObj.key;
            const value = ctrlObj.value;

            return {
                ...state,
                request: {
                    ...state.request,
                    [key]: (key==='fif_exclusive' && value===null) ? false : value,
                }
            }
        }

        case TOGGLE_PASSWORD_DIALOG_VISIBILITY: {
            return {
                ...state,
                passwordDialogVisible: !state.passwordDialogVisible
            }
        }

        case SET_ACTIVE_COMMUNITY: {
            return {
                ...state,
                activeCommunity: action.payload
            }
        }

        case FETCH_ALL_COMMUNITIES: {
            const data = action.payload;
            return {
                ...state,
                communities: {
                    ...state.communities,
                    isFetched: true,
                    available: [...data.available],
                    requested: [...data.requested]
                }
            }
        }

        case TOGGLE_COMMUNITY_SUBSCRIPTION: {
            let communitiesClone = [...state.communities.available];
            const payload = action.payload;
            const message = payload.message;
            const success = payload.success;
            const toggledCommunity = payload.community;

            if (success) {
                let index = communitiesClone.findIndex(community => community.id === toggledCommunity.id);
                communitiesClone[index] = toggledCommunity;
            }

            return {
                ...state,
                subscriptionMessage: message,
                communities: {
                    ...state.communities,
                    available: [...communitiesClone]
                }
            }
        }

        case SUBMIT_COMMUNITY_REQUEST: {
            let communitiesClone = [...state.communities.requested];
            const payload = action.payload;
            const requested = payload.requested;
            communitiesClone.push(requested);

            return {
                ...state,
                communities: {
                    ...state.communities,
                    requested: [...communitiesClone]
                }
            }
        }

        case TOGGLE_COMMUNITY_REQUEST_VOTE: {
            const payload = action.payload;
            const requested = payload.requested;

            let requestedCommunitiesClone = [...state.communities.requested];

            let communityIndex = requestedCommunitiesClone.findIndex(community => community.id === requested.id);

            if (communityIndex !== -1) {
                requestedCommunitiesClone[communityIndex] = requested;
            }

            return {
                ...state,
                communities: {
                    ...state.communities,
                    requested: requestedCommunitiesClone
                }
            }
        }

        case SET_COMMUNITY_PASSWORD:{
            return {
                ...state,
                communityPassword: action.payload
            }
        }

        default:
            return state;
    }
};
