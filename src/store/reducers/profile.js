import {
    DELETE_COMMENT, DELETE_POST,
    EDIT_COMMENT,
    EDIT_POST,
    FETCH_PROFILE_DATA,
    SET_SEARCH_INPUT,
    TOGGLE_COMMENT_ARCHIVE_STATUS,
    TOGGLE_POST_ARCHIVE_STATUS,
    BOOKMARK_FEED,
    BOOKMARK_COMMENT,
    VOTE_FEED,
    VOTE_COMMENT
} from '../utilities/actionTypes';

const initialState = {
    profileFetched: false,
    data:{
        saved_posts: [],
        saved_comments: [],
        comments: [],
        posts: [],
    },

    search:{
        savedPosts: '',
        savedComments: '',
        myComments: '',
        myPosts: '',
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_SEARCH_INPUT: {
            const payload = action.payload;
            const key = payload.key;
            const value = payload.value;

            return {
                ...state,
                search: {
                    ...state.search,
                    [key]: value
                }
            }
        }

        case FETCH_PROFILE_DATA:{
            const payload = action.payload;
            const profile = payload.profile;
            return{
                ...state,
                profileFetched: true,
                data: profile,
            }
        }

        case EDIT_POST:{
            const edited = action.payload.feed;
            let myPostsClone = JSON.parse(JSON.stringify(state.data.posts));

            const feedIndex = myPostsClone.findIndex(post => post.id === edited.id);
            if(feedIndex !== -1){
                myPostsClone[feedIndex] = edited
            }

            return {
                ...state,
                data:{
                    ...state.data,
                    posts: myPostsClone
                }
            }
        }

        case EDIT_COMMENT:{
            const edited = action.payload.comment;
            let myCommentsClone = JSON.parse(JSON.stringify(state.data.comments));

            const commentIndex = myCommentsClone.findIndex(comment => comment.id === edited.id);
            if(commentIndex !== -1){
                myCommentsClone[commentIndex] = edited
            }

            return {
                ...state,
                data:{
                    ...state.data,
                    comments: myCommentsClone
                }
            }
        }

        case TOGGLE_COMMENT_ARCHIVE_STATUS:{
            const archived = action.payload.comment;
            let myCommentsClone = JSON.parse(JSON.stringify(state.data.comments));

            const commentIndex = myCommentsClone.findIndex(comment => comment.id === archived.id);
            if(commentIndex !== -1){
                myCommentsClone[commentIndex] = archived
            }

            return {
                ...state,
                data:{
                    ...state.data,
                    comments: myCommentsClone
                }
            }
        }

        case TOGGLE_POST_ARCHIVE_STATUS:{
            const archived = action.payload.feed;
            let myPostsClone = JSON.parse(JSON.stringify(state.data.posts));

            const feedId = myPostsClone.findIndex(comment => comment.id === archived.id);
            if(feedId !== -1){
                myPostsClone[feedId] = archived
            }

            return {
                ...state,
                data:{
                    ...state.data,
                    posts: myPostsClone
                }
            }
        }

        case DELETE_COMMENT:{
            const deletedCommentId = action.payload.deleted_comment_id;
            let myCommentsClone = JSON.parse(JSON.stringify(state.data.comments));

            const commentIndex = myCommentsClone.findIndex(comment => comment.id === deletedCommentId);
            if(commentIndex !== -1){
                myCommentsClone.splice(commentIndex, 1);
            }

            return {
                ...state,
                data:{
                    ...state.data,
                    comments: myCommentsClone
                }
            }
        }

        case DELETE_POST:{
            let feedIndex = -1;
            const deletedPostId = action.payload.deleted_post_id;
            let myPostsClone = JSON.parse(JSON.stringify(state.data.posts));

            feedIndex = myPostsClone.findIndex(post => post.id === deletedPostId);

            if(feedIndex !== -1){
                myPostsClone.splice(feedIndex, 1);
            }

            return {
                ...state,
                data:{
                    ...state.data,
                    posts: myPostsClone
                }
            }
        }

        case BOOKMARK_FEED: {
            let fIndex = -1; ;
            const payload = action.payload;
            const feedId = payload.feedId;
            const followers = payload.followers;

            let savedFeedsClone = JSON.parse(JSON.stringify(state.data.saved_posts));

            if (Array.isArray(savedFeedsClone))
                fIndex = savedFeedsClone.findIndex(feed => feed.id === feedId);

            if (fIndex !== -1) {
                savedFeedsClone[fIndex].followers_ids = followers;

                savedFeedsClone.splice(fIndex, 1);
            }
            return {
                ...state,
                data: {
                    ...state.data,
                    saved_posts: savedFeedsClone
                },
            }
        }

        case VOTE_FEED: {
            let fIndex = -1;
            const payload = action.payload;
            const feedId = payload.feedId;
            const upVotes = payload.upVotes;
            const downVotes = payload.downVotes;

            let savedFeedsClone = JSON.parse(JSON.stringify(state.data.saved_posts));

            fIndex = savedFeedsClone.findIndex(feed => feed.id === feedId);

            if (fIndex !== -1) {
                savedFeedsClone[fIndex].up_voters_ids = upVotes;
                savedFeedsClone[fIndex].down_voters_ids = downVotes;
            }

            return {
                ...state,
                data: {
                    ...state.data,
                    saved_posts: savedFeedsClone,
                }
            }
        }

        case BOOKMARK_COMMENT: {
            let cIndex = -1;

            const payload = action.payload;
            const followers = payload.followers;
            const commentId = payload.commentId;

            let savedCommentsClone = JSON.parse(JSON.stringify(state.data.saved_comments));

            if (Array.isArray(savedCommentsClone))
                cIndex = savedCommentsClone.findIndex(comment => comment.id === commentId);

            if (cIndex !== -1) {
                savedCommentsClone[cIndex].followers_ids = followers;
                savedCommentsClone.splice(cIndex, 1);
            }

            return {
                ...state,
                data: {
                    ...state.data,
                    saved_comments: savedCommentsClone
                },
            }
        }

        case VOTE_COMMENT: {
            let cIndex = -1;

            const payload = action.payload;
            const commentId = payload.commentId;
            const upVotes = payload.upVotes;
            const downVotes = payload.downVotes;

            let savedCommentsClone = JSON.parse(JSON.stringify(state.data.saved_comments));

            if (Array.isArray(savedCommentsClone))
                cIndex = savedCommentsClone.findIndex(comment => comment.id === commentId);

            if (cIndex !== -1){
                savedCommentsClone[cIndex].up_voters_ids = upVotes;
                savedCommentsClone[cIndex].down_voters_ids = downVotes;
            }

            return {
                ...state,
                data: {
                    ...state.data,
                    saved_comments: savedCommentsClone
                },
            }
        }

        default:
            return state;
    }
};
