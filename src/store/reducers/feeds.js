import {
    BOOKMARK_COMMENT,
    BOOKMARK_FEED, COMMENT_ON_COMMENT, COMMENT_ON_FEED,
    FETCH_FEEDS,
    SET_ACTIVE_COMMENT,
    SET_ACTIVE_FEED,
    SET_COMMENT_BODY,
    SET_ITEM_TO_COMMENT,
    VOTE_COMMENT,
    VOTE_FEED,
} from '../utilities/actionTypes';

const initialState = {
    feeds:{
        isHomeFeedsFetched: false,
        isCommmunityFeedsFetched: false,
        homeFeeds:[],
        communityFeeds:[],
    },
    activeFeed: {
        homeFeedDetail:{comments:[]},
        communityFeedDetail:{comments:[]},
        myProfileFeedDetail:{comments:[]}
    },
    commenting:{
        commentOnFeed:{},
        commentOnComment:{},
    },
    activeComment:{comments:[]},
    commentBody: '',
};

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_FEEDS: {
            const payload = action.payload;
            const route = payload.route;
            const feeds = payload.feeds;
            let hFeedsStatus = state.feeds.isHomeFeedsFetched;
            let cFeedsStatus = state.feeds.isCommmunityFeedsFetched;

            if (!hFeedsStatus && route === 'homeFeeds')
                hFeedsStatus = true;

            if (!cFeedsStatus && route === 'communityFeeds')
                cFeedsStatus = true;

            return {
                ...state,
                feeds: {
                    ...state.feeds,
                    isHomeFeedsFetched: hFeedsStatus,
                    isCommmunityFeedsFetched: cFeedsStatus,
                    [route]: feeds
                }
            }
        }

        case SET_ACTIVE_FEED: {
            const payload = action.payload;
            const route = payload.route;
            const activeFeed = payload.feed;

            return {
                ...state,
                activeFeed: {
                    ...state.activeFeed,
                    [route]: activeFeed
                }
            }
        }

        case SET_ACTIVE_COMMENT: {
            const payload = action.payload;
            const activeComment = payload.comment;
            return {
                ...state,
                activeComment: activeComment
            }
        }

        case SET_ITEM_TO_COMMENT:{
            const payload = action.payload;
            const route = payload.route;
            const itemToComment = payload.item;

            return {
                ...state,
                commenting: {
                    ...state.commenting,
                    [route]: itemToComment
                }
            }
        }

        case SET_COMMENT_BODY:{
            return {
                ...state,
                commentBody: action.payload
            }
        }

        case BOOKMARK_FEED: {
            let hFeedsIndex = -1; let cFeedsIndex = -1;
            const payload = action.payload;
            const feedId = payload.feedId;
            const followers = payload.followers;

            let homeFeedsClone = JSON.parse(JSON.stringify(state.feeds.homeFeeds));
            let communityFeedsClone = JSON.parse(JSON.stringify(state.feeds.communityFeeds));
            let activeOnHome = JSON.parse(JSON.stringify(state.activeFeed.homeFeedDetail));
            let activeOnCommunity = JSON.parse(JSON.stringify(state.activeFeed.communityFeedDetail));
            let activeOnProfile = JSON.parse(JSON.stringify(state.activeFeed.myProfileFeedDetail));

            if (Array.isArray(homeFeedsClone))
                hFeedsIndex = homeFeedsClone.findIndex(feed => feed.id === feedId);

            if (Array.isArray(communityFeedsClone))
                cFeedsIndex = communityFeedsClone.findIndex(feed => feed.id === feedId);

            if (hFeedsIndex !== -1)
                homeFeedsClone[hFeedsIndex].followers_ids = followers;

            if (cFeedsIndex !== -1)
                communityFeedsClone[cFeedsIndex].followers_ids = followers;

            if (activeOnHome.id === feedId)
                activeOnHome.followers_ids = followers;

            if (activeOnCommunity.id === feedId)
                activeOnCommunity.followers_ids = followers;

            if (activeOnProfile.id === feedId)
                activeOnProfile.followers_ids = followers;
            return {
                ...state,
                feeds: {
                    ...state.feeds,
                    homeFeeds: homeFeedsClone,
                    communityFeeds: communityFeedsClone,
                },
                activeFeed: {
                    ...state.activeFeed,
                    homeFeedDetail: activeOnHome,
                    communityFeedDetail: activeOnCommunity,
                    myProfileFeedDetail: activeOnProfile
                }
            }
        }

        case VOTE_FEED: {
            const payload = action.payload;
            const feedId = payload.feedId;
            const upVotes = payload.upVotes;
            const downVotes = payload.downVotes;

            let homeFeedsClone = JSON.parse(JSON.stringify(state.feeds.homeFeeds));
            let communityFeedsClone = JSON.parse(JSON.stringify(state.feeds.communityFeeds));
            let activeOnHome = JSON.parse(JSON.stringify(state.activeFeed.homeFeedDetail));
            let activeOnCommunity = JSON.parse(JSON.stringify(state.activeFeed.communityFeedDetail));
            let activeOnProfile = JSON.parse(JSON.stringify(state.activeFeed.myProfileFeedDetail));

            let hFeedsIndex = homeFeedsClone.findIndex(feed => feed.id === feedId);
            let cFeedsIndex = communityFeedsClone.findIndex(feed => feed.id === feedId);

            if (hFeedsIndex !== -1) {
                homeFeedsClone[hFeedsIndex].up_voters_ids = upVotes;
                homeFeedsClone[hFeedsIndex].down_voters_ids = downVotes;
            }

            if (cFeedsIndex !== -1){
                communityFeedsClone[cFeedsIndex].up_voters_ids = upVotes;
                communityFeedsClone[cFeedsIndex].down_voters_ids = downVotes;
            }

            if (activeOnHome.id === feedId){
                activeOnHome.up_voters_ids = upVotes;
                activeOnHome.down_voters_ids = downVotes;
            }

            if (activeOnCommunity.id === feedId){
                activeOnCommunity.up_voters_ids = upVotes;
                activeOnCommunity.down_voters_ids = downVotes;
            }

            if (activeOnProfile.id === feedId){
                activeOnProfile.up_voters_ids = upVotes;
                activeOnProfile.down_voters_ids = downVotes;
            }

            return {
                ...state,
                feeds: {
                    ...state.feeds,
                    homeFeeds: homeFeedsClone,
                    communityFeeds: communityFeedsClone,
                },
                activeFeed: {
                    ...state.activeFeed,
                    homeFeedDetail: activeOnHome,
                    communityFeedDetail: activeOnCommunity,
                    myProfileFeedDetail: activeOnProfile
                }
            }
        }

        case BOOKMARK_COMMENT: {
            let hFeedsIndex = -1; let cFeedsIndex = -1; let pFeedsIndex = -1; let cmFeedsIndex = -1;

            const payload = action.payload;
            const followers = payload.followers;
            const commentId = payload.commentId;

            let commentsForActiveHomeFeed = JSON.parse(JSON.stringify(state.activeFeed.homeFeedDetail.comments));
            let commentsForActiveCommunityFeed = JSON.parse(JSON.stringify(state.activeFeed.communityFeedDetail.comments));
            let commentsForActiveProfileFeed = JSON.parse(JSON.stringify(state.activeFeed.myProfileFeedDetail.comments));
            let activeComment = JSON.parse(JSON.stringify(state.activeComment));
            let commentsForActiveComment = JSON.parse(JSON.stringify(state.activeComment ? state.activeComment.comments : []));

            if (Array.isArray(commentsForActiveHomeFeed))
                hFeedsIndex = commentsForActiveHomeFeed.findIndex(comment => comment.id === commentId);

            if (Array.isArray(commentsForActiveCommunityFeed))
                cFeedsIndex = commentsForActiveCommunityFeed.findIndex(comment => comment.id === commentId);

            if (Array.isArray(commentsForActiveProfileFeed))
                pFeedsIndex = commentsForActiveProfileFeed.findIndex(comment => comment.id === commentId);

            if (Array.isArray(commentsForActiveComment))
                cmFeedsIndex = commentsForActiveComment.findIndex(comment => comment.id === commentId);

            if (hFeedsIndex !== -1)
                commentsForActiveHomeFeed[hFeedsIndex].followers_ids = followers;

            if (cFeedsIndex !== -1)
                commentsForActiveCommunityFeed[cFeedsIndex].followers_ids = followers;

            if (pFeedsIndex !== -1)
                commentsForActiveProfileFeed[pFeedsIndex].followers_ids = followers;

            if (cmFeedsIndex !== -1)
                commentsForActiveComment[cmFeedsIndex].followers_ids = followers;

            if (activeComment.id === commentId)
                activeComment.followers_ids = followers;

            return {
                ...state,
                activeFeed: {
                    ...state.activeFeed,
                    homeFeedDetail: {
                        ...state.activeFeed.homeFeedDetail,
                        comments: commentsForActiveHomeFeed
                    },
                    communityFeedDetail: {
                        ...state.activeFeed.communityFeedDetail,
                        comments: commentsForActiveCommunityFeed
                    },
                    myProfileFeedDetail: {
                        ...state.activeFeed.myProfileFeedDetail,
                        comments: commentsForActiveProfileFeed
                    }
                },
                activeComment: {
                    ...activeComment,
                    comments: commentsForActiveComment
                }
            }
        }

        case VOTE_COMMENT: {
            let hFeedsIndex = -1; let cFeedsIndex = -1; let pFeedsIndex = -1; let cmFeedsIndex = -1;

            const payload = action.payload;
            const commentId = payload.commentId;
            const upVotes = payload.upVotes;
            const downVotes = payload.downVotes;

            let commentsForActiveHomeFeed = JSON.parse(JSON.stringify(state.activeFeed.homeFeedDetail.comments));
            let commentsForActiveCommunityFeed = JSON.parse(JSON.stringify(state.activeFeed.communityFeedDetail.comments));
            let commentsForActiveProfileFeed = JSON.parse(JSON.stringify(state.activeFeed.myProfileFeedDetail.comments));
            let activeComment = JSON.parse(JSON.stringify(state.activeComment));

            let commentsForActiveComment = JSON.parse(JSON.stringify(state.activeComment ? state.activeComment.comments : []));

            if (Array.isArray(commentsForActiveHomeFeed))
                hFeedsIndex = commentsForActiveHomeFeed.findIndex(comment => comment.id === commentId);

            if (Array.isArray(commentsForActiveCommunityFeed))
                cFeedsIndex = commentsForActiveCommunityFeed.findIndex(comment => comment.id === commentId);

            if (Array.isArray(commentsForActiveProfileFeed))
                pFeedsIndex = commentsForActiveProfileFeed.findIndex(comment => comment.id === commentId);

            if (Array.isArray(commentsForActiveComment))
                cmFeedsIndex = commentsForActiveComment.findIndex(comment => comment.id === commentId);

            if (hFeedsIndex !== -1) {
                commentsForActiveHomeFeed[hFeedsIndex].up_voters_ids = upVotes;
                commentsForActiveHomeFeed[hFeedsIndex].down_voters_ids = downVotes;
            }

            if (cFeedsIndex !== -1){
                commentsForActiveCommunityFeed[cFeedsIndex].up_voters_ids = upVotes;
                commentsForActiveCommunityFeed[cFeedsIndex].down_voters_ids = downVotes;
            }

            if (pFeedsIndex !== -1){
                commentsForActiveProfileFeed[pFeedsIndex].up_voters_ids = upVotes;
                commentsForActiveProfileFeed[pFeedsIndex].down_voters_ids = downVotes;
            }

            if (cmFeedsIndex !== -1){
                commentsForActiveComment[cmFeedsIndex].up_voters_ids = upVotes;
                commentsForActiveComment[cmFeedsIndex].down_voters_ids = downVotes;
            }

            if (activeComment.id === commentId) {
                activeComment.up_voters_ids = upVotes;
                activeComment.down_voters_ids = downVotes
            }

            return {
                ...state,
                activeFeed: {
                    ...state.activeFeed,
                    homeFeedDetail: {
                        ...state.activeFeed.homeFeedDetail,
                        comments: [...commentsForActiveHomeFeed]
                    },
                    communityFeedDetail: {
                        ...state.activeFeed.communityFeedDetail,
                        comments: [...commentsForActiveCommunityFeed]
                    },
                    myProfileFeedDetail: {
                        ...state.activeFeed.myProfileFeedDetail,
                        comments: [...commentsForActiveProfileFeed]
                    },
                },
                activeComment: {
                    ...activeComment,
                    comments: [...commentsForActiveComment]
                }
            }
        }

        case COMMENT_ON_FEED: {
            let hFeedsIndex = -1; let cFeedsIndex = -1;
            const payload = action.payload;
            const commentedFeed = payload.feed;

            let homeFeedsClone = JSON.parse(JSON.stringify(state.feeds.homeFeeds));
            let communityFeedsClone = JSON.parse(JSON.stringify(state.feeds.communityFeeds));
            let activeOnHome = JSON.parse(JSON.stringify(state.activeFeed.homeFeedDetail));
            let activeOnCommunity = JSON.parse(JSON.stringify(state.activeFeed.communityFeedDetail));
            let activeOnProfile = JSON.parse(JSON.stringify(state.activeFeed.myProfileFeedDetail));

            hFeedsIndex = homeFeedsClone.findIndex(feed => feed.id === commentedFeed.id);
            cFeedsIndex = communityFeedsClone.findIndex(feed => feed.id === commentedFeed);

            if (hFeedsIndex !== -1)
                homeFeedsClone[hFeedsIndex] = commentedFeed;

            if (cFeedsIndex !== -1)
                communityFeedsClone[cFeedsIndex] = commentedFeed;

            if (activeOnHome.id === commentedFeed.id)
                activeOnHome = commentedFeed;

            if (activeOnCommunity.id === commentedFeed.id)
                activeOnCommunity = commentedFeed;

            if (activeOnProfile.id === commentedFeed.id)
                activeOnProfile = commentedFeed;

            return {
                ...state,
                feeds: {
                    ...state.feeds,
                    homeFeeds: homeFeedsClone,
                    communityFeeds: communityFeedsClone,
                },
                activeFeed: {
                    ...state.activeFeed,
                    homeFeedDetail: activeOnHome,
                    communityFeedDetail: activeOnCommunity,
                }
            }
        }

        case COMMENT_ON_COMMENT: {
            const payload = action.payload;
            const commentedComment = payload.comment;
            let activeCommentClone = JSON.parse(JSON.stringify(state.activeComment));

            if (activeCommentClone.id === commentedComment.id)
                activeCommentClone = commentedComment;

            return {
                ...state,
                activeComment: activeCommentClone
            }
        }

        default:
            return state;
    }
};
