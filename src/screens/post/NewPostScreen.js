import React, { PureComponent} from 'react';
import {connect } from "react-redux";
import {StyleSheet, View, Picker, TextInput } from 'react-native';
import {Divider, Icon} from "react-native-elements";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ColorDefinitions from '../../constants/ColorDefinitions';
import TypoDefinitions from '../../constants/TypoDefinitions';
import Helpers from '../../utilities/Helpers';
import {fetchCommunities, setPostControls, submitPost} from '../../store/utilities/actionsCollection';
import {SkypeIndicator} from 'react-native-indicators';
import CommunityTypes from '../../constants/CommunityTypes';

class NewPostScreen extends PureComponent{
    static navigationOptions = ({ navigation }) => {
        const params = navigation.state.params || {};

        return {
            title: params.title ? params.title : 'New Post',
            headerTintColor: ColorDefinitions.tintColor,
            //headerStyle: { backgroundColor: ColorDefinitions.header.background },
            headerTitleStyle: { fontWeight: TypoDefinitions.ultraLightText },
            headerRight: (
                <Icon
                    onPress={ () => params.handlePostSubmission() }
                    type='font-awesome'
                    name='paper-plane'
                    color={ColorDefinitions.tintColor}
                    iconStyle={{paddingRight:20}}/>
            ),
        };
    };

    componentDidMount = async () => {
        const { navigation } = this.props;

        navigation.setParams({
            handlePostSubmission: this._submitPost
        });

        this.didFocusListener = navigation.addListener (
            'didFocus', async () => {
                if (!this.props.communities || !this.props.communities.length) await this._fetchCommunities();
                this._setInputsData();
            },
        );
    };

    render() {
        const {navigation} = this.props;
        const post = navigation.getParam('toEdit', null);

        const user = this.props.loginPayload.user;
        const communities = this.props.communities;
        const categories = this.props.categories;
        let categoriesPicker = [];
        let communitiesPicker = [];

        if(!communities){
            return <SkypeIndicator/>
        }

        if (communities) {
            const subscribed = communities.filter((community) => {
                return (community.user_ids.find(userId => userId === user.id))
            });

            communitiesPicker = subscribed.map((community) => {
                return <Picker.Item key={community.id.toString()}
                                    value={community}
                                    label={Helpers.titleCaseString(community.name)}/>
            });
        }

        if (categories)
            categoriesPicker = categories.map((category) => {
                return <Picker.Item key={category.id.toString()}
                                    value={category}
                                    label={Helpers.titleCaseString(category.name)}/>
            });

        return (
            <KeyboardAwareScrollView>
                <View style={styles.container}>
                    {!post && <View>
                        <View style={styles.genericSpacing}>
                            <Picker
                                selectedValue={ this.props.np_community }
                                mode='dropdown'
                                onValueChange={ (community) => this._updateInputState('np_community', community)} >

                                <Picker.Item label="Please Select Community" value={null} />
                                {communitiesPicker}
                            </Picker>
                        </View>
                        <Divider/>
                        <View style={styles.genericSpacing}>
                            <Picker
                                dis
                                selectedValue={ this.props.np_category }
                                mode='dropdown'
                                onValueChange={ (category) => this._updateInputState('np_category', category) } >

                                <Picker.Item label="Please Select Category" value={null} />
                                {categoriesPicker}
                            </Picker>
                        </View>
                        <Divider/>
                    </View>}
                    <View style={styles.genericSpacing}>
                        <TextInput
                            {...this.props}
                            value = {this.props.np_title}
                            multiline={true}
                            blurOnSubmit={false}
                            placeholder='Enter an interesting title'
                            onChangeText={(val) => this._updateInputState('np_title', val)} />
                    </View>
                    <Divider/>
                    <View style={styles.genericSpacing}>
                        <TextInput
                            {...this.props}
                            editable = {true}
                            autogrow={true}
                            value ={this.props.np_body}
                            multiline={true}
                            style={{maxHeight: 320 }}
                            placeholder='Type the full description here...'
                            onChangeText={(val) => this._updateInputState('np_body', val) }/>
                    </View>
                </View>
            </KeyboardAwareScrollView>
        );
    }

    _fetchCommunities = () => {
        this.props.fetchCommunities();
    };

    _setInputsData = () =>{
        const {navigation} = this.props;
        const inputs = ['post_id','np_category','np_community','np_title','np_body'];
        const post = navigation.getParam('toEdit', null);

        inputs.forEach(input=>this._updateInputState(input, null));

        if(post){
            const data = {
                post_id: post.id,
                np_community: post.community,
                np_category: post.category,
                np_title: post.title,
                np_body: post.body
            };
            Object.keys(data).forEach(key=> this._updateInputState(key, data[key]));
        }

    };

    _updateInputState = (inputKey, inputValue) => {
        const inputObject = {
            key: inputKey,
            value: inputValue
        };
        this.props.handleInputControls(inputObject)
    };

    _submitPost = async ( )  => {
        const { navigation } = this.props;
        const post = navigation.getParam('toEdit', null);

        if( this.props.np_community && this.props.np_category && this.props.np_title && this.props.np_body ) {
            const newPost = {
                post_id: this.props.post_id,
                title: this.props.np_title,
                body: this.props.np_body,
                category: this.props.np_category.id,
                community: this.props.np_community.id
            };
            await this.props.submitPost(newPost);

            if (post)
                navigation.goBack();
            else
                navigation.navigate('homeFeeds');
        }
        else
            alert('All fields need to be completed');
    }
}

const styles = StyleSheet.create({
    container:{
        paddingTop:10,
        paddingBottom:10,
    },

    genericSpacing:{
        paddingLeft: 10,
        paddingRight: 10,
    },

    spinners: { }
});

const mapStateToProps = (state) => {
    return {
        busyStatus: state.globalReducer.busyStatus,
        loginPayload: state.authReducer.loginPayload,
        communities: state.communitiesReducer.communities.available,

        categories : state.postsReducer.categories,
        post_id: state.postsReducer.post_id,
        np_community: state.postsReducer.np_community,
        np_category: state.postsReducer.np_category,
        np_title: state.postsReducer.np_title,
        np_body: state.postsReducer.np_body,
    }
};

const matchDispatchToProps = dispatch => {
    return {
        fetchCommunities: ()=>dispatch(fetchCommunities()),
        handleInputControls: (inputObject) => dispatch( setPostControls(inputObject)),
        submitPost: (post) => dispatch (submitPost( post )),
    }
};
export default connect(mapStateToProps, matchDispatchToProps)(NewPostScreen);
