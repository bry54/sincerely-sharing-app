import React, { PureComponent } from 'react';
import {
    StyleSheet,
    Text,
    View,
    FlatList, Alert,
} from 'react-native';
import {connect } from "react-redux";
import {Divider, Input} from "react-native-elements";
import CommentListItem from "../../components/comments/MyCommentItem";
import EmptyListItem from "../../components/EmptyList";
import NoScreenData from "../../components/NoScreenData";
import {
    setItemToComment,
    setSearchInput,
    toggleCommentArchiveStatus,
    deleteComment, setActiveComment, fetchProfileData,
} from '../../store/utilities/actionsCollection';
import ColorDefinitions from "../../constants/ColorDefinitions";

class MyCommentsScreen extends PureComponent {

    render() {
        const myComments = this.props.myComments;
        const profileFetched = this.props.profileFetched;
        const busyStatus = this.props.busyStatus;

        if (!profileFetched )
            return (
                <NoScreenData
                    code={busyStatus.code}
                    message={busyStatus.message ? busyStatus.message : 'Fetching comments you have created'}
                    onExecRefresh={() => this._fetchProfileData()}
                />
            );

        return (
            <View style={styles.container}>
                <Input
                    placeholder={'Type to search'}
                    onChangeText={(val) => this._updateSearchInput(val) }/>
                <FlatList
                    keyExtractor={(item) => item.id.toString()}
                    data={ myComments.filter(item =>
                        (item.body.toLowerCase()).includes(this.props.searchValue.toLowerCase())
                    )}
                    renderItem={(item) => (this.renderComment(item))}
                    ListEmptyComponent={<EmptyListItem
                        icon='ios-quote'
                        message='You have not commented on anything, your comments will appear here.'/>
                    }
                />
            </View>
        )
    }

    renderComment = ( {item} ) => (
        <View>
            <CommentListItem
                comment={item}
                onOpenComment={this._openComment}
                onDeletePress={this._onDeletePress}
                onEditPress={this._onEditPress}
                onArchivePress={this._onArchivePress}
            />
            <Divider/>
        </View>
    );

    _fetchProfileData = () =>{
        this.props.fetchProfileData();
    };

    _openComment = async (comment) =>{
        const { navigation } = this.props;

        await this.props.setActiveComment(comment);
        navigation.navigate('commentAsPost');
    };

    _onEditPress = async (comment) =>{
        const {navigation} = this.props;

        if(comment.post){
            await this.props.setItemToComment('commentOnFeed', comment.post);
            navigation.navigate('commentOnFeed', {toEdit: comment, title: 'Update Comment'});
        } else if (comment.parent_comment){
            await this.props.setItemToComment('commentOnComment', comment.parent_comment);
            navigation.navigate('commentOnComment', {toEdit: comment, title: 'Update Comment'});
        } else {
            Alert.alert('Error', 'Item was orphaned, it has no parent')
        }
    };

    _onArchivePress =(comment)=>{
        const title  = 'Toggle comment visibility';
        const msg = comment.is_archived ?
            'Un-archiving will make this comment available general audience. Do you wish to proceed?'
            :
            'Archiving will hide this comment from general audience. Do you wish to proceed?';
        Alert.alert(
            title,
            msg,
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                },
                {
                    text: 'Yes, proceed',
                    onPress: () => this.props.toggleCommentArchiveStatus(comment)},
            ],
            {cancelable: false},
        );
    };

    _onDeletePress =(comment)=>{
        Alert.alert(
            'Delete Comment',
            'Deleting will completely erase comment from your owned content, and make it inaccessible to other users. Do you wish to proceed?',
            [
                {
                    text: 'Cancel',
                    style: 'cancel',
                    onPress: () => console.log('Cancel Pressed'),
                },
                {text: 'Yes, delete comment', onPress: () => this.props.deleteComment(comment)},
            ],
            {cancelable: false},
        );
    };

    _updateSearchInput = (val) =>{
        this.props.setSearchInputs({key: 'myComments', value:val});
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

const mapStateToProps = (state) => {
    return {
        profileFetched: state.profileReducer.profileFetched,
        busyStatus: state.globalReducer.busyStatus,

        searchValue: state.profileReducer.search.myComments,
        myComments: state.profileReducer.data.comments
    }
};

const matchDispatchToProps = dispatch => {
    return {
        fetchProfileData: () => dispatch(fetchProfileData()),
        setSearchInputs: (ctrlObj) => dispatch(setSearchInput(ctrlObj)),
        setItemToComment: (route, feed)=>dispatch( setItemToComment(route, feed)),

        setActiveComment: (comment)=> dispatch(setActiveComment(comment)),
        toggleCommentArchiveStatus: (comment) => dispatch(toggleCommentArchiveStatus(comment)),
        deleteComment: (comment) => dispatch(deleteComment(comment))
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(MyCommentsScreen);
