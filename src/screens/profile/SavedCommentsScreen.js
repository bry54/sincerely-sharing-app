import React, { PureComponent } from 'react';
import {
    StyleSheet,
    View,
    FlatList, Alert,
} from 'react-native';
import {connect } from "react-redux";
import { Divider, Input } from "react-native-elements";
import CommentListItem from "../../components/comments/CommentListItem";
import ColorDefinitions from "../../constants/ColorDefinitions";
import EmptyListItem from '../../components/EmptyList';
import NoScreenData from "../../components/NoScreenData";
import Snackbar from 'react-native-snackbar';
import {
    bookmarkComment,
    fetchProfileData,
    setActiveComment,
    setSearchInput,
    voteComment
} from '../../store/utilities/actionsCollection';
import HTTPCodes from "../../constants/HTTPCodes";
import UnavailableListItem from '../../components/UnavailableListItem';

class SavedCommentsScreen extends PureComponent {

    render() {
        const savedComments = this.props.savedComments;
        const profileFetched = this.props.profileFetched;
        const busyStatus = this.props.busyStatus;

        if (!profileFetched )
            return (
                <NoScreenData
                    code={busyStatus.code}
                    message={busyStatus.message ? busyStatus.message : 'Fetching comments you have created'}
                    onExecRefresh={() => this._fetchProfileData()}
                />
            );

        return (
            <View style={styles.container}>
                <Input
                    placeholder={'Type to search'}
                    onChangeText={(val) => this._updateSearchInput(val) }/>
                <FlatList
                    keyExtractor={(item) => item.id.toString()}
                    data={ savedComments.filter(item =>
                        (item.body.toLowerCase()).includes(this.props.searchValue.toLowerCase())
                    )}
                    renderItem={(item) => (this.renderSavedComment(item))}
                    ListEmptyComponent={<EmptyListItem
                        icon='ios-bookmark'
                        message='You have not saved any comments yet, saved comments will appear here.'/>
                    }
                />
            </View>
        )
    }

    renderSavedComment = ( {item} ) => {
        if (item.is_archived || item.is_deleted)
            return (
                <View>
                    <UnavailableListItem
                        item={item}
                        onOpenItem={this._openUnavailableComment}
                        onDeletePress={this._onCommentBookmarkPress}
                    />
                    <Divider/>
                </View>
            );

        return ( <View>
            <CommentListItem
                comment={item}
                isCommentBookmarked={this._isCommentBookmarked}
                isCommentUpVoted={this._isCommentUpVoted}
                isCommentDownVoted={this._isCommentDownVoted}
                onOpenComment={this._onOpenComment}
                onToggleCommentVote={this._toggleCommentVote}
                onCommentBookmarkPress={this._onCommentBookmarkPress}
            />
            <Divider/>
        </View>);
    };

    _fetchProfileData = () =>{
        this.props.fetchProfileData();
    };

    _updateSearchInput = (val) =>{
        this.props.setSearchInputs({key:'savedComments', value:val});
    };

    _isCommentBookmarked = (comment) =>{
        const user = this.props.loginPayload.user;
        const followers = comment.followers_ids;
        if (followers.find((follower)=> follower === user.id))
            return ColorDefinitions.interactions.includesUser;
        else
            return ColorDefinitions.interactions.idle;
    };

    _isCommentUpVoted = (comment) =>{
        const user = this.props.loginPayload.user;
        const downVoters = comment.up_voters_ids;
        if (downVoters.find((voter) => voter === user.id))
            return ColorDefinitions.interactions.includesUser;
        else
            return ColorDefinitions.interactions.idle;
    };

    _isCommentDownVoted = (comment) =>{
        const user = this.props.loginPayload.user;
        const downVoters = comment.down_voters_ids;
        if (downVoters.find((voter) => voter === user.id))
            return ColorDefinitions.interactions.includesUser;
        else
            return ColorDefinitions.interactions.idle;
    };

    _onOpenComment = async (comment) => {
        const { navigation } = this.props;

        await this.props.setActiveComment(comment);
        navigation.navigate('commentAsPost');
    };

    _openUnavailableComment = (alertObj) => {
        Alert.alert(
            alertObj.title,
            alertObj.message,
            [
                {
                    text: 'Close',
                    style: 'cancel',
                    onPress: () => console.log('Cancel Pressed'),
                },
            ],
            {cancelable: false},
        );
    };

    _onCommentBookmarkPress = (comment) => {
        this.props.bookmarkComment(comment);
        const success = this.props.busyStatus.code === HTTPCodes.OK;

        Snackbar.show({
            backgroundColor: ColorDefinitions.snackBar.genericBackground,
            color: 'white',
            title: success ? 'Comment removed list' : 'Error occurred',
            duration: Snackbar.LENGTH_LONG
        });

    };

    _toggleCommentVote =(vote) =>{
        this.props.voteComment(vote)
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

const mapStateToProps = (state) => {
    return {
        profileFetched: state.profileReducer.profileFetched,
        loginPayload: state.authReducer.loginPayload,
        busyStatus: state.globalReducer.busyStatus,

        searchValue: state.profileReducer.search.savedComments,
        savedComments: state.profileReducer.data.saved_comments
    }
};

const matchDispatchToProps = dispatch => {
    return {
        fetchProfileData: () => dispatch(fetchProfileData()),
        setSearchInputs: (ctrlObj) => dispatch(setSearchInput(ctrlObj)),
        bookmarkComment: (comment)=> dispatch(bookmarkComment(comment)),
        voteComment: (vote)=>dispatch( voteComment(vote)),
        setActiveComment: (comment)=> dispatch(setActiveComment(comment)),
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(SavedCommentsScreen);
