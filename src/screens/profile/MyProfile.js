import React, { PureComponent } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView, RefreshControl,
    Platform
} from 'react-native';

import {Button, Icon} from 'react-native-elements';
import {connect} from "react-redux";
import moment from "moment";
import TypoDefinitions from '../../constants/TypoDefinitions';
import {USER} from '../../assets/images'
import {fetchProfileData, attemptLogout} from "../../store/utilities/actionsCollection";
import ColorDefinitions from '../../constants/ColorDefinitions';
import {SkypeIndicator} from 'react-native-indicators';
import HTTPCodes from "../../constants/HTTPCodes";
import Share from 'react-native-share';

class MyProfile extends PureComponent {

    componentDidMount(): void{
        const {navigation} = this.props;

        this.didFocusListener = navigation.addListener (
            'didFocus', () => {
                if(!this.props.profileFetched) this._fetchProfileData();
            },
        );
    }

    render() {
        const fetched = this.props.profileFetched;
        const busyStatus = this.props.busyStatus;

        const user = fetched ? this.props.profileData : this.props.loginPayload.user;

        return (
            <ScrollView
                refreshControl={<RefreshControl
                    refreshing={ busyStatus.isBusy }
                    onRefresh={ () => this._fetchProfileData()} />
                }
                style={styles.container}>
                <View style={styles.headerContainer}>
                    <View style={styles.userRow}>
                        <Image
                            style={styles.userImage}
                            source={ USER }
                        />
                        <View style={styles.userNameRow}>
                            <Text style={styles.userNameText}>{user.username}</Text>
                        </View>
                        <View style={styles.userBioRow}>
                            <Text style={styles.userBioText}>User since {moment(user.created_at).format('lll')} </Text>
                        </View>
                    </View>
                    <View style={styles.socialRow}>
                        <View>
                            <Icon
                                size={30}
                                type="ionicon"
                                color="#3B5A98"
                                name={Platform.OS === 'ios' ? "ios-share" : 'md-share'}
                                onPress={() => this._shareApp()}/>
                        </View>
                    </View>
                </View>
                <View style={styles.tabBar}>
                    <View>
                        <Text style={[styles.tabLabelText, { color: 'black' }]}>
                            {fetched ? user.readable_age : '---'}
                        </Text>
                        <Text style={[styles.tabLabelNumber, { color: ColorDefinitions.cards.idle }]}>
                            {'InApp Age'}
                        </Text>
                    </View>

                    <View>
                        <Text style={[styles.tabLabelText, { color: 'black' }]}>
                            {fetched ? user.reputation : '---'}
                        </Text>
                        <Text style={[styles.tabLabelNumber, { color: ColorDefinitions.cards.idle }]}>
                            {'Reputation'}
                        </Text>
                    </View>
                    <View>
                        <Text style={[styles.tabLabelText, { color: 'black' }]}>
                            {fetched ? user.posts_count : '---'}
                        </Text>
                        <Text style={[styles.tabLabelNumber, { color: ColorDefinitions.cards.idle }]}>
                            {'Posts'}
                        </Text>
                    </View>
                    <View>
                        <Text style={[styles.tabLabelText, { color: 'black' }]}>
                            {fetched ? user.comments_count : '---'}
                        </Text>
                        <Text style={[styles.tabLabelNumber, { color: ColorDefinitions.cards.idle }]}>
                            {'Comments'}
                        </Text>
                    </View>
                </View>

                <View style={{flexDirection: 'row', marginVertical: 20, paddingHorizontal: 5, justifyContent: 'center'}}>
                    <Button
                        onPress={()=>this._attemptLogout()}
                        buttonStyle={{paddingLeft:10, paddingRight: 10}}
                        containerStyle={{marginRight: 15}}
                        title="Logout"
                    />

                    <Button
                        onPress={()=>this._attemptAccountDeletion()}
                        buttonStyle={{paddingLeft:10, paddingRight: 10}}
                        containerStyle={{marginLeft: 15}}
                        title="Delete Account"
                    />
                </View>

                { this.getInfoView() }
            </ScrollView>
        );
    }

    getInfoView = () =>{
        const busyStatus = this.props.busyStatus;

        if (busyStatus.code === HTTPCodes.GATEWAY_TIMEOUT) return (
            <View style={{marginTop: 20}}>
                <Text style={{textAlign: 'center'}}>{busyStatus.message}</Text>
            </View>
        );

        if (busyStatus.code === HTTPCodes.CREATED) return (
            <View style={{marginTop: 20}}>
                <SkypeIndicator size={20} color={ColorDefinitions.tintColor}/>
                <Text style={{textAlign: 'center'}}>Fetching profile data...</Text>
            </View>
        )
    };

    _fetchProfileData = () =>{
        this.props.fetchProfileData();
    };

    _attemptLogout = () =>{
        this.props.attemptLogout();
    };

    _attemptAccountDeletion = () =>{
        this.props.attemptLogout();
    };

    _shareApp = ( ) =>{
        let shareOptions = {
            title: 'Share via',
            message: 'Im using this app to engage with other people, discussing anonymously',
            url: 'www.google.com'
        };

        Share.open(shareOptions);
    };
}

const styles = StyleSheet.create({
    container:{
    },

    headerContainer: {
        alignItems: 'center',
        backgroundColor: '#FFF',
        marginVertical: 15,
    },

    socialIcon: {
        marginHorizontal: 20,
    },

    socialRow: {
        flexDirection: 'row',
    },

    tabBar: {
        padding: 10,
        backgroundColor: '#EEE',
        flexDirection: 'row',
        justifyContent: 'space-between',
        elevation: 1,
        marginBottom: 20
    },

    tabLabelNumber: {
        color: ColorDefinitions.cards.idle,
        fontSize: TypoDefinitions.xxSmallFont,
        textAlign: 'center',
    },
    tabLabelText: {
        color: 'black',
        fontSize: TypoDefinitions.bigFont,
        fontWeight: TypoDefinitions.semiBoldText,
        textAlign: 'center',
    },
    userBioRow: {
        marginHorizontal: 10
    },
    userBioText: {
        color: ColorDefinitions.cards.idle,
        fontSize: TypoDefinitions.xxSmallFont,
        textAlign: 'center',
    },
    userImage: {
        borderRadius: 60,
        height: 80,
        marginBottom: 10,
        width: 80,
    },
    userNameRow: {
        marginBottom: 10,
        flexDirection: 'row'
    },
    userNameText: {
        color: '#5B5A5A',
        fontSize: TypoDefinitions.xBigFont,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    userRow: {
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        marginBottom: 12,
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
});

const mapStateToProps = (state) => {
    return {
        profileFetched: state.profileReducer.profileFetched,
        profileData: state.profileReducer.data,
        loginPayload: state.authReducer.loginPayload,

        busyStatus: state.globalReducer.busyStatus,
    }
};

const matchDispatchToProps = dispatch => {
    return {
        fetchProfileData: () => dispatch(fetchProfileData()),
        attemptLogout: ()=>dispatch(attemptLogout())
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(MyProfile);
