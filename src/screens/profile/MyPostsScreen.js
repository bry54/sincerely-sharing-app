import React, { PureComponent } from 'react';
import {
    StyleSheet,
    View,
    FlatList,
    Alert,
} from 'react-native';
import {connect } from "react-redux";
import {
    setSearchInput,
    togglePostArchiveStatus,
    deletePost,
    setActiveFeed, fetchProfileData,
} from '../../store/utilities/actionsCollection';
import {Input} from "react-native-elements";
import MyFeedItem from "../../components/feeds/MyFeedItem";
import EmptyListItem from '../../components/EmptyList'
import NoScreenData from "../../components/NoScreenData";

class MyPostsScreen extends PureComponent {

    render() {
        const myPosts = this.props.myPosts;
        const profileFetched = this.props.profileFetched;
        const busyStatus = this.props.busyStatus;

        if (!profileFetched )
            return (
                <NoScreenData
                    code={busyStatus.code}
                    message={busyStatus.message ? busyStatus.message : 'Fetching comments you have created'}
                    onExecRefresh={() => this._fetchProfileData()}
                />
            );

        return (
            <View style={styles.container}>
                <Input
                    placeholder={'Type to search'}
                    onChangeText={(val) => this._updateSearchInput(val) }/>
                <FlatList
                    keyExtractor={(item) => item.id.toString()}
                    data={ myPosts.filter(item =>
                        (item.title.toLowerCase()).includes(this.props.searchValue.toLowerCase())
                    )}
                    renderItem={(item) => (this.renderPost(item))}
                    ListEmptyComponent={<EmptyListItem
                        icon='ios-paper'
                        message='You have not created any feed yet, your feeds will appear here.'/>
                    }
                />
            </View>
        )
    }

    renderPost = ( {item} ) => (
        <MyFeedItem
            feed={item}
            onOpenFeed={this._openFeed}
            onDeletePress={this._onDeletePress}
            onEditPress={this._onEditPress}
            onArchivePress={this._onArchivePress}/>
    );

    _fetchProfileData = () =>{
        this.props.fetchProfileData();
    };

    _openFeed = async (feed) =>{
        const { navigation } = this.props;
        await this.props.setActiveFeed('myProfileFeedDetail', feed);
        navigation.navigate('myProfileFeedDetail');
    };

    _onEditPress = (feed) =>{
        const {navigation} = this.props;
        navigation.navigate('editPost', {toEdit: feed, title: 'Edit Post'})
    };

    _onArchivePress =(feed)=>{
        const title  = 'Toggle post visibility';
        const msg = feed.is_archived ?
            'Un-archiving will make this post available general audience. Do you wish to proceed?'
            :
            'Archiving will make this post inaccessible to general audience. Do you wish to proceed?';
        Alert.alert(
            title,
            msg,
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                },
                {
                    text: 'Yes, proceed',
                    onPress: () => this.props.togglePostArchiveStatus(feed)},
            ],
            {cancelable: false},
        );
    };

    _onDeletePress =(feed)=>{
        Alert.alert(
            'Delete Post',
            'Deleting will completely erase post from your owned content, and make it inaccessible to other users. Do you wish to proceed?',
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                },
                {
                    text: 'Yes, delete post',
                    onPress: () => this.props.deletePost(feed)
                }
            ],
            {cancelable: false},
        );
    };

    _updateSearchInput = (val) =>{
        this.props.setSearchInputs({key: 'myPosts', value:val});
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

const mapStateToProps = (state) => {
    return {
        profileFetched: state.profileReducer.profileFetched,
        busyStatus: state.globalReducer.busyStatus,

        searchValue: state.profileReducer.search.myPosts,
        myPosts: state.profileReducer.data.posts
    }
};

const matchDispatchToProps = dispatch => {
    return {
        fetchProfileData: () => dispatch(fetchProfileData()),
        setSearchInputs: (ctrlObj) => dispatch(setSearchInput(ctrlObj)),
        togglePostArchiveStatus: (post) => dispatch(togglePostArchiveStatus(post)),
        deletePost: (post) => dispatch(deletePost(post)),
        setActiveFeed: (route, feed) =>dispatch( setActiveFeed(route, feed) ),
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(MyPostsScreen);
