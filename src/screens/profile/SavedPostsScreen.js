import React, { PureComponent } from 'react';
import {
    StyleSheet,
    View,
    FlatList,
    Alert
} from 'react-native';
import {connect } from "react-redux";
import {Divider, Input} from 'react-native-elements';
import ColorDefinitions from "../../constants/ColorDefinitions";
import FeedListItem from "../../components/feeds/FeedListItem";
import EmptyListItem from '../../components/EmptyList';
import NoScreenData from "../../components/NoScreenData";
import {
    bookmarkFeed, fetchProfileData,
    setActiveFeed,
    setSearchInput,
    voteFeed
} from '../../store/utilities/actionsCollection';
import HTTPCodes from "../../constants/HTTPCodes";
import Snackbar from "react-native-snackbar";
import UnavailableListItem from '../../components/UnavailableListItem';

class SavedPostsScreen extends PureComponent {

    render() {
        const savedPosts = this.props.savedPosts;
        const profileFetched = this.props.profileFetched;
        const busyStatus = this.props.busyStatus;

        if (!profileFetched )
            return (
                <NoScreenData
                    code={busyStatus.code}
                    message={busyStatus.message ? busyStatus.message : 'Fetching comments you have created'}
                    onExecRefresh={() => this._fetchProfileData()}
                />
            );

        return (
            <View style={styles.container}>
                <Input
                    placeholder={'Type to search'}
                    onChangeText={(val) => this._updateSearchInput(val) }/>
                <FlatList
                    keyExtractor={(item) => item.id.toString()}
                    data={ savedPosts.filter(item =>
                        (item.title.toLowerCase()).includes(this.props.searchValue.toLowerCase())
                    )}
                    renderItem={(item) => (this.renderSavedPost(item))}
                    ListEmptyComponent={<EmptyListItem
                        icon='ios-bookmarks'
                        message='You have not saved any feeds yet. Your saved feeds will appear here.'/>
                    }
                />
            </View>
        )
    }

    renderSavedPost = ( {item} ) => {
        if (item.is_archived || item.is_deleted)
            return (
                <View>
                    <UnavailableListItem
                        item={item}
                        onOpenItem={this._openUnAvailablePost}
                        onDeletePress={this._toggleFeedBookmark}
                    />
                    <Divider/>
                </View>
            );

        return (<FeedListItem
                feed={item} user={null}
                openFeed={this._openFeed}
                toggleFeedVote={this._toggleFeedVote}
                toggleFeedBookmark={this._toggleFeedBookmark}
                isBookmarked={this._isBookmarked}
                isUpVoted={this._isUpVoted}
                isDownVoted={this._isDownVoted}
            />
        )
    };

    _fetchProfileData = () =>{
        this.props.fetchProfileData();
    };

    _updateSearchInput = (val) =>{
        this.props.setSearchInputs({key:'savedPosts', value:val});
    };

    _openFeed = async (feed) => {
        const { navigation } = this.props;
        await this.props.setActiveFeed('myProfileFeedDetail', feed);
        navigation.navigate('myProfileFeedDetail');
    };

    _openUnAvailablePost = (alertObj) => {
        Alert.alert(
            alertObj.title,
            alertObj.message,
            [
                {
                    text: 'Close',
                    style: 'cancel',
                    onPress: () => console.log('Cancel Pressed'),
                },
            ],
            {cancelable: false},
        );
    };

    _toggleFeedBookmark = (feed) =>{
        this.props.bookmarkFeed(feed);
        const success = this.props.busyStatus.code === HTTPCodes.OK;

        Snackbar.show({
            backgroundColor: ColorDefinitions.snackBar.genericBackground,
            color: 'white',
            title: success ? 'Feed removed list' : 'Error occurred',
            duration: Snackbar.LENGTH_LONG,
            /*action: {
                title: success ? 'UNDO' : 'RETRY',
                color: 'green',
                onPress: () => this._onCommentBookmarkPress(comment),
            },*/
        });
    };

    _toggleFeedVote =(vote) =>{
        this.props.voteFeed(vote);
    };

    _isBookmarked = (feed)=> {
        const user = this.props.loginPayload.user;
        const followers = feed.followers_ids;
        if (followers.find((follower)=> follower === user.id))
            return ColorDefinitions.interactions.includesUser;
        else
            return ColorDefinitions.interactions.idle;
    };

    _isUpVoted = (feed)=> {
        const user = this.props.loginPayload.user;
        const upVoters = feed.up_voters_ids;
        if (upVoters.find((voter) => voter === user.id))
            return ColorDefinitions.interactions.includesUser;
        else
            return ColorDefinitions.interactions.idle;
    };

    _isDownVoted = (feed)=> {
        const user = this.props.loginPayload.user;
        const downVoters = feed.down_voters_ids;
        if (downVoters.find((voter) => voter === user.id))
            return ColorDefinitions.interactions.includesUser;
        else
            return ColorDefinitions.interactions.idle;
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

const mapStateToProps = (state) => {
    return {
        profileFetched: state.profileReducer.profileFetched,
        loginPayload: state.authReducer.loginPayload,
        busyStatus: state.globalReducer.busyStatus,

        searchValue: state.profileReducer.search.savedPosts,
        savedPosts: state.profileReducer.data.saved_posts
    }
};

const matchDispatchToProps = dispatch => {
    return {
        fetchProfileData: () => dispatch(fetchProfileData()),
        setSearchInputs: (ctrlObj) => dispatch(setSearchInput(ctrlObj)),
        setActiveFeed: (route, feed) =>dispatch( setActiveFeed(route, feed) ),
        bookmarkFeed: (feed)=>dispatch( bookmarkFeed(feed)),
        voteFeed: (vote)=>dispatch( voteFeed(vote)),
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(SavedPostsScreen);
