import {StyleSheet} from "react-native";
import ColorDefinitions from "../../constants/ColorDefinitions";

const styles = StyleSheet.create({
    container:{
        flex:1,
        paddingLeft: 30,
        paddingRight: 30,
        justifyContent: 'center'
    },

    inputsContainer:{
        marginBottom: 15,
    },

    iconTextHolder: {
        flexDirection: 'row',
        borderWidth: 1,
        borderRadius: 50,
        borderColor:ColorDefinitions.auth.inputBorder,
        paddingLeft: 15,
        height: 40,
        marginBottom: 20
    },

    inputIcon: {
        padding: 7,
    },

    visibilityIcon: {
        padding: 7,
        paddingRight: 10,
    },

    inputText: {
        flex: 1,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 10,
        color: ColorDefinitions.auth.input
    },

    actionButton: {
        marginTop:40,
        paddingTop:10,
        paddingBottom:10,
        backgroundColor:ColorDefinitions.auth.button,
        borderRadius:50,
        borderWidth: 1,
        borderColor: 'transparent',
    },

    buttonText: {
        color:ColorDefinitions.auth.buttonText,
        textAlign:'center'
    },

    actionTextContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop:15,
        paddingBottom:15
    },

    rightFlatButton:{
        textAlign: 'right',
    },

    leftFlatButton:{
        textAlign: 'left',
    },

    divider:{
        backgroundColor: ColorDefinitions.auth.inputBorder,
    },

    errorContainer:{

    },

    authErrorMessageHeading:{
        textAlign: 'center',
        color: 'red',
        paddingBottom: 10,
    },

    authErrorMessage:{
        paddingBottom: 5,
        color: 'red'
    }
});

export default styles;
