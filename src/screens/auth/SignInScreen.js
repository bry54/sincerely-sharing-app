import React, { PureComponent } from 'react';
import { TextInput, View, Text, TouchableOpacity} from 'react-native';
import {Icon, Divider} from 'react-native-elements';
import {SkypeIndicator} from 'react-native-indicators'
import ColorDefinitions from "../../constants/ColorDefinitions";
import styles from './styles';
import TypoDefinitions from "../../constants/TypoDefinitions";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {connect} from "react-redux";
import {
    setAuthControls,
    attemptUserLogin,
    togglePasswordVisibility
} from "../../store/utilities/actionsCollection";

class SignInScreen extends PureComponent {

    static navigationOptions = {
        title:'Sign In',
        headerTintColor: ColorDefinitions.tintColor,
        //headerStyle: { backgroundColor: ColorDefinitions.header.background },
        headerTitleStyle: { fontWeight: TypoDefinitions.ultraLightText },
    };

    render() {
        if(this.props.busyStatus.isBusy)
            return (
                <SkypeIndicator/>
            );

        let formattedErrors = this.props.controlErrors.map(err => (
            <Text style={styles.authErrorMessage} key={err}>{`\u2022 ${err}`}</Text>
        ));

        return (
            <KeyboardAwareScrollView>
            <View style={styles.container}>
                <View style={styles.errorContainer}>
                    <Text style={styles.authErrorMessageHeading}>
                        {this.props.requestErrorMessage ? this.props.requestErrorMessage : null }
                    </Text>
                    {formattedErrors}
                </View>
                <View style={styles.inputsContainer}>
                    <View style={styles.iconTextHolder}>
                        <Icon
                            iconStyle={styles.inputIcon}
                            name="ios-mail"
                            type='ionicon'
                            size={24}
                            color={ColorDefinitions.auth.input} />
                        <TextInput
                            keyboardType = 'email-address'
                            autoCapitalize = 'none'
                            style={styles.inputText}
                            placeholder="Registered Email"
                            value={this.props.email}
                            blurOnSubmit={false}
                            returnKeyType = { "next" }
                            onSubmitEditing={() => { this.passwordInput.focus(); }}
                            onChangeText={(val) => this._updateInputState('email', val)} />
                    </View>

                    <View style={styles.iconTextHolder}>
                        <Icon
                            iconStyle={styles.inputIcon}
                            name="ios-lock"
                            type='ionicon'
                            size={24}
                            color={ColorDefinitions.auth.input}/>
                        <TextInput
                            secureTextEntry={ this.props.hidePassword}
                            autoCapitalize = 'none'
                            style={ styles.inputText }
                            placeholder="Password"
                            value={this.props.password}
                            ref={(input) => { this.passwordInput = input; }}
                            onChangeText={(val) => this._updateInputState('password', val)} />
                        <Icon
                            iconStyle={styles.visibilityIcon}
                            name={ this.props.hidePassword ? "ios-eye" : "ios-eye-off" }
                            type='ionicon'
                            size={24}
                            color={ColorDefinitions.auth.input}
                            onPress={() => this._togglePasswordVisibility()} />
                    </View>
                </View>

                <Divider style={styles.divider}/>
                <View style={styles.actionTextContainer}>
                    <TouchableOpacity onPress={ this._openSignUp }>
                        <Text style={styles.leftFlatButton}>Create New Account?</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={ this._openForgotPassword }>
                        <Text style={styles.rightFlatButton}>Forgot Password?</Text>
                    </TouchableOpacity>
                </View>
                <Divider style={styles.divider}/>
                <TouchableOpacity
                    style={styles.actionButton}
                    activeOpacity = { .5 }
                    onPress={ this._signInAsync}>

                    <Text style={styles.buttonText}> Sign In </Text>
                </TouchableOpacity>
            </View>
            </KeyboardAwareScrollView>
        );
    }

    _signInAsync = async () => {
        const { navigation } = this.props;
        const loginData = {
            email: this.props.email,
            password: this.props.password
        };
        await this.props.attemptSignIn(loginData);

        if (this.props.loginPayload.user)
            navigation.navigate('Main');
    };

    _updateInputState = (inputKey, inputValue) => {
        const inputObject = {
            key: inputKey,
            value: inputValue
        };
        this.props.handleInputControls(inputObject)
    };

    _togglePasswordVisibility = () => {
        this.props.togglePasswordVisibility();
    };

    _openForgotPassword = () => {
        this.props.navigation.navigate('ForgotPassword');
    };

    _openSignUp = () => {
        this.props.navigation.navigate('SignUp');
    };
}

const mapStateToProps = (state) => {
    return {
        busyStatus: state.globalReducer.busyStatus,
        email: state.authReducer.controls.email,
        password: state.authReducer.controls.password,
        hidePassword: state.authReducer.controls.sections.auth,

        requestErrorMessage: state.authReducer.requestErrorMessage,
        controlErrors: state.authReducer.controlErrors,
        loginPayload: state.authReducer.loginPayload,
    }
};

const matchDispatchToProps = dispatch => {
    return {
        togglePasswordVisibility: () => dispatch( togglePasswordVisibility('auth') ),
        handleInputControls: (controlObject) => dispatch( setAuthControls(controlObject) ),
        attemptSignIn: (loginData) => dispatch( attemptUserLogin( loginData )),
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(SignInScreen);
