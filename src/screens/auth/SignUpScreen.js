import React, { PureComponent } from 'react';
import { View, Text, TextInput, TouchableOpacity } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {Icon, Divider} from "react-native-elements";
import {connect} from "react-redux";
import {SkypeIndicator} from "react-native-indicators";
import ColorDefinitions from "../../constants/ColorDefinitions";
import TypoDefinitions from "../../constants/TypoDefinitions";
import styles from './styles';
import {
    attemptSignUp,
    setAuthControls,
    togglePasswordVisibility
} from "../../store/utilities/actionsCollection";

class SignUpScreen extends PureComponent{

    static navigationOptions = {
        title:'Sign Up',
        headerTintColor: ColorDefinitions.tintColor,
        //headerStyle: { backgroundColor: ColorDefinitions.header.background },
        headerTitleStyle: { fontWeight: TypoDefinitions.ultraLightText },
    };

    render() {
        if(this.props.busyStatus.isBusy)
            return (
                <SkypeIndicator/>
            );

        let formattedErrors = this.props.controlErrors.map(err => (
            <Text style={styles.authErrorMessage} key={err}>{`\u2022 ${err}`}</Text>
        ));

        return (
            <KeyboardAwareScrollView>
            <View style={styles.container}>
                <View style={styles.errorContainer}>
                    <Text style={styles.authErrorMessageHeading}>
                        { this.props.requestErrorMessage ? this.props.requestErrorMessage : null }
                    </Text>
                    {formattedErrors}
                </View>
                <View style={styles.inputsContainer}>
                    <View style={styles.iconTextHolder}>
                        <Icon
                            iconStyle={styles.inputIcon}
                            name="logo-reddit"
                            type='ionicon'
                            size={24}
                            color={ColorDefinitions.auth.input} />

                        <TextInput
                            autoCapitalize = 'none'
                            style={styles.inputText}
                            placeholder="Choose an anonymous username"
                            value = {this.props.username}
                            blurOnSubmit={false}
                            returnKeyType = { "next" }
                            onSubmitEditing={() => { this.emailInput.focus(); }}
                            onChangeText={(val) => {this._updateInputState('username', val)}} />
                    </View>

                    <View style={styles.iconTextHolder}>
                        <Icon
                            iconStyle={styles.inputIcon}
                            name="ios-mail"
                            type='ionicon'
                            size={24}
                            color={ColorDefinitions.auth.input} />

                        <TextInput
                            autoCapitalize = 'none'
                            style={styles.inputText}
                            placeholder="Email Address"
                            value = {this.props.email}
                            ref={(input) => { this.emailInput = input; }}
                            blurOnSubmit={false}
                            returnKeyType = { "next" }
                            onSubmitEditing={() => { this.passwordInput.focus(); }}
                            onChangeText={(val) => {this._updateInputState('email', val)}} />
                    </View>

                    <View style={styles.iconTextHolder}>
                        <Icon
                            iconStyle={styles.inputIcon}
                            name="ios-lock"
                            type='ionicon'
                            size={24}
                            color={ColorDefinitions.auth.input} />

                        <TextInput
                            autoCapitalize = 'none'
                            secureTextEntry={ this.props.hidePassword}
                            style={styles.inputText}
                            placeholder="Password"
                            value = {this.props.password}
                            ref={(input) => { this.passwordInput = input; }}
                            blurOnSubmit={false}
                            returnKeyType = { "next" }
                            onSubmitEditing={() => { this.confirmPasswordInput.focus(); }}
                            onChangeText={(val) => {this._updateInputState('password', val)}} />
                    </View>

                    <View style={styles.iconTextHolder}>
                        <Icon
                            iconStyle={styles.inputIcon}
                            name="ios-lock"
                            type='ionicon'
                            size={24}
                            color={ColorDefinitions.auth.input} />

                        <TextInput
                            autoCapitalize = 'none'
                            secureTextEntry={ this.props.hidePassword}
                            style={styles.inputText}
                            placeholder="Conform Password"
                            value = {this.props.passwordConfirm}
                            ref={(input) => { this.confirmPasswordInput = input; }}
                            onChangeText={(val) => {this._updateInputState('passwordConfirm', val)}} />

                        <Icon
                            iconStyle={styles.visibilityIcon}
                            name={ this.props.hidePassword ? "ios-eye" : "ios-eye-off" }
                            type='ionicon'
                            size={24}
                            color={ColorDefinitions.auth.input}
                            onPress={() => this._togglePasswordVisibility()} />
                    </View>
                </View>

                <Divider/>
                <View style={styles.actionTextContainer}>
                    <TouchableOpacity onPress={ this._openSignIn }>
                        <Text style={styles.leftFlatButton}>Back To Login?</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={ this._openForgotPassword }>
                        <Text style={styles.rightFlatButton}>Forgot Password?</Text>
                    </TouchableOpacity>
                </View>
                <Divider/>

                <TouchableOpacity
                    style={styles.actionButton}
                    activeOpacity = { .5 }
                    onPress={ this._handleSignUp }>

                    <Text style={styles.buttonText}> Sign Up </Text>
                </TouchableOpacity>
            </View>
            </KeyboardAwareScrollView>
        );
    }

    _updateInputState = (inputKey, inputValue) => {
        const inputObject = {
            key: inputKey,
            value: inputValue
        };
        this.props.handleInputControls(inputObject)
    };

    _openSignIn = async () => {
        this.props.navigation.navigate('SignIn');
    };

    _openForgotPassword = () => {
        this.props.navigation.navigate('ForgotPassword');
    };

    _togglePasswordVisibility = () => {
        this.props.togglePasswordVisibility();
    };

    _handleSignUp = async () => {
        const { navigation } = this.props;
        const loginData = {
            email: this.props.email,
            username: this.props.username,
            password: this.props.password,
            passwordConfirm: this.props.passwordConfirm
        };

        await this.props.attemptSignUp(loginData);

        if (this.props.loginPayload.user)
            navigation.navigate('Main');
    };
}

const mapStateToProps = (state) => {
    return {
        hidePassword: state.authReducer.controls.sections.auth,
        username: state.authReducer.controls.username,
        email: state.authReducer.controls.email,
        password: state.authReducer.controls.password,
        passwordConfirm: state.authReducer.controls.passwordConfirm,

        requestErrorMessage: state.authReducer.requestErrorMessage,
        controlErrors: state.authReducer.controlErrors,

        busyStatus: state.globalReducer.busyStatus,
        loginPayload: state.authReducer.loginPayload,
    }
};

const matchDispatchToProps = dispatch => {
    return {
        togglePasswordVisibility: () =>dispatch(togglePasswordVisibility('auth')),
        handleInputControls: (controlObject) => dispatch( setAuthControls(controlObject)),
        attemptSignUp: (loginData) => dispatch( attemptSignUp(loginData) ),
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(SignUpScreen);
