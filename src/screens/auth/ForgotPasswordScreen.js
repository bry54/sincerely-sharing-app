import React, { PureComponent} from 'react';
import {Text, TextInput, TouchableOpacity, View} from 'react-native';
import {Icon, Divider} from "react-native-elements";

import ColorDefinitions from '../../constants/ColorDefinitions';
import styles from './styles';
import TypoDefinitions from "../../constants/TypoDefinitions";
import { attemptForgetPasswordRequest, setAuthControls } from "../../store/utilities/actionsCollection";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {connect} from "react-redux";

class ForgotPasswordScreen extends PureComponent{

    static navigationOptions = {
        title:'Forgot Password',
        headerTintColor: ColorDefinitions.tintColor,
        //headerStyle: { backgroundColor: ColorDefinitions.header.background },
        headerTitleStyle: { fontWeight: TypoDefinitions.ultraLightText },
    };

    render() {
        return (
            <KeyboardAwareScrollView>
                <View style={styles.container}>
                    <View style={styles.inputsContainer}>
                        <View style={styles.iconTextHolder}>
                            <Icon
                                iconStyle={styles.inputIcon}
                                name="logo-reddit"
                                type='ionicon'
                                size={24}
                                color={ColorDefinitions.auth.input} />

                            <TextInput
                                autoCapitalize = 'none'
                                style={styles.inputText}
                                placeholder="Your anonymous username"
                                value = {this.props.username}
                                blurOnSubmit={false}
                                returnKeyType = { "next" }
                                onSubmitEditing={() => { this.emailInput.focus(); }}
                                onChangeText={(val) => {this._updateInputState('username', val)}} />
                        </View>

                        <View style={styles.iconTextHolder}>
                            <Icon
                                iconStyle={styles.inputIcon}
                                name="ios-mail"
                                type='ionicon'
                                size={24}
                                color={ColorDefinitions.auth.input} />

                            <TextInput
                                keyboardType = 'email-address'
                                autoCapitalize = 'none'
                                style={styles.inputText}
                                placeholder="Email Address"
                                value = {this.props.email}
                                ref={(input) => { this.emailInput = input; }}
                                onChangeText={(val) => {this._updateInputState('email', val)}} />
                        </View>

                    </View>

                    <Divider/>

                    <View style={styles.actionTextContainer}>
                        <TouchableOpacity onPress={ this._openSignIn }>
                            <Text style={styles.leftFlatButton}>Back To Login?</Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={ this._openSignUp }>
                            <Text style={styles.rightFlatButton}>Create New Account?</Text>
                        </TouchableOpacity>
                    </View>
                    <Divider/>

                    <TouchableOpacity
                        style={styles.actionButton}
                        activeOpacity = { .5 }
                        onPress={ this._handlePasswordReset }>

                        <Text style={styles.buttonText}> Reset Password </Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAwareScrollView>
        );
    }

    _updateInputState = (inputKey, inputValue) => {
        const inputObject = {
            key: inputKey,
            value: inputValue
        };
        this.props.handleInputControls(inputObject)
    };

    _openSignIn = async () => {
        this.props.navigation.navigate('SignIn');
    };

    _openSignUp = () => {
        this.props.navigation.navigate('SignUp');
    };

    _handlePasswordReset = () => {
        const { navigation } = this.props;
        const identityInformation = {
            email: this.props.email,
            username: this.props.username
        };

        this.props.attemptForgetPasswordRequest(navigation, identityInformation)
    };
}

const mapStateToProps = (state) => {
    return {
        username: state.authReducer.controls.username,
        email: state.authReducer.controls.email,

        requestErrorMessage: state.authReducer.requestErrorMessage,
        controlErrors: state.authReducer.controlErrors,
    }
};

const matchDispatchToProps = dispatch => {
    return {
        handleInputControls: (controlObject) => dispatch( setAuthControls(controlObject)),
        attemptForgetPasswordRequest: (navigation, identityInformation) => dispatch( attemptForgetPasswordRequest(navigation, identityInformation) ),
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(ForgotPasswordScreen);
