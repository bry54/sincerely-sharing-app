import React, { PureComponent } from 'react';
import { ActivityIndicator, StyleSheet, View } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { setLoginPayload } from "../../store/utilities/actionsCollection";
import { connect } from "react-redux";

class AuthLoadingScreen extends PureComponent {

    constructor(props) {
        super(props);
        this._bootstrapAsync();
    }

    render() {
        return (
            <View style={styles.container}>
                <ActivityIndicator size="large" />
            </View>
        );
    }

    /**
     * Fetches the token from storage then navigate to our appropriate place.
     * This will switch to the App screen or Auth screen and this loading screen will be unmounted and thrown away.
     */
    _bootstrapAsync = async () => {
        const { navigation } = this.props;
        const rawLoginPayload = await AsyncStorage.getItem('loginPayload');
        const loginPayload = JSON.parse(rawLoginPayload);

        if (loginPayload) {
            this.props.setLoginPayload(loginPayload);
            navigation.navigate('Main');
        } else {
            navigation.navigate('Auth')
        }
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    }
});

const mapStateToProps = (state) => {
    return {
        loginPayload: state.authReducer.loginPayload,
        busyStatus: state.globalReducer.busyStatus,
    }
};

const matchDispatchToProps = dispatch => {
    return {
        setLoginPayload: (loginPayload) => dispatch (setLoginPayload(loginPayload)),
    }
};

export default connect(mapStateToProps, matchDispatchToProps)(AuthLoadingScreen);
