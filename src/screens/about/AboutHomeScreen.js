import React, {PureComponent} from 'react';
import {Picker, StyleSheet, View, Linking} from 'react-native';
import { Button } from "react-native-elements";
import ColorDefinitions from '../../constants/ColorDefinitions';
import TypoDefinitions from '../../constants/TypoDefinitions';

export default class HomeScreen extends PureComponent{

    static navigationOptions = {
        title: 'About',
        headerTintColor: ColorDefinitions.tintColor,
        //headerStyle: { backgroundColor: ColorDefinitions.header.background },
        headerTitleStyle: { fontWeight: TypoDefinitions.ultraLightText },
    };

    state = { };

    render() {
        return (
            <View style={styles.container}>
                {this._renderElements()}
            </View>
        );
    }

    _renderElements = () => {
        let buttons=[];
        const screens = [
            {
                title: 'Terms Of Use',
                route: 'tou',
                icon: 'life-ring'
            },
            {
                title: 'Privacy Policy',
                route: 'privacyPolicy',
                icon: 'user-secret'
            },
            {
                title: 'Report Problem',
                route: 'reportProblem',
                icon: 'bug',
                openMailer: true
            },
            {
                title: 'About Developer',
                route: 'aboutDeveloper',
                icon: 'code'
            }
        ];

        screens.map((screen, index)=>{buttons.push(
            <View style={styles.genericSpacing} key={screen.route}>
                <Button
                    icon={{
                        type: 'font-awesome',
                        name: screen.icon,
                        size: 18,
                        color: "white",
                    }}
                    type={'solid'}
                    title={screen.title}
                    titleStyle={{fontWeight: '100'}}
                    onPress={ screen.openMailer ?
                        () => Linking.openURL('mailto:brianacyth@icloud.com?subject=Feedback&body=Description').catch((err) => console.error('An error occurred', err))
                        :
                        ()=>this._openAboutPage(screen.route)
                    }
                />
            </View>)
        });

        return buttons
    };

    _openAboutPage = (routeName) => {
        const {navigation} = this.props;
        navigation.push(routeName);
    }
};

const styles = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent:'center',
        paddingTop:10,
        paddingBottom:10,
    },

    genericSpacing:{
        paddingBottom: 20,
        paddingLeft: 10,
        paddingRight: 10,
    },
    spinners: { }
});
