import React, {PureComponent} from 'react';
import {Picker, StyleSheet, TextInput, View} from 'react-native';
import {Icon, Divider} from "react-native-elements";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ColorDefinitions from '../../constants/ColorDefinitions';
import TypoDefinitions from '../../constants/TypoDefinitions';

export default class ReportProblemScreen extends PureComponent{

    static navigationOptions = ({ navigation }) => {
        const params = navigation.state.params || {};

        return {
            title: 'Send Feedback',
            headerTintColor: ColorDefinitions.tintColor,
            //headerStyle: { backgroundColor: ColorDefinitions.header.background },
            headerTitleStyle: { fontWeight: TypoDefinitions.ultraLightText },
            headerRight: (
                <Icon
                    onPress={()=>alert('Feedback Sent')}
                    type='font-awesome'
                    name='paper-plane'
                    color={ColorDefinitions.tintColor}
                    iconStyle={{paddingRight:20}}/>
            ),
        };
    };

    render() {
        return (
            <KeyboardAwareScrollView>
            <View style={styles.container}>
                <View style={styles.genericSpacing}>
                    <Picker
                        selectedValue={this.state.community}
                        style={styles.spinners}
                        onValueChange={(itemValue, itemIndex) => this.setState({community: itemValue})}>
                        <Picker.Item label="Select Feedback Type" value=""/>
                        <Picker.Item label="Bug" value=""/>
                        <Picker.Item label="General Feedback" value="" />
                    </Picker>
                </View>
                <Divider/>
                <View style={styles.genericSpacing}>
                    <TextInput
                        multiline={true}
                        placeholder='Enter an subject'/>
                </View>
                <Divider/>
                <View style={styles.genericSpacing}>
                    <TextInput
                        multiline={true}
                        placeholder='Type the full feedback here...'/>
                </View>
            </View>
            </KeyboardAwareScrollView>
        );
    }
};

const styles = StyleSheet.create({
    container:{
        flex: 0,
        paddingTop:10,
        paddingBottom:10,
    },

    genericSpacing:{
        paddingLeft: 10,
        paddingRight: 10,
    },
    spinners: { }
});
