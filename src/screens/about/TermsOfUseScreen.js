import React, {PureComponent} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import {} from 'react-native-elements';
import {Divider} from "react-native-elements";
import {termsOfUse} from "../../constants/LegalInformation";
import ColorDefinitions from '../../constants/ColorDefinitions';
import TypoDefinitions from '../../constants/TypoDefinitions';

export default class TermsOfUseScreen extends PureComponent{

    static navigationOptions = {
        title: 'Terms Of Use',
        headerTintColor: ColorDefinitions.tintColor,
        //headerStyle: { backgroundColor: ColorDefinitions.header.background },
        headerTitleStyle: { fontWeight: TypoDefinitions.ultraLightText },
    };

    render() {
        let legalInfo = termsOfUse.map(terms => (
            <View key={terms.title}>
                <Text style={styles.sectionHeading}>{terms.title}</Text>
                <Text style={styles.sectionBody}>{terms.body}</Text>
            </View>
        ));

        return (
            <View style={styles.container}>
                <ScrollView style={styles.container}>
                    {legalInfo}
                    <Divider/>
                    <Text style={[styles.sectionBody, styles.copyright]}>Copyright @ 2019</Text>
                    <Divider/>
                </ScrollView>
            </View>
        );
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        paddingTop:5,
        paddingLeft:5,
        paddingRight:5
    },
    copyright:{
        textAlign: 'center',
        fontSize: TypoDefinitions.smallFont
    },
    sectionHeading:{
        fontSize: TypoDefinitions.normalFont,
        fontWeight: TypoDefinitions.mediumText,
        paddingBottom: 4
    },
    sectionBody:{
        fontSize: TypoDefinitions.normalFont,
        fontWeight: TypoDefinitions.lightText,
        paddingBottom: 8
    }
});
