import React, {PureComponent} from 'react';
import {Linking, StyleSheet, Text, View} from 'react-native';
import {} from 'react-native-elements';
import {SocialIcon} from "react-native-elements";
import ColorDefinitions from '../../constants/ColorDefinitions';
import TypoDefinitions from '../../constants/TypoDefinitions';

export default class AboutDeveloperScreen extends PureComponent{

    static navigationOptions = {
        title: 'About Developer',
        headerTintColor: ColorDefinitions.tintColor,
        //headerStyle: { backgroundColor: ColorDefinitions.header.background },
        headerTitleStyle: { fontWeight: TypoDefinitions.ultraLightText },
    };

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.fullName}>Brian Paidamoyo Sithole</Text>
                <Text style={styles.career}>Software Dev || Mobile App Dev || Web Dev</Text>

                <View style={styles.communications}>
                    { this._showCommunicationChannels() }
                </View>
            </View>
        );
    }

    _showCommunicationChannels = () =>{
        let icons=[];
        const channels = [
            {
                web_url: 'https://www.linkedin.com/in/brian-paidamoyo-sithole-b6544877/',
                icon: 'linkedin',
                color:'tomato'
            },
            {
                web_url: 'https://twitter.com/brizzy_p',
                icon: 'twitter',
                color:'blue'
            },
            {
                web_url: 'https://github.com/bry54',
                icon: 'github-alt',
                color:'blue'
            },
        ];

        channels.map((channel, index)=>{icons.push(
            <View key={index.toString()}>
                <SocialIcon
                    light
                    iconColor={channel.color}
                    type={channel.icon}
                    onPress={
                        () => Linking.openURL(channel.web_url).catch(
                            (err)=>console.log(err))

                    }/>
            </View>
        )});

        return icons
    }
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },

    fullName: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },

    career: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 10,
    },

    communications: {
        paddingTop: 20,
        paddingLeft: 20,
        paddingRight: 20,
        flexDirection: 'row' ,
        justifyContent: 'space-between',
    },
});
