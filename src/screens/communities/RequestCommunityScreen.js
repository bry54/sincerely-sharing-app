import React, { PureComponent} from 'react';
import {connect } from "react-redux";
import {StyleSheet, View, Picker, TextInput } from 'react-native';
import {CheckBox, Divider, Icon, Button} from 'react-native-elements';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ColorDefinitions from '../../constants/ColorDefinitions';
import TypoDefinitions from '../../constants/TypoDefinitions';
import {setRequestControls, submitRequest} from '../../store/utilities/actionsCollection';

class RequestCommunityScreen extends PureComponent{
    static navigationOptions = ({ navigation }) => {
        const params = navigation.state.params || {};

        return {
            title: 'Request Community',
            headerTintColor: ColorDefinitions.tintColor,
            //headerStyle: { backgroundColor: ColorDefinitions.header.background },
            headerTitleStyle: { fontWeight: TypoDefinitions.ultraLightText }
        };
    };

    componentDidMount = async () => {
        const { navigation } = this.props;

        navigation.setParams({

        });

        this.didFocusListener = navigation.addListener (
            'didFocus', () => {
                this._clearInputs();
            },
        );
    };

    render() {
        return (
            <KeyboardAwareScrollView>
                <View style={styles.container}>
                    <View style={styles.genericSpacing}>
                        <TextInput
                            {...this.props}
                            value = {this.props.title}
                            multiline={true}
                            blurOnSubmit={false}
                            placeholder='Enter community name'
                            onChangeText={(val) => this._updateInputState('title', val)} />
                    </View>
                    <Divider/>
                    <View style={styles.genericSpacing}>
                        <TextInput
                            {...this.props}
                            editable = {true}
                            autogrow={true}
                            value ={this.props.body}
                            multiline={true}
                            style={{maxHeight: 320 }}
                            placeholder='Type community description here...'
                            onChangeText={(val) => this._updateInputState('body', val) }/>
                    </View>

                    <View style={styles.genericSpacing}>
                        <CheckBox
                            containerStyle={{marginHorizontal: 0}}
                            title='Make the community exclusive to FIF members'
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            checked={this.props.fif_exclusive}
                            onPress={() => this._updateInputState('fif_exclusive', !this.props.fif_exclusive)}
                        />
                    </View>

                    <View style={{marginHorizontal: 15, marginVertical: 20}}>
                        <Button
                            onPress={() => this._submitRequest()}
                            title="Request Community"
                            type="outline"
                        />
                    </View>
                </View>
            </KeyboardAwareScrollView>
        );
    }

    _clearInputs = () =>{
        const inputs = ['title','body','fif_exclusive'];
        inputs.forEach(input=>this._updateInputState(input, null));
    };

    _updateInputState = (inputKey, inputValue) => {
        const inputObject = {
            key: inputKey,
            value: inputValue
        };
        this.props.handleInputControls(inputObject)
    };

    _submitRequest = async ( )  => {
        const { navigation } = this.props;

        if( this.props.title && this.props.body && this.props.fif_exclusive ) {
            const newPost = {
                title: this.props.title,
                body: this.props.body,
                fif_exclusive: this.props.fif_exclusive
            };
            await this.props.submitRequest(newPost);

            navigation.goBack();
        }
        else
            alert('All fields need to be completed');
    }
}

const styles = StyleSheet.create({
    container:{
        paddingTop:10,
        paddingBottom:10,
    },

    genericSpacing:{
        paddingLeft: 10,
        paddingRight: 10,
    },

    spinners: { }
});

const mapStateToProps = (state) => {
    return {
        title : state.communitiesReducer.request.title,
        body : state.communitiesReducer.request.body,
        fif_exclusive: state.communitiesReducer.request.fif_exclusive
    }
};

const matchDispatchToProps = dispatch => {
    return {
        handleInputControls: (inputObject) => dispatch( setRequestControls(inputObject)),
        submitRequest: (community) => dispatch (submitRequest( community )),
    }
};
export default connect(mapStateToProps, matchDispatchToProps)(RequestCommunityScreen);
