import React, { PureComponent} from 'react';
import {StyleSheet, View, FlatList, Alert, TextInput, Text, TouchableOpacity, RefreshControl} from 'react-native';
import {Avatar, ButtonGroup, Icon, ListItem, Button} from 'react-native-elements';
import {connect } from "react-redux";
import ColorDefinitions from '../../constants/ColorDefinitions';
import TypoDefinitions from '../../constants/TypoDefinitions';
import {UIActivityIndicator} from 'react-native-indicators';
import { Dialog } from 'react-native-simple-dialogs';

import Helpers from '../../utilities/Helpers';
import CommunityTypes from '../../constants/CommunityTypes';
import {
    updateCommunityButtonIndex,
    fetchCommunities,
    toggleCommunitySubscription,
    setActiveCommunity,
    togglePasswordDialogVisibility,
    togglePasswordVisibility,
    setCommunityPassword,
    toggleCommunityRequestVote
} from '../../store/utilities/actionsCollection'
import EmptyListItem from "../../components/EmptyList";
import NoScreenData from "../../components/NoScreenData";
import Snackbar from "react-native-snackbar";
import HTTPCodes from "../../constants/HTTPCodes";

class CommunitiesHomeScreen extends PureComponent{

    static navigationOptions = ({ navigation }) => {
        const params = navigation.state.params || {};

        return {
            title: 'Communities',
            headerTintColor: ColorDefinitions.tintColor,
            //headerStyle: { backgroundColor: ColorDefinitions.header.background },
            headerTitleStyle: { fontWeight: TypoDefinitions.ultraLightText },

            headerRight: params.isAddBtnShow ?  (
                    <Icon
                        onPress={ () => params.requestCommunity() }
                        type='font-awesome'
                        name='plus-square-o'
                        color={ColorDefinitions.tintColor}
                        iconStyle={{paddingRight:20}}/>)
                :( <View/> ),
        };
    };

    componentDidMount = async () => {
        const { navigation } = this.props;

        this.didFocusListener = navigation.addListener (
            'didFocus', () => {
                if (!this.props.fetchedCommunities) this._fetchCommunities();
            },
        );

        navigation.setParams({
            requestCommunity: this._requestCommunity,
        });
    };

    _toggleCommunityAddButton = (index)=>{
        const {navigation} = this.props;
        navigation.setParams({isAddBtnShow: index === CommunityTypes.REQUESTED});
    };

    _requestCommunity = () => {
        const {navigation} = this.props;
        navigation.navigate('communityRequest');
    };

    render() {
        const buttons = ['FIF Exclusive', 'For All', 'Requested'];
        const busyStatus = this.props.busyStatus;
        const selectedIndex = this.props.communitiesButtonIndex;
        const communities = this.props.communities;
        const protectedCommunities = communities ? communities.filter(community => community.password) : [];
        const openCommunities = communities? communities.filter(community => !community.password) : [];
        const requestedCommunities = this.props.requested;

        let toShow = [];
        switch (selectedIndex) {
            case CommunityTypes.PROTECTED :
                toShow = protectedCommunities;
                break;
            case CommunityTypes.OPEN:
                toShow = openCommunities;
                break;
            case CommunityTypes.REQUESTED:
                toShow = requestedCommunities;
                break;
            default:
                break
        }

        if (!this.props.fetchedCommunities)
            return (
                <NoScreenData
                    code={busyStatus.code}
                    message={busyStatus.message}
                    onExecRefresh={this._fetchCommunities}
                />
            );

        return (
            <View style={styles.container}>
                {this.passwordPromptDialog()}
                <ButtonGroup
                    onPress={this._updateCommunityButtonIndex}
                    selectedIndex={selectedIndex}
                    buttons={buttons}
                    containerStyle={{height: 30, marginTop:10}}
                />
                <FlatList
                    keyExtractor={this.keyExtractor}
                    refreshControl={ <RefreshControl
                        refreshing={ busyStatus.isBusy }
                        onRefresh={ () => this._fetchCommunities()}
                    /> }
                    data={ toShow }
                    renderItem={selectedIndex === CommunityTypes.REQUESTED ? this.renderRequestItem : this.renderCommunityItem}
                    ListEmptyComponent={<EmptyListItem
                        icon='ios-color-filter'
                        message={selectedIndex === CommunityTypes.REQUESTED ? 'No community has been requested yet. Be the first one to request' : 'No community has been defined in this category yet.'}/>
                    }
                />
            </View>
        )
    }

    keyExtractor = (item, index) => item.id.toString();

    renderCommunityItem = ({ item }) => (
        <ListItem
            bottomDivider={true}
            title={Helpers.titleCaseString(item.name)}
            subtitle={item.users_count+' Subscribers'}
            onPress={()=>this._openCommunity(item)}
            leftAvatar={ ()=>(
                <Avatar
                    size="medium"
                    rounded={true}
                    title={Helpers.extractInitials(item.name).toUpperCase()}
                    titleStyle={{fontSize: TypoDefinitions.normalFont}}/>
            )}

            rightIcon={ ()=>(
                <Icon
                    name={this._isSubscribed(item.user_ids) ? 'ios-star' : 'ios-star-outline'}
                    type='ionicon'
                    color={ColorDefinitions.interactions.includesUser}
                    onPress={() => this._toggleCommunitySubscription(item)} />
            )}
        />
    );

    renderRequestItem = ({ item }) => (
        <ListItem
            bottomDivider={true}
            title={Helpers.titleCaseString(item.title)}
            subtitle={`Upvotes: ${item.up_votes_count} | Downvotes: ${item.down_votes_count} `}
            onPress={()=>this.showRequestInformation(item)}

            rightIcon={ ()=>(
                <View style={{flexDirection: 'row'}}>
                    <Icon
                        name={'md-arrow-round-up'}
                        size={TypoDefinitions.bigFont}
                        iconStyle={{paddingLeft: 4, paddingRight: 6}}
                        type='ionicon'
                        color={ item.up_voters_ids.find(
                            id => id === this.props.user.id ) ? ColorDefinitions.interactions.includesUser : ColorDefinitions.interactions.idle
                        }
                        onPress={() => this._upVoteRequest(item)}/>

                    <Icon
                        name={'md-arrow-round-down'}
                        size={TypoDefinitions.bigFont}
                        iconStyle={{paddingLeft: 6, paddingRight: 4}}
                        type='ionicon'
                        color={ item.down_voters_ids.find(
                            id => id === this.props.user.id ) ? ColorDefinitions.interactions.includesUser : ColorDefinitions.interactions.idle
                        }
                        onPress={() => this._downVoteRequest(item)} />
                </View>
            )}
        />
    );

    _upVoteRequest = (community) =>{
        const vote = {
            communityId: community.id,
            up: 1,
            down: 0
        };
        this.props.voteRequestedCommunity(vote);
    };

    _downVoteRequest = (community) =>{
        const vote = {
            communityId: community.id,
            up: 0,
            down: 1
        };
        this.props.voteRequestedCommunity(vote);
    };

    showRequestInformation = () =>{

    };

    passwordPromptDialog = () => (
        <Dialog
            title="Protected Community"
            animationType="slide"
            contentStyle={{ alignItems: "center", justifyContent: "center" }}
            onTouchOutside={() => this._togglePasswordDialogVisibility()}
            visible={this.props.passwordDialogVisible}>

            <Text style={ { marginBottom: 15 } }>
                Community is protected, please enter community password to subscribe.
            </Text>

            <View style={styles.iconTextHolder}>
                <Icon
                    iconStyle={styles.inputIcon}
                    name="ios-lock"
                    type='ionicon'
                    size={18}
                    color={ColorDefinitions.auth.input}/>
                <TextInput
                    secureTextEntry={ this.props.hidePassword}
                    autoCapitalize = 'none'
                    style={ styles.inputText }
                    placeholder="Password"
                    value={this.props.password}
                    ref={(input) => { this.passwordInput = input; }}
                    onChangeText={(val) => this._updateInputState(val)} />
                <Icon
                    iconStyle={styles.visibilityIcon}
                    name={ this.props.hidePassword ? "ios-eye" : "ios-eye-off" }
                    type='ionicon'
                    size={18}
                    color={ColorDefinitions.auth.input}
                    onPress={() => this._togglePasswordVisibility()} />
            </View>

            <View style={{flexDirection: 'row', flex: 1, justifyContent: 'space-between', marginTop: 10, marginBottom: 20}}>
                <Button
                    onPress={() => this._togglePasswordDialogVisibility()}
                    containerStyle={{marginHorizontal: 15}}
                    buttonStyle={{padding: 15}}
                    title="Close"/>

                <Button
                    containerStyle={{marginHorizontal: 15}}
                    buttonStyle={{padding: 15}}
                    onPress={() => this._subscribeToCommunity()}
                    title="Subscribe"
                />
            </View>
        </Dialog>
    );

    _fetchCommunities = () => {
        this.props.fetchCommunities();
    };

    _updateCommunityButtonIndex = (index) => {
        this.props.updateCommunityButtonIndex(index);
        this._toggleCommunityAddButton(index)
    };

    _isSubscribed = (ids) =>{
        const userId = this.props.user.id;
        return ids.find( id => id === userId );
    };

    _toggleCommunitySubscription = async (community) =>{
        await this.props.setActiveCommunity(community);

        if (this._isSubscribed(community.user_ids)){
            await this.props.toggleCommunitySubscription(community);
            this._showToast();
        }

        else if (!community.password && !this._isSubscribed(community.user_ids)){
            await this.props.toggleCommunitySubscription(community);
            this._showToast();
        }

        else if (community.password && !this._isSubscribed(community.user_ids)){
            this._togglePasswordDialogVisibility();
        }

    };

    _showToast =()=>{
        const success  = this.props.busyStatus.code === HTTPCodes.OK;
        Snackbar.show({
            backgroundColor: ColorDefinitions.snackBar.genericBackground,
            color: 'white',
            title: success ? 'Community Subscription updated' : 'An Error occurred',
            duration: Snackbar.LENGTH_LONG,
        });
    };

    _togglePasswordDialogVisibility = () =>{
        this.props.togglePasswordDialogVisibility()
    };

    _togglePasswordVisibility = () => {
        this.props.togglePasswordVisibility();
    };

    _updateInputState = (inputValue) => {
        this.props.setCommunityPassword(inputValue)
    };

    _openCommunity = async (community) =>{
        const { navigation } = this.props;
        await this.props.setActiveCommunity(community);

        if (community.password && !this._isSubscribed(community.user_ids)){
            this._togglePasswordDialogVisibility();
        } else {
            navigation.navigate('communityFeeds')
        }
    };

    _subscribeToCommunity = async () =>{
        const { navigation } = this.props;
        const password = this.props.password;

        await this.props.subscribeToCommunity(this.props.activeCommunity, password);

        if (this._isSubscribed(this.props.activeCommunity.user_ids)) {
            this._togglePasswordDialogVisibility();
            navigation.navigate('communityFeeds');
        }
        else
            Alert.alert('Error Subscribing', 'An error occurred while trying to subscribe to community. Try again')
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
    },

    buttonGroupContainer:{
        paddingVertical:10
    },

    visibilityIcon: {
        padding: 5,
        paddingRight: 10,
    },

    inputText: {
        flex: 1,
        paddingTop: 7,
        paddingRight: 10,
        paddingBottom: 7,
        paddingLeft: 10,
        color: ColorDefinitions.auth.input
    },

    inputIcon: {
        padding: 5,
    },

    iconTextHolder: {
        flexDirection: 'row',
        borderWidth: 1,
        borderRadius: 15,
        borderColor:ColorDefinitions.auth.inputBorder,
        height: 30,
        paddingLeft:10,
        marginBottom: 10
    },
});

const mapStateToProps = (state) => {
    return {
        user: state.authReducer.loginPayload.user,
        busyStatus: state.globalReducer.busyStatus,
        hidePassword: state.authReducer.controls.sections.community,

        communitiesButtonIndex: state.communitiesReducer.communityButtonIndex,
        fetchedCommunities: state.communitiesReducer.communities.isFetched,
        communities: state.communitiesReducer.communities.available,
        requested: state.communitiesReducer.communities.requested,
        passwordDialogVisible: state.communitiesReducer.passwordDialogVisible,
        password: state.communitiesReducer.communityPassword,
        activeCommunity: state.communitiesReducer.activeCommunity
    }
};

const matchDispatchToProps = dispatch => {
    return {
        updateCommunityButtonIndex: (buttonIndex) => dispatch (updateCommunityButtonIndex(buttonIndex)),
        setActiveCommunity: (community)=> dispatch(setActiveCommunity(community)),
        fetchCommunities: ()=>dispatch(fetchCommunities()),
        toggleCommunitySubscription: (community)=>dispatch(toggleCommunitySubscription(community)),
        subscribeToCommunity: (community, password)=>dispatch(toggleCommunitySubscription(community,password)),
        togglePasswordDialogVisibility: () => dispatch(togglePasswordDialogVisibility()),
        togglePasswordVisibility: () =>dispatch(togglePasswordVisibility('community')),
        setCommunityPassword: (inputValue) => dispatch(setCommunityPassword(inputValue)),
        voteRequestedCommunity: (vote) => dispatch(toggleCommunityRequestVote(vote))
    }
};
export default connect(mapStateToProps, matchDispatchToProps)(CommunitiesHomeScreen);
