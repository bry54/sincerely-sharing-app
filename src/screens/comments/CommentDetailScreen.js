import React, { PureComponent} from 'react';
import {StyleSheet, View, FlatList, Text, RefreshControl, BackHandler } from 'react-native';
import {Divider, Icon} from 'react-native-elements';
import {connect } from "react-redux";
import ColorDefinitions from '../../constants/ColorDefinitions';
import TypoDefinitions from '../../constants/TypoDefinitions';
import {SkypeIndicator, UIActivityIndicator} from 'react-native-indicators';
import {
    setActiveComment,
    fetchCommentDetail,
    setItemToComment,
    bookmarkComment,
    voteComment,
} from '../../store/utilities/actionsCollection';
import CommentListItem from '../../components/comments/CommentListItem';
import CommentDetailCard from '../../components/comments/CommentDetailCard';
import EmptyListItem from "../../components/EmptyList";

class CommentDetailScreen extends PureComponent{

    static navigationOptions = ({ navigation }) => {
        const params = navigation.state.params || {};

        return {
            title: 'Comments Thread',
            headerTintColor: ColorDefinitions.tintColor,
            //headerStyle: { backgroundColor: ColorDefinitions.header.background },
            headerTitleStyle: { fontWeight: TypoDefinitions.ultraLightText },
            headerRight: (
                <Icon
                    onPress={ () => params.handleCommentingRequest() }
                    type='ionicon'
                    name='md-create'
                    color={ColorDefinitions.tintColor}
                    iconStyle={{paddingRight:20}}/>
            ),

            headerLeft: (
                <Icon
                    onPress={ () => params.handleCustomBackButton() }
                    type='ionicon'
                    name='md-arrow-back'
                    color={ColorDefinitions.tintColor}
                    size={24}
                    iconStyle={{paddingLeft:20}}/>
            )
        };
    };

    componentDidMount = async () => {
        const { navigation } = this.props;
        BackHandler.addEventListener('hardwareBackPress', this._handleCustomBackButton );

        navigation.setParams({
            handleCommentingRequest: this._handleCommentingRequest,
            handleCustomBackButton: this._handleCustomBackButton,
        });

        this.didFocusListener = navigation.addListener (
            'didFocus', () => {
                this._fetchCommentDetail();
            },
        );
    };

    render() {
        const busyStatus = this.props.busyStatus;
        const comment = this.props.activeComment;

        if (!comment)
            return (
                <View style={styles.container}>
                    <UIActivityIndicator animating={busyStatus.isBusy}/>
                </View>
            );

        return (
            <FlatList
                refreshControl={ <RefreshControl
                    refreshing={ false }
                    onRefresh={ () => this._fetchCommentDetail('DATE')}
                /> }
                ListHeaderComponent={ this.renderCommentDetailCard(comment) }
                ListHeaderComponentStyle={{marginBottom: 5, elevation:3, backgroundColor:'#fefefe'}}
                ListEmptyComponent={comment.comments_count === 0 ? <EmptyListItem
                        icon='ios-quote'
                        message='This comment has not received any comments yet...'/>
                    :
                    <SkypeIndicator/>
                }
                initialNumToRender={10}
                data={comment.comments}
                keyExtractor={(item) => item.id.toString()}
                renderItem={this.renderCommentCard}
            />
        )
    }

    renderCommentDetailCard = (comment)=>{
        return (
            <CommentDetailCard
                comment={comment}
                isCommentBookmarked={this._isCommentBookmarked}
                isCommentUpVoted={this._isCommentUpVoted}
                isCommentDownVoted={this._isCommentDownVoted}
                toggleCommentVote={this._toggleCommentVote}
                onCommentBookmarkPress={this._onCommentBookmarkPress}
            />
        )
    };

    renderCommentCard = ({ item }) => {
        return (
            <View>
                <CommentListItem
                    comment={item}
                    isCommentBookmarked={this._isCommentBookmarked}
                    isCommentUpVoted={this._isCommentUpVoted}
                    isCommentDownVoted={this._isCommentDownVoted}
                    onOpenComment={this._onOpenComment}
                    onToggleCommentVote={this._toggleCommentVote}
                    onCommentBookmarkPress={this._onCommentBookmarkPress}
                />
                <Divider/>
            </View>
        );
    };

    _handleCustomBackButton = async () => {
        const {navigation} = this.props;
        const comment = this.props.activeComment;

        if (comment.parent_comment)
            await this.props.setActiveComment( comment.parent_comment );

        navigation.goBack();
    };

    _handleCommentingRequest = async () =>{
        const { navigation } = this.props;
        const comment = this.props.activeComment;

        await this.props.setItemToComment('commentOnComment', comment);
        navigation.navigate('commentOnComment');
    };

    _fetchCommentDetail = () => {
        const comment = this.props.activeComment;

        this.props.fetchCommentDetail(comment);
    };

    _isCommentBookmarked = (comment) =>{
        const user = this.props.loginPayload.user;
        const followers = comment.followers_ids;
        if (followers.find((follower)=> follower === user.id))
            return ColorDefinitions.interactions.includesUser;
        else
            return ColorDefinitions.interactions.idle;
    };

    _isCommentUpVoted = (comment) =>{
        const user = this.props.loginPayload.user;
        const downVoters = comment.up_voters_ids;
        if (downVoters.find((voter) => voter === user.id))
            return ColorDefinitions.interactions.includesUser;
        else
            return ColorDefinitions.interactions.idle;
    };

    _isCommentDownVoted = (comment) =>{
        const user = this.props.loginPayload.user;
        const downVoters = comment.down_voters_ids;
        if (downVoters.find((voter) => voter === user.id))
            return ColorDefinitions.interactions.includesUser;
        else
            return ColorDefinitions.interactions.idle;
    };

    _onCommentBookmarkPress = (comment) => {
        this.props.bookmarkComment(comment);
    };

    _toggleCommentVote =(vote) =>{
        this.props.voteComment(vote);
    };

    _onOpenComment = async (comment) => {
        const { navigation } = this.props;

        await this.props.setActiveComment(comment);
        navigation.push('commentAsPost');
    };
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
    },
    noComments:{
        fontSize: TypoDefinitions.normalFont,
        paddingVertical: 10,
        textAlign: 'center'
    }
});

const mapStateToProps = (state) => {
    return {
        busyStatus: state.globalReducer.busyStatus,
        loginPayload: state.authReducer.loginPayload,
        activeComment: state.feedsReducer.activeComment,
    }
};

const matchDispatchToProps = dispatch => {
    return {
        setActiveComment: (comment)=> dispatch(setActiveComment(comment)),
        setItemToComment: (route, feed)=>dispatch( setItemToComment(route, feed)),
        fetchCommentDetail: (comment) => dispatch ( fetchCommentDetail(comment) ),

        bookmarkComment: (comment)=> dispatch(bookmarkComment(comment)),
        voteComment: (vote)=>dispatch( voteComment(vote)),
    }
};
export default connect(mapStateToProps, matchDispatchToProps)(CommentDetailScreen);
