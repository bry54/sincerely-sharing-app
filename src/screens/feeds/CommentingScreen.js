import React, { PureComponent } from 'react';
import {StyleSheet, Text, View, TextInput, } from 'react-native';
import {Divider, Icon} from "react-native-elements";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {
    submitComment,
    setCommentBodyInput
} from "../../store/utilities/actionsCollection";
import {connect } from "react-redux";
import ColorDefinitions from '../../constants/ColorDefinitions';
import TypoDefinitions from '../../constants/TypoDefinitions';

class CommentingScreen extends PureComponent{

    static navigationOptions = ({ navigation }) => {
        const params = navigation.state.params || {};

        return {
            title: params.title ? params.title : 'Submit Comment',
            headerTintColor: ColorDefinitions.tintColor,
            //headerStyle: { backgroundColor: ColorDefinitions.header.background },
            headerTitleStyle: { fontWeight: TypoDefinitions.ultraLightText },
            headerRight: (
                <Icon
                    onPress={ () => params.handleCommentSubmission() }
                    type='font-awesome'
                    name='paper-plane'
                    color={ColorDefinitions.tintColor}
                    iconStyle={{paddingRight:20}}/>
            ),
        };
    };

    componentDidMount = async () => {
        const { navigation } = this.props;

        this.didFocusListener = navigation.addListener (
            'didFocus', () => {
                this._setDefaultInputs();
            },
        );

        navigation.setParams({
            handleCommentSubmission: this._submitComment
        });
    };

    render() {
        const { navigation } = this.props;
        const route = navigation.state.routeName;
        const busyStatus = this.props.busyStatus;
        const item = this.props.commenting[route];

        return (
            <KeyboardAwareScrollView>
                <View style={styles.container}>
                    <View style={styles.genericSpacing}>
                        <Text style={styles.parentBody}>
                            { this._getBodyText(item)  }
                        </Text>
                    </View>
                    <Divider/>
                    {item && <View style={styles.genericSpacing}>
                        <TextInput
                            autogrow={true}
                            value ={this.props.commentBody}
                            multiline={true}
                            placeholder='Type your reply here...'
                            onChangeText={(val) => this._updateInputState( val ) }/>
                    </View>}
                </View>
            </KeyboardAwareScrollView>
        );
    }

    _getBodyText = (item) =>{
        if(!item) return '☹️🙄🙄🙄🙄 🙄🙄🙄original item could not be displayed. Owner has deleted the item, commenting not allowed anymore';

        if(item.is_archived === 1) return '☹️🙄🙄🙄🙄 🙄🙄🙄original item could not be displayed. Owner has decided to hide the item, commenting not allowed anymore';

        return item.body;
    };

    _setDefaultInputs = () =>{
        const { navigation } = this.props;
        const comment = navigation.getParam('toEdit', null);

        this._updateInputState('');

        if (comment) this._updateInputState( comment.body );

    };

    _updateInputState = (inputValue) => {
        this.props.handleCommentBodyInput( inputValue );
    };

    _submitComment = async ( )  => {
        const { navigation } = this.props;
        const route = navigation.state.routeName;
        const item = this.props.commenting[route];
        const toEdit = navigation.getParam('toEdit', null);

        const comment = {
            comment_id: toEdit ? toEdit.id : null,
            route: route,
            item: item,
            commentBody: this.props.commentBody,
        };

        await this.props.submitComment(comment);
        navigation.goBack();
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    },

    genericSpacing:{
        paddingLeft: 10,
        paddingRight: 10,
    },

    parentBody: {
        paddingVertical: 10,
    }
});

const mapStateToProps = (state) => {
    return {
        commenting: state.feedsReducer.commenting,
        commentBody: state.feedsReducer.commentBody,
    }
};

const matchDispatchToProps = dispatch => {
    return {
        handleCommentBodyInput: (commentBody) => dispatch( setCommentBodyInput(commentBody)),

        submitComment : ( comment ) => dispatch (submitComment(comment)),
    }
};
export default connect(mapStateToProps, matchDispatchToProps)(CommentingScreen);
