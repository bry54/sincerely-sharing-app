import React, { PureComponent} from 'react';
import {StyleSheet, View, FlatList, Alert, RefreshControl} from 'react-native';
import {connect } from "react-redux";
import ColorDefinitions from '../../constants/ColorDefinitions';
import TypoDefinitions from '../../constants/TypoDefinitions';
import {UIActivityIndicator} from 'react-native-indicators';
import {
    fetchFeeds,
    setActiveFeed,
    bookmarkFeed,
    voteFeed
} from '../../store/utilities/actionsCollection'
import FeedListItem from '../../components/feeds/FeedListItem';
import EmptyListItem from "../../components/EmptyList";
import NoScreenData from "../../components/NoScreenData";
import {Icon} from "react-native-elements";

class FeedsListScreen extends PureComponent{
    static navigationOptions = ({ navigation }) => {
        const params = navigation.state.params || {};

        return {
            title: 'Feeds',
            headerTintColor: ColorDefinitions.tintColor,
            //headerStyle: { backgroundColor: ColorDefinitions.header.background },
            headerTitleStyle: { fontWeight: TypoDefinitions.ultraLightText },
            headerRight: (
                    <Icon
                        onPress={ () => params.handleSortingButton() }
                        type='font-awesome'
                        name='sort'
                        color={ColorDefinitions.tintColor}
                        iconStyle={{paddingRight:20}}/>
            )
        };
    };

    componentDidMount = async () => {
        const { navigation } = this.props;
        const route = navigation.state.routeName;

        navigation.setParams({
            handleSortingButton: this._showSortByDialog
        });

        this.didFocusListener = navigation.addListener (
            'didFocus', () => {
                if (route === 'communityFeeds' && !this.props.isCommmunityFeedsFetched) {
                    this._fetchFeeds('DATE');
                }
                if (route === 'homeFeeds' && !this.props.isHomeFeedsFetched) {
                    this._fetchFeeds('DATE');
                }
            },
        );
    };

    render() {
        const { navigation } = this.props;
        const route = navigation.state.routeName;
        const busyStatus = this.props.busyStatus;
        const feeds = this.props.feeds[route];

        if (route === 'communityFeeds' && !this.props.isCommmunityFeedsFetched){
            return (
                <NoScreenData
                    code={busyStatus.code}
                    message={busyStatus.message}
                    onExecRefresh={() => this._fetchFeeds('DATE')}
                />
            );
        }

        if (route === 'homeFeeds' && !this.props.isHomeFeedsFetched){
            return (
                <NoScreenData
                    code={busyStatus.code}
                    message={busyStatus.message}
                    onExecRefresh={() => this._fetchFeeds('DATE')}
                />
            );
        }

        return (
            <FlatList
                refreshControl={ <RefreshControl
                    refreshing={ busyStatus.isBusy }
                    onRefresh={ () => this._fetchFeeds('DATE', true)}
                /> }
                initialNumToRender={10}
                keyExtractor={(item) => item.id.toString()}
                data={feeds}
                renderItem={this.renderItem}
                ListEmptyComponent={<EmptyListItem
                    icon='ios-color-filter'
                    message='You have not subscribed to any community yet, subscribe to a community to get relevant feeds'/>
                }
                ListFooterComponent={<View style={{ height: 0, marginBottom: 0 }} />}
            />
        )
    }

    renderItem = ({ item }) => (
        <FeedListItem
            feed={item} user={null}
            openFeed={this._openFeed}
            toggleFeedVote={this._toggleFeedVote}
            toggleFeedBookmark={this._toggleFeedBookmark}
            isBookmarked={this._isBookmarked}
            isUpVoted={this._isUpVoted}
            isDownVoted={this._isDownVoted}
        />
    );

    _fetchFeeds = (criterion, refreshing = false) => {
        const { navigation } = this.props;
        const route = navigation.state.routeName;
        const activeCommunity = this.props.activeCommunity;

        if (refreshing){
            if (route === 'communityFeeds') this.props.fetchFeeds(route, activeCommunity, criterion);

            if (route === 'homeFeeds') this.props.fetchFeeds(route, null, criterion);
        }

        if (route === 'communityFeeds' && !this.props.isCommmunityFeedsFetched)
            this.props.fetchFeeds(route, activeCommunity, criterion);

        if (route === 'homeFeeds' && !this.props.isHomeFeedsFetched)
            this.props.fetchFeeds(route, null, criterion);
    };

    _openFeed = async (feed) => {
        const { navigation } = this.props;
        const route = navigation.state.routeName;

        if (route === 'homeFeeds') {
            await this.props.setActiveFeed('homeFeedDetail', feed);
            navigation.navigate('homeFeedDetail');
        } else if (route === 'communityFeeds') {
            await this.props.setActiveFeed('communityFeedDetail', feed);
            navigation.navigate('communityFeedDetail');
        }
    };

    _showSortByDialog = () =>{
        Alert.alert(
            'Sort By',
            'Choose sorting criterion for the feeds',
            [
                {
                    text: 'By Confidence',
                    onPress: () => this._fetchFeeds('CONFIDENCE', true)
                },
                {
                    text: 'By Date',
                    onPress: () => this._fetchFeeds('DATE', true)
                },
            ],
            { cancelable: true }
        )
    };

    _toggleFeedBookmark = (feed) =>{
        this.props.bookmarkFeed(feed);
    };

    _toggleFeedVote =(vote) =>{
        this.props.voteFeed(vote);
    };

    _isBookmarked = (feed)=> {
        const user = this.props.loginPayload.user;
        const followers = feed.followers_ids;
        if (followers.find((follower)=> follower === user.id))
            return ColorDefinitions.interactions.includesUser;
        else
            return ColorDefinitions.interactions.idle;
    };

    _isUpVoted = (feed)=> {
        const user = this.props.loginPayload.user;
        const upVoters = feed.up_voters_ids;
        if (upVoters.find((voter) => voter === user.id))
            return ColorDefinitions.interactions.includesUser;
        else
            return ColorDefinitions.interactions.idle;
    };

    _isDownVoted = (feed)=> {
        const user = this.props.loginPayload.user;
        const downVoters = feed.down_voters_ids;
        if (downVoters.find((voter) => voter === user.id))
            return ColorDefinitions.interactions.includesUser;
        else
            return ColorDefinitions.interactions.idle;
    };
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
    },
});

const mapStateToProps = (state) => {
    return {
        busyStatus: state.globalReducer.busyStatus,
        loginPayload: state.authReducer.loginPayload,
        activeCommunity: state.communitiesReducer.activeCommunity,
        feeds: state.feedsReducer.feeds,
        isCommmunityFeedsFetched: state.feedsReducer.feeds.isCommmunityFeedsFetched,
        isHomeFeedsFetched: state.feedsReducer.feeds.isHomeFeedsFetched,
    }
};

const matchDispatchToProps = dispatch => {
    return {
        fetchFeeds: (route, community, criterion) => dispatch ( fetchFeeds(route,community,criterion) ),
        setActiveFeed: (route, feed) =>dispatch( setActiveFeed(route, feed) ),
        bookmarkFeed: (feed) => dispatch( bookmarkFeed(feed) ),
        voteFeed: (vote)=>dispatch( voteFeed(vote)),
    }
};
export default connect(mapStateToProps, matchDispatchToProps)(FeedsListScreen);
