import React, { PureComponent} from 'react';
import {StyleSheet, View, FlatList, Text, RefreshControl } from 'react-native';
import {Avatar, ButtonGroup, Divider, Icon, ListItem} from 'react-native-elements';
import {connect } from "react-redux";
import ColorDefinitions from '../../constants/ColorDefinitions';
import TypoDefinitions from '../../constants/TypoDefinitions';
import {SkypeIndicator, UIActivityIndicator} from 'react-native-indicators';
import {
    bookmarkFeed,
    fetchFeedDetail,
    voteFeed,
    setActiveComment,
    bookmarkComment,
    voteComment,
    setItemToComment
} from '../../store/utilities/actionsCollection';
import FeedsDetailCard from '../../components/feeds/FeedDetail';
import CommentListItem from '../../components/comments/CommentListItem';
import EmptyListItem from "../../components/EmptyList";
import UnavailableHeaderItem from '../../components/UnavailableHeaderItem';

class FeedsDetailScreen extends PureComponent{

    static navigationOptions = ({ navigation }) => {
        const params = navigation.state.params || {};

        return {
            title: 'Feed Thread',
            headerTintColor: ColorDefinitions.tintColor,
            //headerStyle: { backgroundColor: ColorDefinitions.header.background },
            headerTitleStyle: { fontWeight: TypoDefinitions.ultraLightText },
            headerRight: (
                <Icon
                    onPress={ () => params.handleCommentingRequest() }
                    type='ionicon'
                    name='md-create'
                    color={ColorDefinitions.tintColor}
                    iconStyle={{paddingRight:20}}/>
            ),
        };
    };

    componentDidMount = async () => {
        const { navigation } = this.props;

        navigation.setParams({
            handleCommentingRequest: this._handleCommentingRequest
        });

        this.didFocusListener = navigation.addListener (
            'didFocus', () => {
                this._fetchFeedDetail();
            },
        );
    };

    render() {
        const { navigation } = this.props;
        const route = navigation.state.routeName;
        const busyStatus = this.props.busyStatus;
        const feed = this.props.activeFeed[route];

        if (!feed)
            return (
                <View style={styles.container}>
                    <UIActivityIndicator animating={busyStatus.isBusy}/>
                    <Text style={{textAlign: 'center'}}>Fetching Feed Detail</Text>
                </View>
            );

        return (
            <FlatList
                refreshControl={ <RefreshControl
                    refreshing={ false }
                    onRefresh={ () => this._fetchFeedDetail('DATE')}
                /> }
                ListHeaderComponent={ this.renderFeedDetailCard(feed) }
                ListHeaderComponentStyle={{marginBottom: 5, elevation:3, backgroundColor:'#fefefe'}}
                ListEmptyComponent={ feed.comments_count === 0 ?
                    <EmptyListItem
                        icon='ios-quote'
                        message='This comment has not received any comments yet...'/>
                    :
                    <SkypeIndicator/>
                }
                initialNumToRender={10}
                data={feed.comments}
                keyExtractor={(item) => item.id.toString()}
                renderItem={this.renderCommentCard}
            />
        )
    }

    renderFeedDetailCard = (feed)=>{
        if (feed.is_deleted || feed.is_archived)
            return (
                <UnavailableHeaderItem
                item={feed}/>
            );

        return (
            <FeedsDetailCard
                feed={feed}
                isFeedBookmarked={this._isFeedBookmarked}
                isFeedUpVoted={this._isFeedUpVoted}
                isFeedDownVoted={this._isFeedDownVoted}
                onToggleFeedVote={this._toggleFeedVote}
                onFeedBookmarkPress={this._onFeedBookmarkPress}
            />
        )
    };

    renderCommentCard = ({ item }) => {
        return (
            <View>
                <CommentListItem
                    comment={item}
                    isCommentBookmarked={this._isCommentBookmarked}
                    isCommentUpVoted={this._isCommentUpVoted}
                    isCommentDownVoted={this._isCommentDownVoted}
                    onOpenComment={this._onOpenComment}
                    onToggleCommentVote={this._toggleCommentVote}
                    onCommentBookmarkPress={this._onCommentBookmarkPress}
                />
                <Divider/>
            </View>
        );
    };

    _handleCommentingRequest = async () =>{
        const { navigation } = this.props;
        const route = navigation.state.routeName;
        const feed = this.props.activeFeed[route];

        await this.props.setItemToComment('commentOnFeed', feed);
        navigation.navigate('commentOnFeed');
    };

    _fetchFeedDetail = () => {
        const { navigation } = this.props;
        const route = navigation.state.routeName;
        const feed = this.props.activeFeed[route];

        this.props.fetchFeedDetail(route, feed);
    };

    _isCommentBookmarked = (comment) =>{
        const user = this.props.loginPayload.user;
        const followers = comment.followers_ids;
        if (followers.find((follower)=> follower === user.id))
            return ColorDefinitions.interactions.includesUser;
        else
            return ColorDefinitions.interactions.idle;
    };

    _isCommentUpVoted = (comment) =>{
        const user = this.props.loginPayload.user;
        const downVoters = comment.up_voters_ids;
        if (downVoters.find((voter) => voter === user.id))
            return ColorDefinitions.interactions.includesUser;
        else
            return ColorDefinitions.interactions.idle;
    };

    _isCommentDownVoted = (comment) =>{
        const user = this.props.loginPayload.user;
        const downVoters = comment.down_voters_ids;
        if (downVoters.find((voter) => voter === user.id))
            return ColorDefinitions.interactions.includesUser;
        else
            return ColorDefinitions.interactions.idle;
    };

    _onOpenComment = async (comment) => {
        const { navigation } = this.props;

        await this.props.setActiveComment(comment);
        navigation.navigate('commentAsPost');
    };

    _onCommentBookmarkPress = (comment) => {
        this.props.bookmarkComment(comment);
    };

    _toggleCommentVote =(vote) =>{
        this.props.voteComment(vote)
    };

    _isFeedBookmarked = (feed) =>{
        const user = this.props.loginPayload.user;
        const followers = feed.followers_ids;
        if (followers.find((follower)=> follower === user.id))
            return ColorDefinitions.interactions.includesUser;
        else
            return ColorDefinitions.interactions.idle;
    };

    _isFeedUpVoted = (feed) =>{
        const user = this.props.loginPayload.user;
        const upVoters = feed.up_voters_ids;
        if (upVoters.find((voter)=> voter === user.id))
            return ColorDefinitions.interactions.includesUser;
        else
            return ColorDefinitions.interactions.idle;
    };

    _isFeedDownVoted = (feed) =>{
        const user = this.props.loginPayload.user;
        const downVoters = feed.down_voters_ids;
        if (downVoters.find((voter)=> voter === user.id))
            return ColorDefinitions.interactions.includesUser;
        else
            return ColorDefinitions.interactions.idle;
    };

    _onFeedBookmarkPress = (feed) => {
        this.props.bookmarkFeed(feed)
    };

    _toggleFeedVote =(feed) =>{
        this.props.voteFeed(feed);
    };
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
    },
    noComments:{
        fontSize: TypoDefinitions.normalFont,
        paddingVertical: 10,
        textAlign: 'center'
    }
});

const mapStateToProps = (state) => {
    return {
        busyStatus: state.globalReducer.busyStatus,
        loginPayload: state.authReducer.loginPayload,
        activeFeed: state.feedsReducer.activeFeed,
    }
};

const matchDispatchToProps = dispatch => {
    return {
        fetchFeedDetail: (route, feed) => dispatch ( fetchFeedDetail(route, feed) ),
        bookmarkFeed: (feed)=>dispatch( bookmarkFeed(feed)),
        voteFeed: (vote)=>dispatch( voteFeed(vote)),

        setActiveComment: (comment)=> dispatch(setActiveComment(comment)),
        bookmarkComment: (comment)=> dispatch(bookmarkComment(comment)),
        voteComment: (vote)=>dispatch( voteComment(vote)),

        setItemToComment: (route, feed)=>dispatch( setItemToComment(route, feed)),
    }
};
export default connect(mapStateToProps, matchDispatchToProps)(FeedsDetailScreen);
