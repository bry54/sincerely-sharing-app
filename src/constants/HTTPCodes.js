export default {
    CREATED: 201,
    OK: 200,
    INTERNAL_SERVER_ERROR: 500,
    GATEWAY_TIMEOUT: 504
}
