const tintColor = '#2f95dc';
const shade0 = '#502C73';
const shade1 = '#9277AC';
const shade2 = '#6F4D8F';
const shade3 = '#351456';
const shade4 = '#1F0439';

export default {
    tintColor,
    errorBackground: '#ff0000',
    errorText: '#fff',
    warningBackground: '#EAEB5E',
    warningText: '#666804',
    noticeBackground: tintColor,
    noticeText: '#fff',

    mainBackground: '#E8EAF6',
    colorPrimaryDark: '',
    colorPrimary: '',
    colorAccent: '',

    tabs : {
        main : {
            tabIconDefault: '#ccc',
            tabIconSelected: tintColor,
            tabBar: '#fefefe',
        },
        top:{
            tabBackground:'#fefefe',
            tabText:tintColor,
            tabActiveIndicator:tintColor,
            tabIconDefault: 'grey',
            tabIconSelected: tintColor,
        }
    },

    interactions:{
        idle:'grey',
        includesUser: '#f50'
    },

    auth:{
        background: '',
        input: '#212121',
        inputBorder: '#78909c',
        label:'#000',
        button:'#2f95dc',
        buttonText:'#fff',
    },

    cards:{
        idle: 'grey',
        deletedText: '#c8c8c8',
        editable: '#2f95dc',
        deleted: '#DD2C00',
        archived: '#FB8C00'
    },

    snackBar:{
        genericBackground: '#263238',
    }
}
