const thinText = '100';
const ultraLightText = '200';
const lightText = '300';
const normalText = '400';
const mediumText = '500';
const semiBoldText = '600';
const boldText = '700';
const extraBoldText = '800';
const heavyText = '900';

const tinyFont = 6;
const x4SmallFont = 10;
const xxxSmallFont = 12;
const xxSmallFont = 13;
const xSmallFont = 14;
const smallFont = 15;
const normalFont = 16;
const bigFont = 17;
const xBigFont = 18;
const xxBigFont = 19;
const xxxBigFont = 20;
const x4BigFont = 22;

export default {
    thinText, ultraLightText, lightText, normalText, mediumText, semiBoldText, boldText, extraBoldText, heavyText,
    tinyFont, x4SmallFont, xxxSmallFont, xxSmallFont, xSmallFont, smallFont, normalFont, bigFont, xBigFont, xxBigFont,
    xxxBigFont, x4BigFont
}
