export const privacyPolicy = [
    {
        title:'Privacy Policy (Last Updated: 05.02.2019)',
        body:'This page informs you of our policies regarding the collection, use and disclosure of information we receive from users of the application.',
    },
    {
        title:'Information Collection And Use',
        body:'While using the mobile application, the only information we will remotely store is your alias and email for authentication purposes only. We respect your anonymity and no information personally identifiable with you will be used.',
    },
    {
        title:'Security',
        body:'The security of your Personal Information is important to us, but remember that no method of transmission over the Internet, or method of electronic storage, is 100% secure. While we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security.',
    },
    {
        title:'Contact Us',
        body:'If you have any questions about this Privacy Policy, please contact us',
    }
];

export const termsOfUse = [
    {
        title:'Terms of Use ("Terms")',
        body:'Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all users who access or use the Service. By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.',
    },
    {
        title:'Content',
        body:'Our Service allows you to post, link, store, share and otherwise make available certain information, text, graphics, videos, or other material ("Content").',
    },
    {
        title:'Purchases and Sales',
        body:'This application serves no for commercial reasons. No purchasing or selling is permitted in this application',
    },
    {
        title:'Termination',
        body:'We may terminate or suspend access to our Service immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms.',
    }
];
