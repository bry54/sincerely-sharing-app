const AT_HOME = true;

const HOME_DOMAIN = 'http://192.168.0.104/sincerely-sharing/api';
const OFFICE_DOMAIN = 'http://172.16.42.16/sincerely-sharing/api';

const DOMAIN = AT_HOME ? HOME_DOMAIN : OFFICE_DOMAIN;

export default {
    LOGIN: DOMAIN+ '/auth/login',
    LOGOUT: DOMAIN+ '/auth/logout',

    FETCH_ALL_COMMUNITIES: DOMAIN+ '/communities/get-all-communities',
    FETCH_SUBSCRIBED_COMMUNITIES: DOMAIN+ '/communities/get-subscribed-communities',
    TOGGLE_COMMUNITY_SUBSCRIPTION: DOMAIN+ '/communities/toggle-community-subscription',
    REQUEST_COMMUNITY: DOMAIN+ '/communities/request-community',
    VOTE_REQUESTED_COMMUNITY: DOMAIN+ '/communities/vote-request-community',

    FETCH_FEEDS: DOMAIN+ '/posts/get-posts',
    FETCH_FEED_DETAIL: DOMAIN+ '/posts/get-post-detail',
    BOOKMARK_FEED: DOMAIN+ '/posts/follow-post',
    VOTE_FEED: DOMAIN+ '/posts/submit-vote-for-post',

    SUBMIT_POST: DOMAIN+ '/posts/submit-post',
    DELETE_POST: DOMAIN+ '/posts/delete-post',
    ARCHIVE_POST: DOMAIN+ '/posts/archive-post',

    FETCH_COMMENT_DETAIL: DOMAIN+ '/comments/get-comment-detail',
    VOTE_COMMENT: DOMAIN+ '/comments/submit-vote-for-comment',
    BOOKMARK_COMMENT: DOMAIN+ '/comments/follow-comment',

    COMMENT_ON_FEED: DOMAIN+ '/comments/submit-comment-for-post',
    COMMENT_ON_COMMENT: DOMAIN+ '/comments/submit-comment-for-comment',
    DELETE_COMMENT: DOMAIN+ '/comments/delete-comment',
    ARCHIVE_COMMENT: DOMAIN+ '/comments/archive-comment',

    FETCH_PROFILE_DATA: DOMAIN+ '/user/my-profile',
}
