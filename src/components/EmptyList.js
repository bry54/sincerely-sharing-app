import React from 'react';
import {View, StyleSheet} from "react-native";
import {Icon, Text} from "react-native-elements";
import ColorsDefinitions from "../constants/ColorDefinitions";

export default (props) => (
    <View style={styles.emptyListStyle}>
        <Icon
            name={ props.icon }
            type='ionicon'
            size={40}
            iconStyle={{ paddingBottom: 10 }}
            color={ ColorsDefinitions.interactions.idle} />

        <Text style={styles.emptyMessageStyle}>{props.message}</Text>
    </View>
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    emptyListStyle: {
        flex: 1,
        paddingVertical:30,
        alignItems: 'center',
        justifyContent: 'center'
    },

    emptyMessageStyle: {
        textAlign: 'center',
        color: ColorsDefinitions.interactions.idle
    }
});
