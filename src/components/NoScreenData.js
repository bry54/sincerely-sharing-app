import React from 'react';
import {RefreshControl, View, StyleSheet} from "react-native";
import {Icon, Text} from "react-native-elements";
import ColorsDefinitions from "../constants/ColorDefinitions";
import {UIActivityIndicator} from "react-native-indicators";
import HTTPCodes from "../constants/HTTPCodes";

export default (props) => (
    <View style={[styles.container, {alignItems: 'center', justifyContent: 'center'}]}>

        {getImageView( props.code, props.onExecRefresh) }

        <Text style={{textAlign: 'center', paddingBottom: 20}}>{props.message}</Text>
    </View>
);

const getImageView = (code, action) =>{
    if (code === HTTPCodes.CREATED) return (<UIActivityIndicator animating={true}/>);
    if (code === HTTPCodes.GATEWAY_TIMEOUT) return (<Icon
        raised={false}
        iconStyle={{ paddingBottom: 10 }}
        name={'md-refresh-circle'}
        size={60}
        type='ionicon'
        onPress={() => action()}/>)
};

const styles = StyleSheet.create({
    container:{
        flex: 1,
    },
});
