import React,{PureComponent} from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {Avatar, Divider, Icon} from 'react-native-elements';
import TypoDefinitions from '../../constants/TypoDefinitions';
import moment from 'moment-timezone';
import ColorDefinitions from '../../constants/ColorDefinitions';

export default class CommentListItem extends PureComponent{
    render(){
        let comment = this.props.comment;

        return (
            <TouchableOpacity style={styles.container} onPress={()=>this._openComment(comment)}>
                <View style={styles.cardContainer}>
                    <View style={styles.cardBody}>
                        <Text
                            numberOfLines={4}
                            ellipsizeMode='tail'
                            style={styles.bodySummary}>
                            {comment.body}
                        </Text>

                        <View style={styles.userAndPostingTime}>
                            <Icon
                                iconStyle={{paddingTop:4, paddingLeft:3,paddingRight:5}}
                                name='md-person'
                                color={ColorDefinitions.cards.idle}
                                type='ionicon'
                                size={TypoDefinitions.xxSmallFont }/>
                            <Text style={styles.bodyCaption}> {comment.author.username} </Text>
                            <Icon
                                iconStyle={{paddingTop:3, paddingLeft:3,paddingRight:5}}
                                name='ios-water'
                                color={ColorDefinitions.cards.idle}
                                type='ionicon'
                                size={TypoDefinitions.xxSmallFont}/>
                            <Text style={styles.bodyCaption}>{moment(comment.created_at).fromNow()}</Text>
                        </View>
                    </View>
                    <Divider/>
                    <View style={styles.cardFooter}>
                        <View style={styles.comboContainer}>
                            <TouchableOpacity onPress={() => this._onUpVotePress(comment)}>
                                <Icon
                                    name='md-arrow-round-up'
                                    type='ionicon'
                                    color={this._isUpVoted(comment)}
                                    size={TypoDefinitions.smallFont}
                                    iconStyle={{ paddingLeft:6, paddingRight:6, paddingTop:3 }}/>
                            </TouchableOpacity>
                            <Text style={styles.footerLabel}> vote{/*{comment.votes_count}*/} </Text>
                            <TouchableOpacity onPress={() => this._onDownVotePress(comment)}>
                                <Icon
                                    name='md-arrow-round-down'
                                    type='ionicon'
                                    color={this._isDownVoted(comment)}
                                    size={TypoDefinitions.smallFont}
                                    iconStyle={{ paddingTop:3, paddingLeft:4, paddingRight: 4 }}/>
                            </TouchableOpacity>
                        </View>

                        <TouchableOpacity>
                            <View style={styles.comboContainer}>
                                <Icon
                                    type='ionicon'
                                    name='md-chatboxes'
                                    color={ColorDefinitions.cards.idle}
                                    size={TypoDefinitions.smallFont}
                                    iconStyle={{ paddingTop:3, paddingLeft: 20, paddingRight: 2 }}/>
                                <Text style={styles.footerLabel}> {comment.comments_count} </Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this._onBookmarkPress( comment )}>
                            <Icon
                                name='ios-bookmark'
                                type='ionicon'
                                color={this._isBookmarked(comment)}
                                size={TypoDefinitions.smallFont}
                                iconStyle={{ paddingLeft:20, paddingTop:3 }}/>
                        </TouchableOpacity>
                    </View>
                </View>
            </TouchableOpacity>
        )
    };

    _openComment = (comment) =>{
        this.props.onOpenComment(comment)
    };

    _onBookmarkPress = (comment) => {
        this.props.onCommentBookmarkPress(comment)
    };

    _onUpVotePress = (comment) => {
        const vote = {
            commentId: comment.id,
            up: 1,
            down: 0
        };
        this.props.onToggleCommentVote(vote)
    };

    _onDownVotePress = (comment) => {
        const vote = {
            commentId: comment.id,
            up: 0,
            down: 1
        };
        this.props.onToggleCommentVote(vote)
    };

    _isUpVoted(comment) {
        return this.props.isCommentUpVoted(comment)
    };

    _isDownVoted(comment) {
        return this.props.isCommentDownVoted(comment)
    };

    _isBookmarked = (comment) =>{
        return this.props.isCommentBookmarked(comment)
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 0,
        padding: 10,
    },

    cardContainer: {
        flex:0,
        flexDirection:'column'
    },

    cardHeader: {
        flex:0,
        flexDirection: 'row',
    },

    headerIcon:{
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    },

    headerText:{
        flexDirection: 'column',
        justifyContent: 'center',
        paddingLeft: 10,
        paddingRight: 10
    },

    headerTitle:{
        fontSize: TypoDefinitions.smallFont,
        fontWeight: TypoDefinitions.normalText
    },

    headerCaption:{
        fontSize: TypoDefinitions.xSmallFont,
        fontWeight: TypoDefinitions.normalText
    },

    cardBody: {
        flex:0,
        flexDirection: 'column',
        paddingBottom: 10
    },

    bodyTitle:{
        fontSize: TypoDefinitions.normalFont,
        fontWeight: TypoDefinitions.boldText,
        paddingBottom: 5,
    },

    bodySummary:{
        fontSize: TypoDefinitions.smallFont,
        fontWeight: TypoDefinitions.normalText,
        paddingBottom: 5
    },

    bodyCaption:{
        textAlign: 'right',
        fontSize: TypoDefinitions.xSmallFont,
        fontWeight: TypoDefinitions.lightText,
        color: ColorDefinitions.cards.idle
    },

    cardFooter: {
        paddingTop:5,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },

    comboContainer:{
        flexDirection: 'row',
        alignContent:'center',
        justifyContent: 'space-between'
    },
    footerLabel: {
        //paddingBottom:2,
        paddingLeft:2,
        paddingRight:2,
        fontSize: TypoDefinitions.xSmallFont,
        fontWeight: TypoDefinitions.normalText,
        color:ColorDefinitions.cards.idle
    },

    footerIcon: {
        paddingTop:2
    },

    userAndPostingTime:{
        flexDirection: 'row',
        justifyContent: 'flex-start'
    }
});
