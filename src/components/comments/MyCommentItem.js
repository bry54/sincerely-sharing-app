import React,{PureComponent} from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {Avatar, Divider, Icon} from 'react-native-elements';
import Helpers from "../../utilities/Helpers";
import TypoDefinitions from '../../constants/TypoDefinitions';
import moment from 'moment-timezone';
import ColorDefinitions from '../../constants/ColorDefinitions';

export default class CommentListItem extends PureComponent{
    render(){
        let comment = this.props.comment;

        return (
            <TouchableOpacity style={styles.container} onPress={()=>this._openComment(comment)}>
                <View style={styles.cardContainer}>
                    <View style={styles.cardHeader}>
                        <View style={styles.compactComboContainer}>
                            <Icon
                                name='ios-hourglass'
                                type='ionicon'
                                color={ColorDefinitions.cards.idle}
                                size={TypoDefinitions.normalFont}
                                iconStyle={{ paddingRight:6, paddingTop:2 }}/>
                            <Text style={styles.footerLabel}> {moment(comment.created_at).fromNow()} </Text>
                            <Text style={styles.footerLabel}> replying to </Text>
                            <Icon
                                name='md-person'
                                type='ionicon'
                                color={ColorDefinitions.cards.idle}
                                size={TypoDefinitions.normalFont}
                                iconStyle={{ paddingRight:6, paddingLeft:6,paddingTop:2 }}/>
                            <Text style={styles.footerLabel}> {comment.author.username} </Text>
                        </View>
                        <View style={styles.compactComboContainer}>
                            <Icon
                                name='ios-bonfire'
                                type='ionicon'
                                color={ColorDefinitions.cards.idle}
                                size={TypoDefinitions.normalFont}
                                iconStyle={{ paddingRight:6, paddingTop:2 }}/>
                            <Text style={styles.footerLabel}>has {comment.comments_count} replies | voted {comment.votes_count} times</Text>
                        </View>
                    </View>

                    <View style={styles.cardBody}>
                        <Text
                            numberOfLines={4}
                            ellipsizeMode='tail'
                            style={styles.bodySummary}>
                            {comment.body}
                        </Text>
                    </View>
                    <Divider/>
                    <View style={styles.cardFooter}>
                        <View style={styles.comboContainer}>
                            <TouchableOpacity onPress={() => this._onEditPress( comment )}>
                                <Icon
                                    name='ios-create'
                                    type='ionicon'
                                    color={ColorDefinitions.cards.editable}
                                    size={TypoDefinitions.smallFont}
                                    iconStyle={{ paddingLeft:20, paddingRight:6, paddingTop:2 }}/>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this._onArchivePress(comment)}>
                                <Icon
                                    name={comment.is_archived === 1 ? 'ios-eye-off' : 'ios-eye'}
                                    type='ionicon'
                                    color={comment.is_archived === 1 ? ColorDefinitions.cards.archived : ColorDefinitions.cards.idle}
                                    size={TypoDefinitions.smallFont}
                                    iconStyle={{ paddingLeft:20, paddingRight:6, paddingTop:2 }}/>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this._onDeletePress(comment)}>
                                <Icon
                                    name='md-trash'
                                    type='ionicon'
                                    color={ColorDefinitions.cards.deleted}
                                    size={TypoDefinitions.smallFont}
                                    iconStyle={{ paddingLeft: 20, paddingRight: 6, paddingTop:2 }}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    };

    _openComment = (comment) =>{
        this.props.onOpenComment(comment)
    };

    _onDeletePress = (comment) => {
        this.props.onDeletePress(comment)
    };

    _onArchivePress = (comment) => {
        this.props.onArchivePress(comment)
    };

    _onEditPress = (comment) => {
        this.props.onEditPress(comment)
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 0,
        padding: 10,
    },

    cardContainer: {
        flex:0,
        flexDirection:'column'
    },

    cardHeader: {
        flex:0,
    },

    headerIcon:{
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    },

    headerText:{
        flexDirection: 'column',
        justifyContent: 'center',
        paddingLeft: 10,
        paddingRight: 10
    },

    headerTitle:{
        fontSize: TypoDefinitions.smallFont,
        fontWeight: TypoDefinitions.normalText
    },

    headerCaption:{
        fontSize: TypoDefinitions.xSmallFont,
        fontWeight: TypoDefinitions.normalText
    },

    cardBody: {
        flex:0,
        flexDirection: 'column',
        paddingBottom: 10
    },

    bodyTitle:{
        fontSize: TypoDefinitions.normalFont,
        fontWeight: TypoDefinitions.boldText,
        paddingBottom: 5,
    },

    bodySummary:{
        fontSize: TypoDefinitions.smallFont,
        fontWeight: TypoDefinitions.normalText,
        paddingVertical: 5
    },

    bodyCaption:{
        textAlign: 'right',
        fontSize: TypoDefinitions.xSmallFont,
        fontWeight: TypoDefinitions.lightText,
    },

    cardFooter: {
        paddingTop:5,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },

    comboContainer:{
        flexDirection: 'row',
        alignContent:'center',
        justifyContent: 'space-between'
    },

    compactComboContainer:{
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },

    footerLabel: {
        //paddingBottom:2,
        paddingLeft:2,
        paddingRight:2,
        fontSize: TypoDefinitions.xSmallFont,
        fontWeight: TypoDefinitions.normalText,
        color: ColorDefinitions.cards.idle
    },

    footerIcon: {
        paddingTop:2
    },

    userAndPostingTime:{
        flexDirection: 'row',
        justifyContent: 'flex-start'
    }
});
