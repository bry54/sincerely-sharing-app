import React from 'react';
import { Icon } from 'react-native-elements';

import Colors from '../constants/ColorDefinitions';

export default class TabBarIcon extends React.PureComponent {
    render() {
        return (
            <Icon
                name={ this.props.name }
                type='ionicon'
                size={20}
                iconStyle={{ marginBottom: -3 }}
                color={ this.props.focused ? Colors.tabs.top.tabIconSelected : Colors.tabs.top.tabIconDefault } />
        );
    }
}
