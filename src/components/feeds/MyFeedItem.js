import React, { PureComponent} from 'react';
import {StyleSheet, View, Text, TouchableOpacity, Animated} from 'react-native';
import {Divider, Icon} from 'react-native-elements';

import TypoDefinitions from "../../constants/TypoDefinitions";
import moment from 'moment-timezone';
import ColorDefinitions from '../../constants/ColorDefinitions';
import Helpers from '../../utilities/Helpers';

export default class MyFeedItem extends PureComponent{

    render(){
        let feed = this.props.feed;

        return (
            <TouchableOpacity onPress={()=>this._openFeed(feed)}>
                <View style={styles.container}>
                    <View style={styles.cardContainer}>
                        <View style={styles.cardHeader}>
                            <View style={styles.compactComboContainer}>
                                <Icon
                                    name='ios-hourglass'
                                    type='ionicon'
                                    color={ColorDefinitions.cards.idle}
                                    size={TypoDefinitions.normalFont}
                                    iconStyle={{ paddingRight:6, paddingTop:2 }}/>
                                <Text style={styles.footerLabel}> {moment(feed.created_at).fromNow()} in </Text>
                                <Icon
                                    name='ios-color-filter'
                                    type='ionicon'
                                    color={ColorDefinitions.cards.idle}
                                    size={TypoDefinitions.normalFont}
                                    iconStyle={{ paddingRight:6, paddingLeft:6,paddingTop:2 }}/>
                                <Text style={styles.footerLabel}> {Helpers.titleCaseString(feed.community.name)} </Text>
                            </View>
                            <View style={styles.compactComboContainer}>
                                <Icon
                                    name='ios-bonfire'
                                    type='ionicon'
                                    color={ColorDefinitions.cards.idle}
                                    size={TypoDefinitions.normalFont}
                                    iconStyle={{ paddingRight:6, paddingTop:2 }}/>
                                <Text style={styles.footerLabel}>has {feed.comments_count} replies | voted {feed.votes_count} times</Text>
                            </View>
                        </View>

                        <View style={styles.cardBody}>
                            <Text style={styles.bodyTitle}
                                  numberOfLines={3}
                                  ellipsizeMode='tail'>
                                { feed.title }
                            </Text>
                            <Text style={styles.bodySummary}
                                  numberOfLines={5}
                                  ellipsizeMode='tail'>
                                { feed.body }
                            </Text>
                        </View>

                        <Divider/>

                        <View style={styles.cardFooter}>
                            <View style={styles.comboContainer}>
                                <TouchableOpacity onPress={() => this._onEditPress( feed )}>
                                    <Icon
                                        name='ios-create'
                                        type='ionicon'
                                        color={ColorDefinitions.cards.editable}
                                        size={TypoDefinitions.smallFont}
                                        iconStyle={{ paddingLeft:20, paddingRight:6, paddingTop:2 }}/>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this._onArchivePress(feed)}>
                                    <Icon
                                        name={feed.is_archived === 1 ? 'ios-eye-off' : 'ios-eye'}
                                        type='ionicon'
                                        color={feed.is_archived === 1 ? ColorDefinitions.cards.archived : ColorDefinitions.cards.idle}
                                        size={TypoDefinitions.smallFont}
                                        iconStyle={{ paddingLeft:20, paddingRight:6, paddingTop:2 }}/>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this._onDeletePress(feed)}>
                                    <Icon
                                        name='md-trash'
                                        type='ionicon'
                                        color={ColorDefinitions.cards.deleted}
                                        size={TypoDefinitions.smallFont}
                                        iconStyle={{ paddingLeft: 20, paddingRight: 6, paddingTop:2 }}/>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    };

    _openFeed = (feed) =>{
        this.props.onOpenFeed(feed)
    };

    _onDeletePress = (feed) => {
        this.props.onDeletePress(feed)
    };

    _onArchivePress = (feed) => {
        this.props.onArchivePress(feed)
    };

    _onEditPress = (feed) => {
        this.props.onEditPress(feed)
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 0,
        padding: 10,
        marginBottom:10,
        elevation: 3,
        backgroundColor:'#fefefe'
    },

    cardContainer: {
        flex:0,
        flexDirection:'column'
    },

    cardHeader: {
        flex:0,
    },

    headerIcon:{
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    },

    headerText:{
        flexDirection: 'column',
        justifyContent: 'center',
        paddingLeft: 10,
        paddingRight: 10
    },

    headerTitle:{
        fontSize: TypoDefinitions.smallFont,
        fontWeight: TypoDefinitions.normalText
    },

    headerCaption:{
        fontSize: TypoDefinitions.xSmallFont,
        fontWeight: TypoDefinitions.normalText
    },

    cardBody: {
        flex:0,
        flexDirection: 'column',
        paddingTop: 10,
        paddingBottom: 10
    },

    bodyTitle:{
        fontSize: TypoDefinitions.normalFont,
        fontWeight: TypoDefinitions.boldText,
        paddingBottom: 5,
    },

    bodySummary:{
        fontSize: TypoDefinitions.smallFont,
        fontWeight: TypoDefinitions.normalText,
        paddingBottom: 5
    },

    bodyCaption:{
        textAlign: 'right',
        fontSize: TypoDefinitions.xxSmallFont,
        fontWeight: TypoDefinitions.lightText,
        color:ColorDefinitions.cards.idle
    },

    cardFooter: {
        paddingTop:5,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },

    comboContainer:{
        flexDirection: 'row',
        alignContent:'center',
        justifyContent: 'space-between'
    },

    compactComboContainer:{
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    footerLabel: {
        //paddingBottom:2,
        paddingLeft:2,
        paddingRight:2,
        fontSize: TypoDefinitions.xxSmallFont,
        fontWeight: TypoDefinitions.normalText,
        color:ColorDefinitions.cards.idle
    },

    footerIcon: {
        paddingTop:2
    },

    userAndPostingTime:{
        flexDirection: 'row',
        justifyContent: 'flex-end'
    }
});
