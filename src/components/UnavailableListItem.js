import React,{PureComponent} from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {Avatar, Divider, Icon} from 'react-native-elements';
import TypoDefinitions from '../constants/TypoDefinitions';
import ColorDefinitions from '../constants/ColorDefinitions';
import moment from "moment-timezone";

export default class UnavailableListItem extends PureComponent{
    render(){
        let item = this.props.item;

        return (
            <TouchableOpacity style={styles.container} onPress={()=>this._onOpenPress()}>
                <View style={styles.cardContainer}>
                    <View style={styles.cardBody}>
                        {
                            item.title && <Text
                                style={styles.bodyTitle}
                                numberOfLines={2}
                                ellipsizeMode='tail'>
                                { item.title }
                            </Text>
                        }
                        <Text
                            style={styles.bodySummary}
                            numberOfLines={4}
                            ellipsizeMode='tail'>
                            { item.body }
                        </Text>
                        <View style={ styles.userAndPostingTime }>
                            <Icon
                                iconStyle={{paddingTop:4, paddingLeft:3,paddingRight:5}}
                                name={this._getIconName()}
                                color={this._getColor()}
                                type='ionicon'
                                size={TypoDefinitions.xxSmallFont }/>
                            <Text style={[styles.bodyCaption, {color: this._getColor()}]}> {this._getMessage()} </Text>
                            <Icon
                                iconStyle={{paddingTop:4, paddingLeft:3,paddingRight:5}}
                                name='ios-water'
                                color={this._getColor()}
                                type='ionicon'
                                size={TypoDefinitions.xxSmallFont }/>
                            <Text style={[styles.bodyCaption, {color: this._getColor()}]}> {moment(item.updated_at).fromNow()} </Text>
                        </View>
                    </View>
                    <Divider/>
                    <View style={styles.cardFooter}>
                        <TouchableOpacity onPress={() => this.onDeletePress()}>
                            <View style={styles.comboContainer}>
                                <Icon
                                    type='ionicon'
                                    name='md-trash'
                                    color={ColorDefinitions.errorBackground}
                                    size={TypoDefinitions.smallFont}
                                    iconStyle={{ paddingTop:3, paddingLeft: 0, paddingRight: 2 }}/>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </TouchableOpacity>
        )
    };

    onDeletePress = () => {
        const item = this.props.item;
        this.props.onDeletePress(item)
    };

    _onOpenPress = ()=>{
        const item = this.props.item;
        const alertObj={title: 'Inaccessible Content', message: ''};

        if (item.is_deleted)
            alertObj.message = 'This item was deleted by owner and its no longer available ☹️ ';
        if (item.is_archived)
            alertObj.message = 'This item can not be accessed, owner has decided to disable interaction with their content ☹️';

        this.props.onOpenItem(alertObj);
    };


    _getColor = () => {
        const item = this.props.item;
        if (item.is_deleted)
            return ColorDefinitions.cards.deleted;
        else if (item.is_archived)
            return ColorDefinitions.cards.archived;
        else
            return ColorDefinitions.cards.idle
    };

    _getIconName = () =>{
        const item = this.props.item;
        if (item.is_deleted) return 'md-trash';
        else if (item.is_archived) return 'ios-archive';
        else return 'md-person'
    };

    _getMessage = () =>{
        const item = this.props.item;
        if (item.is_deleted) return 'This item was deleted';
        else if (item.is_archived) return 'This item was archived';
        else return item.author
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 0,
        padding: 10,
    },

    cardContainer: {
        flex:0,
        flexDirection:'column'
    },

    cardHeader: {
        flex:0,
        flexDirection: 'row',
    },

    headerIcon:{
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    },

    headerText:{
        flexDirection: 'column',
        justifyContent: 'center',
        paddingLeft: 10,
        paddingRight: 10
    },

    headerTitle:{
        fontSize: TypoDefinitions.smallFont,
        fontWeight: TypoDefinitions.normalText
    },

    headerCaption:{
        fontSize: TypoDefinitions.xSmallFont,
        fontWeight: TypoDefinitions.normalText
    },

    cardBody: {
        flex:0,
        flexDirection: 'column',
        paddingBottom: 10
    },

    bodyTitle:{
        color: ColorDefinitions.cards.deletedText,
        textDecorationLine:'line-through',
        fontSize: TypoDefinitions.normalFont,
        fontWeight: TypoDefinitions.boldText,
        paddingBottom: 5,
    },

    bodySummary:{
        color: ColorDefinitions.cards.deletedText,
        textDecorationLine:'line-through',
        fontSize: TypoDefinitions.smallFont,
        fontWeight: TypoDefinitions.thinText,
        paddingBottom: 5
    },

    bodyCaption:{
        textAlign: 'right',
        fontSize: TypoDefinitions.xSmallFont,
        fontWeight: TypoDefinitions.lightText,
        color: ColorDefinitions.cards.idle
    },

    cardFooter: {
        paddingTop:5,
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },

    comboContainer:{
        flexDirection: 'row',
        alignContent:'center',
        justifyContent: 'space-between'
    },
    footerLabel: {
        //paddingBottom:2,
        paddingLeft:2,
        paddingRight:2,
        fontSize: TypoDefinitions.xSmallFont,
        fontWeight: TypoDefinitions.normalText,
        color:ColorDefinitions.cards.idle
    },

    footerIcon: {
        paddingTop:2
    },

    userAndPostingTime:{
        flexDirection: 'row',
        justifyContent: 'flex-start'
    }
});
