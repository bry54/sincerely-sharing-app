import React,{PureComponent} from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {Avatar, Divider, Icon} from 'react-native-elements';
import TypoDefinitions from '../constants/TypoDefinitions';
import moment from 'moment-timezone';
import ColorDefinitions from '../constants/ColorDefinitions';

export default class UnavailableHeaderItem extends PureComponent{
    render(){
        let item = this.props.item;

        return (
            <View style={styles.container}>
                <View style={styles.cardBody}>
                    {
                        item.title && <Text style={styles.bodyTitle}>
                            {item.title}
                        </Text>
                    }
                    <Text style={styles.bodySummary}>
                        {item.body}
                    </Text>
                    <View style={ styles.userAndPostingTime }>
                        <Icon
                            iconStyle={{paddingTop:4, paddingLeft:3,paddingRight:5}}
                            name={this._getIconName()}
                            color={this._getColor()}
                            type='ionicon'
                            size={TypoDefinitions.xxSmallFont }/>
                        <Text style={[styles.bodyCaption, {color: this._getColor()}]}> {this._getMessage()} </Text>
                        <Icon
                            iconStyle={{paddingTop:4, paddingLeft:3,paddingRight:5}}
                            name='ios-water'
                            color={this._getColor()}
                            type='ionicon'
                            size={TypoDefinitions.xxSmallFont }/>
                        <Text style={[styles.bodyCaption, {color: this._getColor()}]}> {moment(item.updated_at).fromNow()} </Text>
                    </View>
                </View>
            </View>
        )
    };

    _getColor = () => {
        const item = this.props.item;
        if (item.is_deleted) return ColorDefinitions.cards.deleted;
        else if (item.is_archived) return ColorDefinitions.cards.archived;
        else return ColorDefinitions.cards.idle
    };

    _getIconName = () =>{
        const item = this.props.item;
        if (item.is_deleted) return 'md-trash';
        else if (item.is_archived) return 'ios-archive';
        else return 'md-person'
    };

    _getMessage = () =>{
        const item = this.props.item;
        if (item.is_deleted) return 'This item was deleted';
        else if (item.is_archived) return 'This item was archived';
        else return item.author
    };

}

const styles = StyleSheet.create({
    container: {
        flex: 0,
        padding: 10,
    },

    cardContainer: {
        flex:0,
        flexDirection:'column'
    },

    cardHeader: {
        flex:0,
        flexDirection: 'row',
    },

    headerIcon:{
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    },

    headerText:{
        flexDirection: 'column',
        justifyContent: 'center',
        paddingLeft: 10,
        paddingRight: 10
    },

    headerTitle:{
        fontSize: TypoDefinitions.smallFont,
        fontWeight: TypoDefinitions.normalText
    },

    headerCaption:{
        fontSize: TypoDefinitions.xSmallFont,
        fontWeight: TypoDefinitions.normalText
    },

    cardBody: {
        flex:0,
        flexDirection: 'column',
        paddingBottom: 10
    },

    bodyTitle:{
        fontSize: TypoDefinitions.normalFont,
        fontWeight: TypoDefinitions.boldText,
        paddingBottom: 5,
    },

    bodySummary:{
        fontSize: TypoDefinitions.smallFont,
        fontWeight: TypoDefinitions.normalText,
        paddingBottom: 5
    },

    bodyCaption:{
        textAlign: 'right',
        fontSize: TypoDefinitions.xxSmallFont,
        fontWeight: TypoDefinitions.lightText,
        color: ColorDefinitions.cards.idle
    },

    cardFooter: {
        paddingTop:5,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },

    comboContainer:{
        flexDirection: 'row',
        alignContent:'center',
        justifyContent: 'space-between'
    },
    footerLabel: {
        //paddingBottom:2,
        paddingLeft:2,
        paddingRight:2,
        fontSize: TypoDefinitions.xSmallFont,
        fontWeight: TypoDefinitions.normalText,
    },

    footerIcon: {
        paddingTop:2
    },

    userAndPostingTime:{
        flexDirection: 'row',
        justifyContent: 'flex-end'
    }
});
