import React from 'react';
import {View} from 'react-native'
import {createSwitchNavigator, createMaterialTopTabNavigator} from 'react-navigation';
import ColorDefinitions from '../constants/ColorDefinitions';
import TypoDefinitions from '../constants/TypoDefinitions';

const FIFCommunitiesTab = createSwitchNavigator({
    FIFCommunities: ()=>(<View/>),
});

FIFCommunitiesTab.navigationOptions = {
    tabBarLabel: 'FIF Communities',
};

const OpenCommunitiesTab = createSwitchNavigator({
    OpenCommunities: ()=>(<View/>),
});

OpenCommunitiesTab.navigationOptions = {
    tabBarLabel: 'Open Communities',
};

const tabNavigator = createMaterialTopTabNavigator({
    FIFCommunitiesTab,
    OpenCommunitiesTab,
},{
    tabBarOptions: {
        scrollEnabled: false,
        upperCaseLabel: false,
        tabBarLabel: {

        },
        labelStyle: {
            fontSize: TypoDefinitions.xSmallFont,
            fontWeight: TypoDefinitions.ultraLightText,
        },
        style: {
            backgroundColor: ColorDefinitions.tabs.top.tabBackground,
        },
        indicatorStyle: {
            backgroundColor: ColorDefinitions.tabs.top.tabActiveIndicator,
        },
    },
});

export default tabNavigator;
