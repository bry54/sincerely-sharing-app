import React from 'react';
import { Platform, View } from 'react-native';
import { createBottomTabNavigator, createStackNavigator } from 'react-navigation';
import TabBarIcon from '../components/TabBarIcon';
import ProfileTabNavigator from './ProfileTabNavigator';
import ColorDefinitions from '../constants/ColorDefinitions';
import TypoDefinitions from '../constants/TypoDefinitions';
import NewPostScreen from '../screens/post/NewPostScreen';
import CommunitiesHomeScreen from '../screens/communities/CommunitiesHomeScreen';
import FeedsListScreen from '../screens/feeds/FeedsListScreen';
import FeedsDetailScreen from '../screens/feeds/FeedsDetailScreen';
import CommentDetailScreen from '../screens/comments/CommentDetailScreen';
import CommentingScreen from '../screens/feeds/CommentingScreen';
import AboutDeveloperScreen from '../screens/about/AboutDeveloperScreen';
import ReportProblemScreen from '../screens/about/ReportProblemScreen';
import PrivacyPolicyScreen from '../screens/about/PrivacyPolicyScreen';
import TermsOfUseScreen from '../screens/about/TermsOfUseScreen';
import AboutHomeScreen from '../screens/about/AboutHomeScreen';
import RequestCommunityScreen from "../screens/communities/RequestCommunityScreen";

const HomeStack = createStackNavigator({
    homeFeeds: FeedsListScreen,
    homeFeedDetail: FeedsDetailScreen,
    commentAsPost: CommentDetailScreen,
    commentOnFeed: CommentingScreen,
    commentOnComment: CommentingScreen
}, {
    navigationOptions:({navigation})=>({
        tabBarLabel: 'Home',
        tabBarIcon: ({ focused }) => (
            <TabBarIcon
                focused={focused}
                name={ Platform.OS === 'ios' ? 'ios-list-box' : 'ios-list-box' }
            />
        ),
        tabBarOnPress: () => {
            navigation.navigate({ routeName: 'homeFeeds' });
        }
    }),
});

const CommunitiesStack = createStackNavigator({
    communitiesHome: CommunitiesHomeScreen,
    communityFeeds: FeedsListScreen,
    communityFeedDetail: FeedsDetailScreen,
    commentAsPost: CommentDetailScreen,
    commentOnFeed: CommentingScreen,
    commentOnComment: CommentingScreen,
    communityRequest: RequestCommunityScreen,
}, {
    navigationOptions:({navigation})=>({
        tabBarLabel: 'Communities',
        tabBarIcon: ({ focused }) => (
            <TabBarIcon
                focused={focused}
                name={ Platform.OS === 'ios' ? 'ios-color-filter' : 'ios-color-filter' }
            />
        ),
        tabBarOnPress: () => {
            navigation.navigate({ routeName: 'communitiesHome' });
        }
    }),
});

const NewPostStack = createStackNavigator({
    newPost: NewPostScreen,
}, {
    navigationOptions:({navigation})=>({
        tabBarLabel: 'New Post',
        tabBarIcon: ({ focused }) => (
            <TabBarIcon
                focused={focused}
                name={ Platform.OS === 'ios' ? 'ios-create' : 'ios-create' }
            />
        ),
        tabBarOnPress: () => {
            navigation.navigate({ routeName: 'newPost' });
        }
    }),
});

const AboutStack = createStackNavigator({
    aboutHome: AboutHomeScreen,
    tou: TermsOfUseScreen,
    privacyPolicy: PrivacyPolicyScreen,
    reportProblem: ReportProblemScreen,
    aboutDeveloper: AboutDeveloperScreen
}, {
    navigationOptions:({navigation})=>({
        tabBarLabel: 'About',
        tabBarIcon: ({ focused }) => (
            <TabBarIcon
                focused={focused}
                name={ Platform.OS === 'ios' ? 'ios-information-circle-outline' : 'ios-information-circle-outline' }
            />
        ),
        tabBarOnPress: () => {
            navigation.navigate({ routeName: 'aboutHome' });
        }
    }),
});

const ProfileStack = createStackNavigator({
    myProfile: ProfileTabNavigator,
    editPost: NewPostScreen,
    commentOnFeed: CommentingScreen,
    commentOnComment: CommentingScreen,
    myProfileFeedDetail: FeedsDetailScreen,

    commentAsPost: CommentDetailScreen,
}, {
    navigationOptions: ({navigation}) => ({
        tabBarLabel: 'Profile',
        tabBarIcon: ({focused}) => (
            <TabBarIcon
                focused={focused}
                name={Platform.OS === 'ios' ? 'md-person' : 'md-person'}
            />
        ),
        tabBarOnPress: () => {
            navigation.navigate({routeName: 'myProfile'});
        }
    })
});

export default createBottomTabNavigator({
    HomeStack,
    CommunitiesStack,
    NewPostStack,
    ProfileStack,
    AboutStack
},{
    tabBarOptions: {
        scrollEnabled: true,
        upperCaseLabel: false,
        activeTintColor: ColorDefinitions.tabs.main.tabIconSelected
    },
});
