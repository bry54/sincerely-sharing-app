import React from 'react';
import {View} from 'react-native'
import {createSwitchNavigator, createMaterialTopTabNavigator} from 'react-navigation';
import ColorDefinitions from '../constants/ColorDefinitions';
import TypoDefinitions from '../constants/TypoDefinitions';
import TopTabBarIcon from '../components/TopTabBarIcon';

import MyProfileScreen from '../screens/profile/MyProfile'
import MyPostsScreen from "../screens/profile/MyPostsScreen";
import MyCommentsScreen from "../screens/profile/MyCommentsScreen";
import SavedPostsScreen from "../screens/profile/SavedPostsScreen";
import SavedCommentsScreen from "../screens/profile/SavedCommentsScreen";

const MeTab = createSwitchNavigator({
    myInfo: MyProfileScreen,
},{
    navigationOptions: ({navigation}) =>({
        tabBarLabel: 'My Profile',
        tabBarIcon: ({focused}) => (<TopTabBarIcon name='md-person' focused={focused}/>)
    })
});

const PostsTab = createSwitchNavigator({
    myPosts: MyPostsScreen,
},{
    navigationOptions: ({navigation}) =>({
        tabBarLabel: 'My Posts',
        tabBarIcon: ({focused}) => (<TopTabBarIcon name='md-filing' focused={focused}/>)
    })
});

const CommentsTab = createSwitchNavigator({
    myComments: MyCommentsScreen,
}, {
    navigationOptions: ({navigation}) =>({
        tabBarLabel: 'My Comments',
        tabBarIcon: ({focused}) => (<TopTabBarIcon name='md-quote' focused={focused}/>)
    })
});

const BookmarkedPostsTab = createSwitchNavigator({
    bookmarks: SavedPostsScreen,
},{
    navigationOptions: ({navigation}) =>({
        tabBarLabel: 'Saved Posts',
        tabBarIcon: ({focused}) => (<TopTabBarIcon name='md-bookmarks' focused={focused}/>)
    })
});

const BookmarkedCommentsTab = createSwitchNavigator({
    bookmarks: SavedCommentsScreen,
},{
    navigationOptions: ({navigation}) =>({
        tabBarLabel: 'Saved Comments',
        headerTintColor: ColorDefinitions.tintColor,
        tabBarIcon: ({focused}) => (<TopTabBarIcon name='md-bookmark' focused={focused}/>)
    })
});

const tabNavigator = createMaterialTopTabNavigator({
    MeTab,
    PostsTab,
    CommentsTab,
    BookmarkedPostsTab,
    BookmarkedCommentsTab,
},{
    initialRouteName: 'MeTab',
    lazy: true,
    navigationOptions: ({navigation}) => ({
        title: setHeaderTitle(navigation),
        headerTintColor: ColorDefinitions.tintColor,
        //headerStyle: { backgroundColor: ColorDefinitions.header.background },
        headerTitleStyle: { fontWeight: TypoDefinitions.ultraLightText },
    }),
    tabBarOptions: {
        scrollEnabled: false,
        upperCaseLabel: false,
        showIcon: true,
        showLabel: false,
        labelStyle: {
            fontSize: TypoDefinitions.xSmallFont,
            fontWeight: TypoDefinitions.ultraLightText,
        },
        style: {
            backgroundColor: ColorDefinitions.tabs.top.tabBackground,
        },
        indicatorStyle: {
            backgroundColor: ColorDefinitions.tabs.top.tabActiveIndicator,
        },

    },
});

const setHeaderTitle = (navigation)=>{
    let headerTitle = 'My Profile';
    const routeIndex = navigation.state.index;
    switch(routeIndex){
        case 0:
            headerTitle='Me';
            break;

        case 1:
            headerTitle='My Posts';
            break;

        case 2:
            headerTitle='My Comments';
            break;

        case 3:
            headerTitle='Bookmarked Feeds';
            break;

        case 4:
            headerTitle='Bookmarked Comments';
            break;
    }

    return headerTitle;
};

export default tabNavigator;
