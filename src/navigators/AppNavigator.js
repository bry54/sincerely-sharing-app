import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import AuthLoadingScreen from '../screens/auth/AuthLoadingScreen';
import AuthNavigator from './AuthNavigator';
import MainNavigator from './MainTabNavigator';

export default createAppContainer(createSwitchNavigator({
        //Welcome: WelcomeScreen ,
        AuthLoading: AuthLoadingScreen,
        Auth: AuthNavigator,
        Main: MainNavigator,
    },
    {
        initialRouteName: 'AuthLoading',
    }
));
