import React from 'react';
import { createStackNavigator } from 'react-navigation';

import SignInScreen from '../screens/auth/SignInScreen';
import SignUpScreen from '../screens/auth/SignUpScreen';
import ForgotPasswordScreen from '../screens/auth/ForgotPasswordScreen';

const AuthStack = createStackNavigator({
        SignIn : SignInScreen,
        SignUp : SignUpScreen,
        ForgotPassword : ForgotPasswordScreen
    },
    {
        initialRouteName: 'SignIn'
    }
);

export default AuthStack;
