import {store} from "../store/utilities/storeConfiguration";

export const axiosConfig = async () => {
    const state = store.getState();
    const loginPayload  = state.authReducer.loginPayload;

    const config = {
        timeout: 1000 * 5,
        headers: {'Authorization': "Bearer " + loginPayload.token}
    };

    return config
};
