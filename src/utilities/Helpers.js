export default {
    capitalizeString: (text: string) => typeof text === 'string' && text.length > 0 && `${text[0].toUpperCase()}${text.slice(1)}`,

    extractInitials: (text: string) => text.split(/\s/).reduce((response,word)=> response+=word.slice(0,1),''),

    titleCaseString: (text) => text.toLowerCase().split(' ').map((s) => s.charAt(0).toUpperCase() + s.substring(1)).join(' '),

    flattenRequestErrors: (errorsObj) => {
        let errorsArray = [];
        const keys = Object.keys(errorsObj);
        keys.forEach((key) => {
            const error = errorsObj[key]
            for (let i = 0; i<error.length; i++)
                errorsArray.push(error[i])
        });
        return errorsArray
    }
}