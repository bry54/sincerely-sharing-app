import React, {Component} from 'react';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

import {Provider} from 'react-redux';
import {store} from './src/store/utilities/storeConfiguration';
import moment from 'moment-timezone'

class WrappedApp extends Component{
    render() {
        moment.tz.setDefault('UTC');

        return (
            <Provider store={store}>
                <App/>
            </Provider>
        );
    }
}

AppRegistry.registerComponent(appName, () => WrappedApp);
