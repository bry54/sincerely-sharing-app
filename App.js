import React, {Component, Fragment} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  StatusBar,
} from 'react-native';
import AppNavigator from './src/navigators/AppNavigator';

export default class App extends Component {
  render() {
    return (
        <Fragment>
          <SafeAreaView style={{ flex: 0, backgroundColor: 'red' }} />
          <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
            <StatusBar barStyle="light-content" />
            <AppNavigator/>
          </SafeAreaView>
        </Fragment>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F3F3F3',
  },
});
